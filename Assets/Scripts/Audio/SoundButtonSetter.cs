﻿using UnityEngine;
using UnityEngine.UI;


public class SoundButtonSetter : MonoBehaviour
{
    private void OnValidate()
    {
        var buttons = transform.GetComponentsInChildren<Button>();

        foreach (var i in buttons)
        {
            if (i.GetComponent<SoundButton>() == null)
            {
                var sb = i.gameObject.AddComponent<SoundButton>();
                sb.Initialize();
            }
        }
    }
}
