﻿using Fighter5011;
using UnityEngine;


[CreateAssetMenu(fileName = "AudioSettings", menuName = NamingUtility.MenuItems.IngameSettings + "AudioSettings")]
public class AudioSettings : ScriptableObject
{
    public AudioClip uiButtonSoundClips = default;

    [SerializeField] private AudioClip[] arenaMusicClips = default;
    [SerializeField] private AudioClip[] hitClips = default;
    [SerializeField] private AudioClip[] emotionClips = default;
    [SerializeField] private AudioClip[] levelStartClips = default;


    public AudioClip RandomArenaMusic => arenaMusicClips.RandomObject();

    public AudioClip RandomHitClip => hitClips.RandomObject();

    public AudioClip RandomEmotionClip => emotionClips.RandomObject();
    
    public AudioClip RandomArenaStartClip => levelStartClips.RandomObject();
}
