﻿using UnityEngine;
using UnityEngine.UI;


public class SoundButton : MonoBehaviour
{
    [SerializeField] private Button button = default;



    public void Initialize()
    {
        button = button ?? GetComponent<Button>();
    }


    private void Awake()
    {
        Initialize();
        button.onClick.AddListener(PlaySound);
    }


    private void OnDestroy()
    {
        button.onClick.RemoveListener(PlaySound);
    }

    private void PlaySound()
    {
        AudioManager.Instance.Play(IngameData.asset.Value.Settings.audioSettings.uiButtonSoundClips);
    }
}
