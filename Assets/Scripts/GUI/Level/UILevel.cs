﻿using System;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;
using Fighter5011.Ui;
using Fighter5011;
using TMPro;


public class UILevel : AnimatorScreen
{
    public event Action OnShouldFinishLevel;


    [Header("Movement")]
    [SerializeField]
    private UILevelMovement movementController = null;

    [Space]

    [Header("Attacking")]
    [SerializeField]
    private UILevelAttacking attackingController = null;

    [Space]

    [Header("Abilities")]
    [SerializeField]
    private UILevelAbilities abilitiesController = null;

    [Space]

    [Header("Top Elements")]
    [SerializeField]
    private UILevelCounter counters = default;

    [Header("Health")]
    [SerializeField]
    private Image healthSlider = null;
    [SerializeField]
    private float healthReduceDuration = 0.3f;

    [Header("Combo")]
    [SerializeField]
    private TMP_Text levelTimer = null;

    [Header("Common")]
    [SerializeField] private Button menuBackButton = default;
    [SerializeField] private TMP_Text additionalTimeAnnouncerRoot = default;
    [SerializeField] private VectorAnimation additionalTimeAnimation = default;
    [SerializeField] private FactorAnimation additionalTimeAnimationFade = default;

    [Header("Tutorial")]
    [SerializeField] private UILevelTutorial uILevelTutorial = default;


    public UILevelMovement MovementController => movementController;


    public UILevelAttacking AttackingController => attackingController;


    public UILevelAbilities AbilitiesController => abilitiesController;


    public UILevelCounter UILevelCounter => counters;

    public override ScreenType ScreenType => ScreenType.Ingame;


    public override void Show()
    {
        base.Show();

        movementController.Initialize();
        uILevelTutorial.Initialize();
        additionalTimeAnnouncerRoot.gameObject.SetActive(false);

        menuBackButton.onClick.AddListener(FinishLevel);

        ArenaTimerController.OnTimeChanged += SetSecondBeforeLevelEnd;
        ArenaTimerController.OnAdditionalTimeAdded += PlayAdditionalTimeAnnouncer;
    }


    public override void Hide()
    {
        movementController.Deinitialize();
        attackingController.Deinitialize();
        uILevelTutorial.Deinitialize();

        menuBackButton.onClick.RemoveListener(FinishLevel);

        ArenaTimerController.OnTimeChanged -= SetSecondBeforeLevelEnd;
        ArenaTimerController.OnAdditionalTimeAdded -= PlayAdditionalTimeAnnouncer;

        base.Hide();
    }

    public override void Deinitialize()
    {
        DOTween.Kill(healthSlider);
        DOTween.Kill(this);

        additionalTimeAnnouncerRoot.gameObject.SetActive(false);
        base.Deinitialize();
    }



    public void InitializeAttackButton(FighterCombatData.Type type)
    {
        attackingController.Initialize(type);
    }


    public void InitializeAbilityButtons(List<Ability> abilitiesData)
    {
        abilitiesController.Initialize(abilitiesData);
    }


    public void SetHealthInfo(float health, float maxHealth)
    {
        float sliderValue = health / maxHealth;

        healthSlider.DOComplete();
        healthSlider.DOFillAmount(sliderValue, healthReduceDuration);
    }


    public void SetSecondBeforeLevelEnd(float second)
    {
        int secondsLeft = Mathf.RoundToInt(second);
        TimeSpan span = new TimeSpan(0, 0, secondsLeft);

        levelTimer.text = span.ToTotalMMSS();
    }


    public void PlayAdditionalTimeAnnouncer()
    {
        additionalTimeAnnouncerRoot.gameObject.SetActive(true);

        additionalTimeAnimation.Play((value) => additionalTimeAnnouncerRoot.rectTransform.localScale = value, this, () =>
        {
            additionalTimeAnimation.Play((value) => additionalTimeAnnouncerRoot.rectTransform.localScale = value, this, reversed: true);
        });
        
        additionalTimeAnimationFade.Play((value) => additionalTimeAnnouncerRoot.color = additionalTimeAnnouncerRoot.color.SetA(value), this, () =>
        {
            additionalTimeAnimationFade.Play((value) => additionalTimeAnnouncerRoot.color = additionalTimeAnnouncerRoot.color.SetA(value), this, reversed: true);
        });
    }

    private void FinishLevel()
    {
        OnShouldFinishLevel?.Invoke();
    }
}
