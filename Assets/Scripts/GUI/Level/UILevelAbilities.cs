﻿using System;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;


[Serializable]
public class UILevelAbilities
{
    [Serializable]
    public class AbilityInfo
    {
        public Image image = default;
        public UIButton useAbilityButton = default;
        public Text textInfo = default;
        [NonSerialized] public Ability ability = default;
    }

    public event Action<Ability> OnPressAbility;
    public event Action<Ability> OnPressUpAbility;

    [Header("Data")]
    [SerializeField]
    private AbilityInfo[] abilityInfos = default;



    public void Initialize(List<Ability> abilitiesData)
    {
        for (int i = 0; i < abilityInfos.Length; i++)
        {
            if (i >= abilitiesData.Count || abilitiesData[i] == null)
            {
                abilityInfos[i].image.gameObject.SetActive(false);
                continue;
            }

            Ability abilityForInfo = abilitiesData[i];

            void abilityUseEvent() => TriggerUseAbilityEvent(abilityForInfo); // EBAAAAT. LAMBDA METHODS !!! TUT YA UZNAL O NIH
            void abilityUpUseEvent(float duration) => TriggerOnUpAbilityEvent(abilityForInfo, duration);

            abilityInfos[i].useAbilityButton.onPressed.AddListener(abilityUseEvent);
            abilityInfos[i].useAbilityButton.onClickUp.AddListener(abilityUpUseEvent);

            abilityInfos[i].image.sprite = abilityForInfo.UISprite;
            abilityInfos[i].ability = abilityForInfo;

            abilityInfos[i].textInfo.gameObject.SetActive(abilityForInfo.shouldShowText);
            
            abilityInfos[i].ability.OnShouldDisplayTextInfo += DisplayText;
            abilityInfos[i].ability.OnShouldChangeUISprite += SwapAbilitySprite;

            if (abilityInfos[i].ability is ActiveAbility activeAbility)
            {
                activeAbility.OnAbilityBegin += AnimateAbilityUI;
                activeAbility.OnCooldownEnd += CompleteAnimateAbilityUI;
            }
            
            if (abilityInfos[i].ability is DoubleUseAbility doubleUseAbility)
            {
                doubleUseAbility.OnFirstTimeUsed += SetSecondPhaseState;
                doubleUseAbility.OnSecondTimeUsed += ReturnSecondPhaseToNormalState;
                doubleUseAbility.OnCooldownEnd += ReturnSecondPhaseToNormalState;
            }
        }
    }


    public void Deinitialize()
    {
        foreach (var info in abilityInfos)
        {
            info.useAbilityButton.onPressed.RemoveAllListeners();
            info.useAbilityButton.onClickUp.RemoveAllListeners();

            info.ability.OnShouldChangeUISprite -= SwapAbilitySprite;
            info.ability.OnShouldDisplayTextInfo -= DisplayText;

            if (info.ability is ActiveAbility activeAbility)
            {
                activeAbility.OnAbilityBegin -= AnimateAbilityUI;
                activeAbility.OnCooldownEnd -= CompleteAnimateAbilityUI;
            }

            if (info.ability is DoubleUseAbility doubleUseAbility)
            {
                doubleUseAbility.OnFirstTimeUsed -= SetSecondPhaseState;
                doubleUseAbility.OnSecondTimeUsed -= ReturnSecondPhaseToNormalState;
                doubleUseAbility.OnCooldownEnd -= ReturnSecondPhaseToNormalState;
            }
        }

        DOTween.Kill(this, true);
    }

    private void SwapAbilitySprite(Ability ability, Sprite sprite)
    {
        var info = Array.Find(abilityInfos, element => element.ability.Equals(ability));

        if (info != null)
        {
            info.image.sprite = sprite;
        }
    }


    public void AnimateColldown(Ability ability, float duration, float startValue = 0.0f)
    {
        var info = Array.Find(abilityInfos, element => element.ability.Equals(ability));

        if (info != null)
        {
            info.image.DOComplete(true);

            info.image.fillAmount = startValue;
            info.image
                .DOFillAmount(1.0f, duration)
                .SetEase(Ease.Linear)
                .SetId(this);
        }
        else
        {
            Debug.LogError("Try set cooldown non existent ability " + ability.AbilityName);
        }
    } 


    public void CompleteAnimateColldown(Ability ability)
    {
        var info = Array.Find(abilityInfos, element => element.ability.Equals(ability));

        if (info != null)
        {
            info.image.DOComplete(true);
        }
    }


    private void DisplayText(Ability.Name abilityName, string text)
    {
        var info = Array.Find(abilityInfos, element => element.ability.AbilityName == abilityName);

        if (info != null)
        {
            info.textInfo.text = text;
        }
    }


    private void TriggerUseAbilityEvent(Ability ability)
    {
           OnPressAbility?.Invoke(ability);
    }


    private void TriggerOnUpAbilityEvent(Ability ability, float holdDuration)
    {
        OnPressUpAbility?.Invoke(ability);
    }


    private void SetSecondPhaseState(Ability ability)
    {
        var info = Array.Find(abilityInfos, element => element.ability.Equals(ability));

        if (info != null && info.ability is DoubleUseAbility doubleUseAbility)
        {
            info.image.sprite = doubleUseAbility.SecondaryUISprite;

            float secondPhaseDuration = doubleUseAbility.SecondsForSecondPhase;
            AnimateColldown(ability, secondPhaseDuration);
        }
    }


    private void ReturnSecondPhaseToNormalState(Ability ability)
    {
        var info = Array.Find(abilityInfos, element => element.ability.Equals(ability));

        if (info != null)
        {
            info.image.sprite = info.ability.UISprite;
        }

        if (ability is ActiveAbility activeAbility)
        {
            // how much is rest
            float currentTimer = activeAbility.CoolDown - activeAbility.coolDownTimer;
            float startValue = activeAbility.coolDownTimer / activeAbility.CoolDown;

            AnimateColldown(ability, currentTimer, startValue);
        }
    }


    private void CompleteAnimateAbilityUI(Ability ability)
    {
        CompleteAnimateColldown(ability);
    }


    private void AnimateAbilityUI(Ability ability)
    {
        var info = Array.Find(abilityInfos, element => element.ability.Equals(ability));
        float cooldown = (info.ability as ActiveAbility).CoolDown;

        AnimateColldown(ability, cooldown);
    }

}
