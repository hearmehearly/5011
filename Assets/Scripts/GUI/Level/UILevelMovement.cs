﻿using System;
using UnityEngine;
using UnityEngine.UI;


[Serializable]
public class UILevelMovement
{
    public event Action OnShouldMoveLeft;
    public event Action OnShouldMoveRight;
    public event Action OnShouldStop;

    public event Action<bool> OnShouldJump; // is from joystick
    public event Action OnShouldSeatDown;
    public event Action OnShouldStandUp;
    public event Action OnShouldCharge;


    [Header("Movement")]

    [SerializeField] private UILevelMovementSettings settings = default;

    [SerializeField] private VariableJoystick movementJoystick = default;

    [SerializeField] private float secondsForCharge = default;
    [SerializeField]
    private PressedButton moveLeftButton = default;
    [SerializeField]
    private PressedButton moveRightButton = default;
    [SerializeField]
    private UIButton jumpButton = default;
    [SerializeField]
    private UIButton seatDownButton = default;
    
    private bool isSeatDown;
    private Vector2 movingDirection;
    private bool isJoystickChargeEventAvailable;



    public void Initialize()
    {
        moveLeftButton.onPressedDown.AddListener(TriggerMoveLeftEvent);
        moveRightButton.onPressedDown.AddListener(TriggerMoveRightEvent);

        moveLeftButton.onPressedUp.AddListener(TriggerStopEvent);
        moveRightButton.onPressedUp.AddListener(TriggerStopEvent);

        jumpButton.onPressed.AddListener(TriggerJumpEventButton);
        jumpButton.onPressed.AddListener(TriggerChargeEvent);

        seatDownButton.onPressed.AddListener(TriggerSeatDownEvent);
        seatDownButton.onClickUp.AddListener(TriggerStandUpEvent);

        movementJoystick.onPressed.AddListener(ProcessJoystick);
        movementJoystick.onPressedUp.AddListener(TriggerStopEvent);

        Scheduler.OnUpdate += OnJoystickUpdate;

        isSeatDown = false;
        isJoystickChargeEventAvailable = false;
    }


    public void Deinitialize()
    {
        moveLeftButton.onPressedDown.RemoveListener(TriggerMoveLeftEvent);
        moveRightButton.onPressedDown.RemoveListener(TriggerMoveRightEvent);

        moveLeftButton.onPressedUp.RemoveListener(TriggerStopEvent);
        moveRightButton.onPressedUp.RemoveListener(TriggerStopEvent);
        
        jumpButton.onPressed.RemoveListener(TriggerJumpEventButton);
        jumpButton.onPressed.RemoveListener(TriggerChargeEvent);

        seatDownButton.onPressed.RemoveListener(TriggerSeatDownEvent);
        seatDownButton.onClickUp.RemoveListener(TriggerStandUpEvent);

        movementJoystick.onPressed.RemoveListener(ProcessJoystick);
        movementJoystick.onPressedUp.RemoveListener(TriggerStopEvent);

        Scheduler.OnUpdate -= OnJoystickUpdate;
        TaskInvoker.Instance.UnscheduleAllMethodForTarget(this);
    }


    private void TriggerMoveLeftEvent()
    {
        movingDirection = Vector2.left;
        OnShouldMoveLeft?.Invoke();
    }


    private void TriggerMoveRightEvent()
    {
        movingDirection = Vector2.right;
        OnShouldMoveRight?.Invoke();
    }


    private void TriggerStopEvent()
    {
        movingDirection = Vector2.zero;
        OnShouldStop?.Invoke();
    }
    

    private void TriggerJumpEventJoystick()
    {
        OnShouldJump?.Invoke(true);
    }


    private void TriggerJumpEventButton()
    {
        OnShouldJump?.Invoke(false);
    }


    private void TriggerSeatDownEvent()
    {
        if (!isSeatDown)
        {
            OnShouldSeatDown?.Invoke();
            isSeatDown = true;
        }
    }


    private void TriggerStandUpEvent(float duration)
    {
        if (isSeatDown)
        {
            OnShouldStandUp?.Invoke();
            isSeatDown = false;
        }
    }


    private void TriggerChargeEvent()
    {
        if (isJoystickChargeEventAvailable)
        {
            OnShouldCharge?.Invoke();
        }
    }


    private void ProcessJoystick()
    {
        Vector2 horizontalVector = movementJoystick.Direction.x < 0 ? Vector2.left : Vector2.right;
        float angle = Vector2.Angle(movementJoystick.Direction, Vector2.right);
        
        if (settings.IsJumpRange(angle) &&
            movementJoystick.Direction.y > settings.minDirectionForChargeY)
        {
            TriggerJumpEventJoystick();
        }

        TriggerStandUpEvent(0.0f);
    }


    private void OnJoystickUpdate(float deltaTime)
    {
        float angle = (Mathf.Atan2(movementJoystick.Direction.y, movementJoystick.Direction.x) * Mathf.Rad2Deg).Clamp0360();

        Vector2 horizontalVector = movementJoystick.Direction.x < 0 ? Vector2.left : Vector2.right;

        if (movingDirection != Vector2.zero ||
            (movementJoystick.Direction.x > settings.minDirectionForMoveX ||
            movementJoystick.Direction.x < -settings.minDirectionForMoveX))
        {
            if (settings.IsMoveRangeLeft(angle))
            {
                TriggerMoveLeftEvent();
            }
            else if (settings.IsMoveRangeRight(angle))
            {
                TriggerMoveRightEvent();
            }
        }

        if (!isSeatDown &&
            movementJoystick.Direction.y < settings.minDirectionForSeatY &&
            settings.IsSeatDownRange(angle))
        {
            TriggerStopEvent();
            TriggerSeatDownEvent();
        }
        else if (movementJoystick.Direction.y > settings.minDirectionForSeatY &&
                isSeatDown)
        {
            TriggerStandUpEvent(0.0f);
        }
        else if (settings.IsJumpInMoveRange(angle, movementJoystick.Direction.y))
        {
            TriggerJumpEventJoystick();
        }

        isJoystickChargeEventAvailable = movementJoystick.Direction.y < settings.minDirectionForChargeY;
    }
}
