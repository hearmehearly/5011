﻿using System;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;


namespace Fighter5011
{
    [Serializable]
    public class UIFighterInfo : MonoBehaviour
    {
        [NonSerialized]
        public Fighter fighter = default;

        [SerializeField] private TMP_Text killsCounter = default;
        [SerializeField] private Material tmpEnemyMaterial = default;
        [SerializeField] private Material tmpPlayerMaterial = default;

        [SerializeField] private Image icon = default;

        [SerializeField] private VectorAnimation scaleInAnimation = default;
        [SerializeField] private VectorAnimation scaleOutAnimation = default;


        public void Initialize(Fighter _fighter)
        {
            fighter = _fighter;

            bool isPlayer = fighter is PlayerFighter;
            icon.sprite = isPlayer ?
                Fighters.GetVisualData(fighter.CurrentName).UiCounterSpritePlayer : Fighters.GetVisualData(fighter.CurrentName).UiCounterSpriteNPC;
            icon.SetNativeSize();

            killsCounter.fontMaterial = isPlayer ? tmpPlayerMaterial : tmpEnemyMaterial;

            transform.localScale = transform.localScale.SetX(isPlayer ? transform.localScale.x : -transform.localScale.x);
            killsCounter.transform.localScale = killsCounter.transform.localScale.SetX(isPlayer ? killsCounter.transform.localScale.x : -killsCounter.transform.localScale.x);
            icon.transform.localScale = icon.transform.localScale.SetX(isPlayer ? icon.transform.localScale.x : -icon.transform.localScale.x);
        }
        

        public void Deinitialize()
        {
            DOTween.Kill(this, true);
        }


        public void SetCounterText(string text)
        {
            DOTween.Kill(this);

            scaleInAnimation.Play((value) => killsCounter.transform.localScale = value, this, () => killsCounter.text = text);
            scaleOutAnimation.Play((value) => killsCounter.transform.localScale = value, this, () =>
            {
                bool isPlayer = fighter is PlayerFighter;
                icon.transform.localScale = icon.transform.localScale.SetX(isPlayer ? icon.transform.localScale.x : -icon.transform.localScale.x);
            });
        }
    }
}
