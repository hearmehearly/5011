﻿using System;
using UnityEngine;


[Serializable]
public class UILevelTutorial
{
    [SerializeField] private TutorialButton[] tutorialButtons = default;


    public void Initialize()
    {
        TutorialManager.OnShouldTiggerTutorial += TutorialManager_OnShouldTiggerTutorial;
    }


    public void Deinitialize()
    {
        TutorialManager.OnShouldTiggerTutorial -= TutorialManager_OnShouldTiggerTutorial;
    }


    private void TutorialManager_OnShouldTiggerTutorial(TutorialType type)
    {
        foreach (var button in tutorialButtons)
        {
            if (button.TutorialType == type)
            {
               TutorialManager.StartTutorial(button);
            }
        }
    }

}
