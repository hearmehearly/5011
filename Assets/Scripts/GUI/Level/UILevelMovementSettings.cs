﻿using System;
using UnityEngine;


public class UILevelMovementSettings : ScriptableObject
{
    [Serializable]
    private class AngleData
    {
        public float min = default;
        public float max = default;
    }
    
    [Range(0.0f, 1.0f)] public float minDirectionForMoveX = default;
    [Range(0.0f, 1.0f)] public float minDirectionForChargeY = default;
    [Range(-1.0f, 0.0f)] public float minDirectionForSeatY = default;
    [Range(0.0f, 1.0f)] public float minDirectionForJumpInMoveY = default;

    [SerializeField] private AngleData moveAnglesLeft = default;
    [SerializeField] private AngleData moveAnglesRight = default;

    [SerializeField] private AngleData jumpInMoveAngles = default;
    [SerializeField] private AngleData jumpAngles = default;
    [SerializeField] private AngleData seatDownAngles = default;

    public float minDelayBetweenJumps = default;

    public bool IsMoveRangeLeft(float angle) => IsAngleRange(angle, moveAnglesLeft);


    public bool IsMoveRangeRight(float angle) => IsAngleRange(angle, moveAnglesRight);


    public bool IsSeatDownRange(float angle) => IsAngleRange(angle, seatDownAngles);


    public bool IsJumpRange(float angle) => IsAngleRange(angle, jumpAngles);


    public bool IsJumpInMoveRange(float angle, float directionY) => IsAngleRange(angle, jumpInMoveAngles) && directionY >= minDirectionForJumpInMoveY;


    private bool IsAngleRange(float angle, AngleData data) => CommonExtensions.IsAngleBetween(angle, data.min, data.max);// (angle > data.min) && (angle < data.max);
}
