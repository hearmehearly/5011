﻿using System;
using UnityEngine;
using UnityEngine.UI;


[Serializable]
public class UILevelAttacking
{
    [Serializable]
    public class AttackImageInfo
    {
        public Sprite sprite = null;
        public FighterCombatData.Type type = FighterCombatData.Type.None;
    }


    public event Action OnShouldAttack;

    [Header("Data")]
    [SerializeField]
    private AttackImageInfo[] attackImageInfos = null;

    [Header("Components")]
    [SerializeField]
    private Image attackImage = null;
    [SerializeField]
    private UIButton attackButton = null;


    public void Initialize(FighterCombatData.Type fighterCombatType)
    {
        attackButton.onPressed.RemoveListener(TriggerAttackEvent);
        attackButton.onPressed.AddListener(TriggerAttackEvent);

        AttackImageInfo attackInfo = Array.Find(attackImageInfos, element => element.type == fighterCombatType);
        attackImage.sprite = attackInfo != null ? attackInfo.sprite : null;
    }


    public void Deinitialize()
    {
        attackButton.onPressed.RemoveListener(TriggerAttackEvent);
    }


    private void TriggerAttackEvent()
    {
        OnShouldAttack?.Invoke();
    }
}
