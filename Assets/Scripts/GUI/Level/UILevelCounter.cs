﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace Fighter5011
{
    [Serializable]
    public class UILevelCounter
    {
        [SerializeField]
        private Transform killsCountersRoot = default;
        [SerializeField]
        private UIFighterInfo uIFighterInfoPrefab = default;


        private List<UIFighterInfo> currentFighterKillInfo = new List<UIFighterInfo>();

        public void InitializeKillsCounter(List<Fighter> fighters)
        {
            currentFighterKillInfo.Clear();
            
            for (int i = 0; i < fighters.Count; i++)
            {
                UIFighterInfo info = UnityEngine.Object.Instantiate(uIFighterInfoPrefab, killsCountersRoot);
                info.Initialize(fighters[i]);

                currentFighterKillInfo.Add(info);

                SetKillsCount(fighters[i], default);
            }
        }


        public void Deinitialize()
        {
            for (int i = currentFighterKillInfo.Count - 1; i >= 0; i--)
            {
                currentFighterKillInfo[i].Deinitialize();
                UnityEngine.Object.Destroy(currentFighterKillInfo[i].gameObject);
            }

            currentFighterKillInfo.Clear();
        }


        public void SetKillsCount(Fighter fighter, int count)
        {
            UIFighterInfo info = currentFighterKillInfo.Find(element => element.fighter.Equals(fighter));

            if (info != null)
            {
                info.SetCounterText(count.ToString());
            }
        }
    }
}
