﻿using Spine.Unity;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Fighter5011.Ui;
using TMPro;


public class UIPlayerMenu : UITweensUnit<UIPlayerMenu.Result>
{
    public static readonly ResourceGameObject<UIPlayerMenu> Prefab = new ResourceGameObject<UIPlayerMenu>("Game/UI/UIMenu");

    public class Result : UnitResult
    {
        public Fighter.Name ChoosedFighterName = default;
    }


    [Header("Transition")]
    [SerializeField] private Button backButton = default;
    [SerializeField] private Button settingsButton = default;

    [Header("Content")]
    [SerializeField] private TMP_Text mainName = default;

    [SerializeField]
    private UIHeroCell[] fighterCells = default;

    [SerializeField]
    private Text textCoins = null;
    [SerializeField]
    private SkeletonGraphic characterAnimation = null;
    [SerializeField]
    private Button play = null;

    [SerializeField] private GameObject mainTab = default;
    [SerializeField] private GameObject chooseTab = default;
    [SerializeField] private Button chooseButton = default;

    [Header("Abilities")]
    [SerializeField] private Transform abilityCellsRoot = default;
    [SerializeField] private UiAbilityCell abilityCellPrefab = default;

    private readonly List<UiAbilityCell> abilityCells = new List<UiAbilityCell>();

    private static Fighter.Name currentChoosedFighter = Fighter.Name.Leiv;


    private Vector2 SaveTopOffset { get; set; }

    private Vector2 SaveTopWithBannerOffset { get; set; }



    public override void Show(Action<Result> onHided = null, Action onShowed = null)
    {
        base.Show(onHided, onShowed);

        play.onClick.AddListener(OnClickClose);
        chooseButton.onClick.AddListener(ShowChooseTab);
        backButton.onClick.AddListener(ShowMainTab);
        settingsButton.onClick.AddListener(ShowSettingsScreen);

        Player.OnChangeCoins += RefreshVisual;

        foreach (var vcell in fighterCells)
        {
            vcell.Initialize();
            vcell.OnChoosed += (c) => ChooseFighter(c);
        }

        var cell = Array.Find(fighterCells, e => e.FighterName == currentChoosedFighter);
        cell = cell ?? fighterCells.FirstObject();
        ChooseFighter(cell);
        RefreshVisual();

        ShowMainTab();
    }


    public override void Hide(Result result = null)
    {
        play.onClick.RemoveListener(OnClickClose);
        chooseButton.onClick.RemoveListener(ShowChooseTab);
        backButton.onClick.RemoveListener(ShowMainTab);
        settingsButton.onClick.RemoveListener(ShowSettingsScreen);

        Player.OnChangeCoins -= RefreshVisual;

        foreach (var cell in fighterCells)
        {
            cell.Deinitialize();
        }
        
        base.Hide(result);
    }


    protected override void Hided(Result result = null)
    {
        ClearAbilitiesCells();

        result = new Result
        {
            ChoosedFighterName = currentChoosedFighter
        };

        base.Hided(result);
    }


    private void OnClickClose()
    {
        Hide();
    }


    private void RefreshVisual()
    {
        textCoins.text = Player.Coins.ToString();
    }


    private void ChooseFighter(UIHeroCell cell)
    {
        Fighter.Name name = cell.FighterName;

        foreach (var c in fighterCells)
        {
            c.MarkChoosed(name == c.FighterName);
        }

        currentChoosedFighter = name;

        characterAnimation.skeletonDataAsset = Fighters.GetVisualData(name).skeletonDataAsset;
        characterAnimation.Initialize(true);
        characterAnimation.AnimationState.SetAnimation(default, Fighters.GetVisualData(name).idleAnimation, true);

        ClearAbilitiesCells();

        foreach (var ability in Fighters.GetAbilityData(currentChoosedFighter).Abilities)
        {
            var abilityCell = Instantiate(abilityCellPrefab, abilityCellsRoot);
            abilityCell.Initialize(ability.AbilityName);

            abilityCells.Add(abilityCell);
        }

        ShowMainTab();
        mainName.text = currentChoosedFighter.ToString().ToUpper();
    }


    private void ShowMainTab()
    {
        mainTab.gameObject.SetActive(true);
        chooseTab.gameObject.SetActive(false);
    }


    private void ShowChooseTab()
    {
        mainTab.gameObject.SetActive(false);
        chooseTab.gameObject.SetActive(true);
    }


    private void ShowSettingsScreen() =>
        UiScreenManager.Instance.ShowScreen(Fighter5011.ScreenType.Settings);
    

    private void ClearAbilitiesCells()
    {
        foreach (var cell in abilityCells)
        {
            cell.Deinitialize();
            Destroy(cell.gameObject);
        }

        abilityCells.Clear();
    }
}
