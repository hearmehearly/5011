﻿using Fighter5011.Helpers;
using Fighter5011.Ui;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class UIHeroCell : MonoBehaviour
{
    public event Action<UIHeroCell> OnChoosed;

    [SerializeField] private Fighter.Name fighterName = default;

    [SerializeField] private Button chooseButton = default;

    [SerializeField] private GameObject progressRoot = default;

    [SerializeField] private GameObject buyRoot = default;
    [SerializeField] private TMP_Text price = default;

    private bool isChoosed;

    public Fighter.Name FighterName => fighterName;

    public void Initialize()
    {
        chooseButton.onClick.AddListener(OnChoose);
        
    }


    public void Deinitialize()
    {
        chooseButton.onClick.RemoveListener(OnChoose);

        OnChoosed = null;
    }


    public void MarkChoosed(bool value)
    {
        isChoosed = value;
        RefreshVisual();
    }
    

    private void RefreshVisual()
    {
        buyRoot.SetActive(!Player.IsFighterOpen(fighterName));
        progressRoot.SetActive(false && isChoosed && Player.IsFighterOpen(fighterName));

        price.text = PlayerConfig.GetPrice(fighterName).ToShortFormat();
    }


    private void Buy()
    {
        if (Player.TryRemoveCoins(PlayerConfig.GetPrice(fighterName)))
        {
            Player.OpenFighter(fighterName);
            OnChoose();
        }
        else
        {
            UiScreenManager.Instance.ShowPopup(OkPopupType.NotEnoughCurrency);
        }
    }


    private void OnChoose()
    {
        if (!Player.IsFighterOpen(fighterName))
        {
            Buy();
        }
        else
        {
            OnChoosed?.Invoke(this);
        }
    }
}
