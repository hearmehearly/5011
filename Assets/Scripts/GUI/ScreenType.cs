﻿namespace Fighter5011
{
    public enum ScreenType
    {
        None                = 0,
        MainMenu            = 1,
        Ingame              = 2,
        Result              = 3,

        Settings            = 5,
        Tutorial            = 6,
        SkinScreen          = 8,
        OkayScreen          = 9,
        RateUsScreen        = 10,
        Roulette            = 11,
        ShopResult          = 12,
        PauseScreen         = 13,
        WeaponSkinScreen    = 14,
        TutorialPopup = 15
    }
}
