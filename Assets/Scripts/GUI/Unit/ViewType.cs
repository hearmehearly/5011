﻿namespace Fighter5011
{
    public enum ViewType
    {
        None    = 0,
        Screen  = 1,
        Popup   = 2,
    }
}
