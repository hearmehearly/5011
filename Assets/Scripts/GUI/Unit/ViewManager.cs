﻿using Fighter5011.Ui;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Fighter5011
{
    public static class ViewManager
    {
        public static Action OnViewInfosChanged;

        private const float ZOffset = 100f;
        private const int OrderOffset = 10;

        private const int DirectionMultiplier = -1;

        private const int AdditionalSortingOrderValueForOverlay = 1;
        private const int AdditionalZDistanceForOverlay = 1;

        private static readonly List<AnimatorView> views = new List<AnimatorView>();
        
        public static int SortingOrderForOverlay { get; private set; }

        public static float ZDistanceForOverlay { get; private set; }
        
        public static void AddViewInfo(AnimatorView viewInfo)
        {
            views.Add(viewInfo);

            RecalculateViewInfos();            
        }


        public static void RemoveLastViewInfo(AnimatorView animatorView)
        {
            bool isContain = views.Contains(animatorView);

            if (isContain)
            {
                views.Remove(animatorView);
            }

            RecalculateViewInfos();
        }


        static float CalculateZPosition(int index) => ZOffset * index * DirectionMultiplier;


        static int CalculateSortingOrder(int itemIndex) => OrderOffset * itemIndex;


        static void RecalculateViewInfos()
        {
            views.RemoveAll(view => view.IsNull());
                        
            for (int i = 0; i < views.Count; i++)
            {
                int order = i + 1;

                views[i].SortingOrder = CalculateSortingOrder(order);
                views[i].ZPosition = CalculateZPosition(order);

                views[i].SetVisualOrderSettings();
            }

            if (views.Count > 0)
            {
                SortingOrderForOverlay = views.Max(element => element.SortingOrder) + AdditionalSortingOrderValueForOverlay;
                ZDistanceForOverlay = views.Min(element => element.ZPosition) + (AdditionalZDistanceForOverlay * DirectionMultiplier);
            }
        }
    }
}
