﻿using Fighter5011.Ui;
using System;
using UnityEngine;
using UnityEngine.UI;


public class UISocialPopup : UITweensUnit<UnitResult>
{
    public static readonly ResourceGameObject<UISocialPopup> Prefab = new ResourceGameObject<UISocialPopup>("Game/UI/UISocialPopup");
    [SerializeField]
    private Button closeButton = null;
    [SerializeField]
    private Button openLinkButton = null;

    private float reward;


    public void Show(float rewardValue, Action<UnitResult> onHided)
    {
        Show(onHided);

        reward = rewardValue;
        openLinkButton.onClick.AddListener(OpenLink);
        closeButton.onClick.AddListener(Close);
    }


    public override void Hide(UnitResult result = null)
    {
        base.Hide(result);

        openLinkButton.onClick.RemoveListener(OpenLink);
        closeButton.onClick.RemoveListener(Close);
    }


    private void OpenLink()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            UiScreenManager.Instance.ShowPopup(OkPopupType.NoInternet);
            return;
        }

        Application.OpenURL(SocialController.URL);

        SocialController.TakeReward();
        Hide();
    }


    private void Close()
    {
        Hide();
    }
}
