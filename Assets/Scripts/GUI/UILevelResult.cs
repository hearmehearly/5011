﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Fighter5011.Helpers;


namespace Fighter5011.Ui
{
    public class UILevelResult : AnimatorScreen
    {
        private const float BonusMultiplier = 2.0f;

        [SerializeField]
        private TMP_Text resultReward = null;
        [SerializeField] private Button claimReward = null;
        [SerializeField] private Button claimRewardBonus = null;

        private ArenaResult arenaResult;



        public override ScreenType ScreenType => ScreenType.Result;

        protected override string ShowKey => AnimationKeys.Screen.Show;

        protected override string HideKey => AnimationKeys.Screen.Hide;


        public override void Show()
        {
            base.Show();

            claimReward.onClick.AddListener(ClaimReward);
            claimRewardBonus.onClick.AddListener(ClaimBonus);
        }


        public override void Hide()
        {
            claimReward.onClick.RemoveListener(ClaimReward);
            claimRewardBonus.onClick.RemoveListener(ClaimBonus);

            // todo fast for change backs
            Player.UpLevel();

            base.Hide();
        }


        public void ClaimReward()
        {
            Player.AddCoins(arenaResult.Coins);
            Hide();
        }


        private void ClaimBonus()
        {
            UnityAds.ShowVideo((result) =>
            {
                switch (result)
                {
                    case VideoResult.NoInternet:
                        UiScreenManager.Instance.ShowPopup(OkPopupType.NoInternet);
                        break;
                    case VideoResult.NoAds:
                        UiScreenManager.Instance.ShowPopup(OkPopupType.NoVideo);
                        break;
                    case VideoResult.Viewed:
                        arenaResult.Coins *= BonusMultiplier;
                        RefreshVisual();
                        ClaimReward();
                        break;

                    default:
                        break;
                }
            });
        }

        public void SetupResult(ArenaResult _arenaResult)
        {
            arenaResult = _arenaResult;
            
            RefreshVisual();
        }


        private void RefreshVisual()
        {
            resultReward.text = string.Concat("+", arenaResult.Coins.ToShortFormat());
        }
    }
}
