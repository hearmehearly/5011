﻿using System;
using UnityEngine;
using UnityEngine.UI;


public class UITrainingRoom : UITweensUnit<UnitResult>
{
    [Serializable]
    public class InputButton
    {
        public Button button = default;
        public InputField inputField = default;
    }


    [Serializable]
    public class SwitchCharacterButton
    {
        public Button button = default;
        public Text characterText = default;
    }

    public static readonly ResourceGameObject<UITrainingRoom> Prefab = new ResourceGameObject<UITrainingRoom>("Game/UI/UITrainingRoom");

    public static event Action<Fighter.Name> OnShouldSwitchFighter;
    public static event Action<ControlType> OnShouldApplyControlFighter;
    public static event Action OnShouldDispel;
    public static event Action<float> OnShouldApplyDamage;

    [Header("Top Panel")]
    [SerializeField]
    private Button returnToIntroScene = default;

    [SerializeField]
    private Dropdown fightersDropdown = default;
    [SerializeField]
    private Dropdown controlsDropdown = default;
    [SerializeField]
    private Button dispelButton = default;
    [SerializeField]
    private InputButton getDamageInput = default;
    [SerializeField]
    private Button spawnWeakTargetButton = default;


    [Header("Bottom Panel")]
    [SerializeField]
    private Text lastDamageText = default;
    [SerializeField]
    private Text lastComboText = default;
    [SerializeField]
    private Text velocityText = default;

    private NPCFighter targetFighter;
    private Fighter playerFighter;


    public void InitializeTarget(Fighter target)
    {
        targetFighter = target as NPCFighter;
    }


    public void InitializePlayer(Fighter target)
    {
        playerFighter = target;
    }


    public override void Show(Action<UnitResult> onHided = null, Action onShowed = null)
    {
        base.Show(onHided, onShowed);

        returnToIntroScene.onClick.AddListener(Close);
        dispelButton.onClick.AddListener(TriggerDispelEvent);
        getDamageInput.button.onClick.AddListener(TriggerDamageEvent);
        spawnWeakTargetButton.onClick.AddListener(TriggerSpawnTargetEvent);

        InitializeCharacterButtons();
        InitializeControlButtons();


        void InitializeCharacterButtons()
        {
            fightersDropdown.onValueChanged.AddListener(SwitchFighter);
            fightersDropdown.ClearOptions();

            foreach (var config in Fighters.Configs)
            {
                fightersDropdown.options.Add(new Dropdown.OptionData(config.Name.ToString()));
            }

            SwitchFighter(default);
        }


        void InitializeControlButtons()
        {
            controlsDropdown.onValueChanged.AddListener(TriggerApplyControlEvent);
            controlsDropdown.ClearOptions();

            foreach (var controlType in Enum.GetValues(typeof(ControlType)))
            {
                controlsDropdown.options.Add(new Dropdown.OptionData(controlType.ToString()));
            }
        }
    }


    public override void Hide(UnitResult result = null)
    {
        base.Hide(result);
        
        returnToIntroScene.onClick.RemoveListener(Close);
        controlsDropdown.onValueChanged.RemoveListener(TriggerApplyControlEvent);
        fightersDropdown.onValueChanged.RemoveListener(SwitchFighter);
        dispelButton.onClick.RemoveListener(TriggerDispelEvent);
        getDamageInput.button.onClick.RemoveListener(TriggerDamageEvent);
        spawnWeakTargetButton.onClick.RemoveListener(TriggerSpawnTargetEvent);
    }


    public void SetLastDamage(float damage)
    {
        lastDamageText.text = damage.ToString();
    }
    

    public void SetLastCombo(int combo)
    {
        lastComboText.text = combo.ToString();
    }


    public void SetVelocity(Vector2 velocity)
    {
        velocityText.text = velocity.ToString();
    }


    private void Close()
    {
        Hide();
    }


    private void TriggerDispelEvent()
    {
        OnShouldDispel?.Invoke();
    }


    private void TriggerDamageEvent()
    {
        if (float.TryParse(getDamageInput.inputField.text, out float damage))
        {
            OnShouldApplyDamage?.Invoke(damage);
        }
    }


    private void TriggerSpawnTargetEvent()
    {
        TrainingRoomManager.Instance.SpawnWeakTarget();
    }


    private void SwitchFighter(int num)
    {
        Fighter.Name name = Fighters.Configs[num].Name;

        OnShouldSwitchFighter?.Invoke(name);
    }


    private void TriggerApplyControlEvent(int control)
    {
        OnShouldApplyControlFighter?.Invoke((ControlType)control);
        controlsDropdown.SetValueWithoutNotify(0);
    }


    private void Update()
    {
        // movement
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            targetFighter.MovementController.MoveLeft();
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            targetFighter.MovementController.MoveRight();
        }
        else if(Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow))
        {
            targetFighter.MovementController.Stop();
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            targetFighter.CombatController.Attack();
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            targetFighter.MovementController.Jump();
        }
        // abilities
        if (Input.GetKeyDown(KeyCode.Z))
        {
            targetFighter.AbilityController.OnPressAbility(targetFighter.AbilityController.currentAbilities[0]);
        }
        else if (Input.GetKeyDown(KeyCode.X))
        {
            targetFighter.AbilityController.OnPressAbility(targetFighter.AbilityController.currentAbilities[1]);
        }
        else if (Input.GetKeyDown(KeyCode.C))
        {
            targetFighter.AbilityController.OnPressAbility(targetFighter.AbilityController.currentAbilities[2]);
        }
        else if (Input.GetKeyDown(KeyCode.V))
        {
            targetFighter.AbilityController.OnPressAbility(targetFighter.AbilityController.currentAbilities[3]);
        }

        // Solutions

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            targetFighter.FighterSolutions.MoveRight();
        }
        else if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            targetFighter.FighterSolutions.MoveLeft();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            targetFighter.FighterSolutions.Stop();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            targetFighter.FighterSolutions.Jump();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            targetFighter.FighterSolutions.JumpRight();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            targetFighter.FighterSolutions.JumpLeft();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            targetFighter.FighterSolutions.ChargeRight();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            targetFighter.FighterSolutions.ChargeLeft();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            targetFighter.FighterSolutions.Attack();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            targetFighter.FighterSolutions.SeatDown();
        }
        else if (Input.GetKeyUp(KeyCode.Alpha0))
        {
            targetFighter.FighterSolutions.StandUp();
        }
    }
}
