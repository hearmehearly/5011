﻿using UnityEngine;


public class EffectExample : MonoBehaviour
{
    [SerializeField] private ParticleSystem effectPrefab = default;


    public void PlayEffect()
    {
        ParticleSystem effect = Instantiate(effectPrefab, transform.position, Quaternion.identity, transform);
        effect.Play(true);
        effect.Stop();
    }
}
