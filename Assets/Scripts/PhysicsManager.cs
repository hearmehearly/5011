﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class PhysicsManager : MonoBehaviour
{
    class RigidbodyDistanceData
    {
        public float MaxDistance = default;
        public Action OnMaxDistanceReachedCallback = default;

        public float CurrentDistance = default;

        public Rigidbody2D MainRigidbody = default;
        public Vector3 InitialPosition = default;
    }

    public static PhysicsManager Instance { get; private set; }

    private readonly Dictionary<Rigidbody2D, RigidbodyDistanceData> rigidbodyDistanceData = new Dictionary<Rigidbody2D, RigidbodyDistanceData>();
    private KeyValuePair<Rigidbody2D, RigidbodyDistanceData>[] itemsToRemove;


    private void Awake()
    {
        Instance = this;
    }


    private void FixedUpdate()
    {
        foreach (var data in rigidbodyDistanceData)
        {
            if (data.Key == null)
            {
                Debug.Log("CHECK ADDING. IT WAS ITEM WITH CURRENT DISTANCE " + data.Value.CurrentDistance);
            }
            else
            {
                data.Value.CurrentDistance += Time.fixedDeltaTime * Mathf.Abs(data.Key.velocity.x);
            }
        }

        itemsToRemove = rigidbodyDistanceData
                            .Where(element => element.Value.CurrentDistance > element.Value.MaxDistance || element.Value.MainRigidbody.position.x > (element.Value.InitialPosition.x + element.Value.MaxDistance)) // check ||
                            .ToArray();

        foreach (var item in itemsToRemove)
        {
            item.Value.OnMaxDistanceReachedCallback?.Invoke();
            rigidbodyDistanceData.Remove(item.Key);
        }
    }


    public void AddDistanceRigidbody(Rigidbody2D rigidbody, float maxDistance, Action onMaxDistanceReachedCallback)
    {
        RigidbodyDistanceData dataToAdd = new RigidbodyDistanceData { MainRigidbody = rigidbody, MaxDistance = maxDistance, InitialPosition = rigidbody.position, OnMaxDistanceReachedCallback = onMaxDistanceReachedCallback };

        if (rigidbodyDistanceData.ContainsKey(rigidbody))
        {
            Debug.LogWarning("rigidbodyDistanceData already contains key: " + rigidbody);
        }
        else
        {
            rigidbodyDistanceData.Add(rigidbody, dataToAdd);
        }
    }


    public void RemoveDistanceRigidbody(Rigidbody2D mainRigidbody)
    {
        rigidbodyDistanceData.Remove(mainRigidbody);
    }


    public List<Fighter> OverlapFightersInRadius(Vector3 position, float radius, Fighter exceptedFighter = null)
    {
        List<Fighter> result = new List<Fighter>();

        Collider2D[] overlapColliders = Physics2D.OverlapCircleAll(position, radius);

        if (overlapColliders != null)
        {
            foreach (var collider in overlapColliders)
            {
                Fighter collidedFighter = collider.GetComponent<Fighter>();

                if (collidedFighter != null && !collidedFighter.Equals(exceptedFighter))
                {
                    result.Add(collidedFighter);
                }
            }
        }

        return result;
    }


    public (float, Fighter) DistanceAndEnemyToFaceFighter(Fighter owner)
    {
        float result = float.MaxValue;

        List<Fighter> nearFighters = RaycastFightersInFaceArea(5.0f, owner);
        var fighter = nearFighters.Find(element => (element as Fighter != null));
        result = fighter == null ? result : Vector2.Distance(owner.MovementController.MainRigidbody.position, fighter.MovementController.MainRigidbody.position);

        return (result, fighter);
    }
    

    public (float, Fighter) DistanceAndEnemyToBackFighter(Fighter owner)
    {
        float result = float.MaxValue;
        
        List<Fighter> nearFighters = RaycastFightersInBackArea(5.0f, owner);
        var fighter = nearFighters.Find(element => (element as Fighter != null));
        result = fighter == null ? result : Vector2.Distance(owner.MovementController.MainRigidbody.position, fighter.MovementController.MainRigidbody.position);

        return (result, fighter);
    }


    public List<Fighter> RaycastFightersInFace(Fighter owner = null)
    {
        List<Fighter> result = new List<Fighter>(3);

        if (owner == null)
        {
            return result;
        }

        RaycastHit2D[] hits = Physics2D.RaycastAll(owner.transform.position, owner.transform.right * owner.MovementController.VelocityMultiplier);

        foreach (var hit in hits)
        {
            Fighter fighter = hit.transform.GetComponent<Fighter>();

            if (fighter != null && !fighter.Equals(owner))
            {
                result.Add(fighter);
            }
        }

        return result;
    }


    public List<Fighter> RaycastFightersInFaceArea(float radius, Fighter owner)
    {
        List<Fighter> result = new List<Fighter>();

        if (owner == null)
        {
            return result;
        }

        int layerMask = 1 << 8;

        Vector2 boxSize = Vector2.one.SetY(5f);
        RaycastHit2D[] hits = Physics2D.BoxCastAll(owner.transform.position, boxSize, default, owner.MovementController.SideVector, layerMask);

        foreach (var hit in hits)
        {
            Fighter fighter = hit.transform.GetComponent<Fighter>();

            if (fighter != null && !fighter.Equals(owner))
            {
                result.Add(fighter);
            }
        }

        return result;
    }


    public List<Fighter> RaycastFightersInBackArea(float radius, Fighter owner)
    {
        List<Fighter> result = new List<Fighter>();

        if (owner == null)
        {
            return result;
        }

        int layerMask = 1 << 8;

        Vector2 boxSize = Vector2.one.SetY(5f);
        RaycastHit2D[] hits = Physics2D.BoxCastAll(owner.transform.position, boxSize, default, owner.MovementController.BackVector, layerMask);

        foreach (var hit in hits)
        {
            Fighter fighter = hit.transform.GetComponent<Fighter>();

            if (fighter != null && !fighter.Equals(owner))
            {
                result.Add(fighter);
            }
        }

        return result;
    }
}
