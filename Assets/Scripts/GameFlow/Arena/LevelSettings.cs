﻿using UnityEngine;


[CreateAssetMenu]
public class LevelSettings : ScriptableObject
{
    public string Name = default;
    public GameObject Background = default;
    public float CoinsForWin = default;
    public NPCFightersController.Settings NPCFightersControllerSettings = default;

    [Header("Duration Settings")]
    public float BaseDuration = default;
    public float AdditionalDurationForFirstKill = default;
    public float NoKillsLoseDuration = default;
}
