﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BattleResult
{
    None = 0,
    Lose = 1,
    Draw = 2,
    Win = 3
}


public class ArenaResult : UnitResult
{
    public BattleResult battleResult = default;
    public float Coins = default;
}
