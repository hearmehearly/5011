﻿using UnityEngine;


[CreateAssetMenu]
public class TutorialLevelSettings : ScriptableObject
{
    public string Name = default;
    public GameObject Background = default;
    public float CoinsForWin = default;
    public NPCFightersController.Settings NPCFightersControllerSettings = default;
}