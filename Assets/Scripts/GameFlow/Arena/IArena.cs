﻿using System;
using System.Collections.Generic;
using UnityEngine;


public interface IArena
{
    void Show(UIPlayerMenu.Result result, Action<ArenaResult> onHided, Action onShowed = null);
}