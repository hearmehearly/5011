﻿using UnityEngine;


[CreateAssetMenu]
public class Levels : ScriptableObject
{
    private static readonly ResourceAsset<Levels> asset = new ResourceAsset<Levels>("Game/Levels");

    [SerializeField] private LevelSettings[] configs = default;
    public TutorialLevelSettings tutorialLevelSettings = default;
    
    public static TutorialLevelSettings TutorialLevelSettings =>
        asset.Value.tutorialLevelSettings;

    public static GameObject GetLevelBackground(int level)
    {
        int index = level % asset.Value.configs.Length;
        return asset.Value.configs[index].Background;
    }


    public static int GetLevelsCount()
    {
        return asset.Value.configs.Length;
    }


    public static float GetCoinsForWin(int level)
    {
        int index = level % asset.Value.configs.Length;
        return asset.Value.configs[index].CoinsForWin;
    }


    public static float GetLevelDuration(int level)
    {
        int index = level % asset.Value.configs.Length;
        return asset.Value.configs[index].BaseDuration;
    }


    public static float GetAdditionalLevelDuration(int level)
    {
        int index = level % asset.Value.configs.Length;
        return asset.Value.configs[index].AdditionalDurationForFirstKill;
    }


    public static float GetLoseIfNoKillsLevelDuration(int level)
    {
        int index = level % asset.Value.configs.Length;
        return asset.Value.configs[index].NoKillsLoseDuration;
    }

    public static NPCFightersController.Settings GetNPCSettings(int level)
    {
        int index = level % asset.Value.configs.Length;
        return asset.Value.configs[index].NPCFightersControllerSettings;
    }
}
