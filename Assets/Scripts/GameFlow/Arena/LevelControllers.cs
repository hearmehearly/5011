﻿public class LevelControllers
{
    public NPCFightersController NPCFightersController = default;
    public ArenaTimerController ArenaTimerController = default;
    public ArenaFightersController ArenaFightersController = default;
    public ArenaRunesController ArenaRunesController = default;
    public ArenaOutOfZoneController ArenaOutOfZoneController = default;


    public LevelControllers()
    {
        NPCFightersController = new NPCFightersController();
        ArenaTimerController = new ArenaTimerController();
        ArenaFightersController = new ArenaFightersController();
        ArenaRunesController = new ArenaRunesController();
        ArenaOutOfZoneController = new ArenaOutOfZoneController();
    }


    public void Initialize()
    {
        NPCFightersController.Initialize();
        ArenaTimerController.Initialize();
        ArenaFightersController.Initialize();
        ArenaRunesController.Initialize();
        ArenaOutOfZoneController.Initialize();
    }


    public void Deinitialize()
    {
        NPCFightersController.Deinitialize();
        ArenaTimerController.Deinitialize();
        ArenaFightersController.Deinitialize();
        ArenaRunesController.Deinitialize();
        ArenaOutOfZoneController.Deinitialize();
    }
}
