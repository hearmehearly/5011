﻿using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Fighter5011.Ui;


public class Arena : Unit<ArenaResult>, IArena
{
    public enum State
    {
        Playing,
        Win,
        Lose,
        Draw
    } 

    public const float Border = 52.0f;
    public const float TopMaxBorder = 15.0f;

    private readonly Dictionary<GameObject, GameObject> backs = new Dictionary<GameObject, GameObject>();

    private Action<float> OnFighterWin;

    private LevelControllers levelControllers;

    private PlayerFighter playerFighter;



    private readonly List<Fighter> currentFighters = new List<Fighter>();

    private State CurrentState
    {
        get;
        set;
    }


    public bool IsDrawStateDetected => !currentFighters.Any(element => element.KillsCounter != currentFighters.First().KillsCounter);





    public void Initialize()
    {
        levelControllers = new LevelControllers();
    }


    public void Show(UIPlayerMenu.Result result, Action<ArenaResult> onHided, Action onShowed = null)
    {
        Show(onHided, onShowed);

        HideBack();
        ShowBack(Levels.GetLevelBackground(Player.Level));

        levelControllers.Initialize();
        levelControllers.NPCFightersController.SetupSettings(Levels.GetNPCSettings(Player.Level));

        List<Fighter> createdFighters = levelControllers.NPCFightersController.CreateLevelNPCs();

        foreach (var npc in createdFighters)
        {
            (npc as NPCFighter)?.SetAIEnabled(true);
        }

        if (playerFighter != null)
        {
            Destroy(playerFighter.gameObject);
        }

        playerFighter = ContentStorage.CreatePlayerFighter(levelControllers.ArenaFightersController.RandomRespawnPosition);

        playerFighter.SetOnWinCallback(() => OnFighterWin(200));

        playerFighter.Initialize(result.ChoosedFighterName);
        playerFighter.SetupCamera(GameManager.Instance.GameCamera);


        playerFighter.StartLevel();

        levelControllers.ArenaTimerController.SetupArena(this);
        levelControllers.ArenaTimerController.AddOnTimeOutCallback(FinishLevel);

        levelControllers.ArenaTimerController.StartTimer();

        currentFighters.Clear();
        currentFighters.Add(playerFighter);
        currentFighters.AddRange(createdFighters);


        UILevel uiLevel = UiScreenManager.Instance.LoadedScreen<UILevel>(Fighter5011.ScreenType.Ingame);
        uiLevel.UILevelCounter.InitializeKillsCounter(currentFighters);
        uiLevel.OnShouldFinishLevel += FinishLevel;

        OnFighterWin = OnLevelCompleted;

        foreach (var fighter in currentFighters)
        {
            fighter.CombatController.OnGottaKill += IncrementKillsCounter;
            fighter.AddOnDieCallback(levelControllers.ArenaFightersController.RespawnFighter);
        }

        levelControllers.ArenaOutOfZoneController.SetupFighters(currentFighters);
    }


    private void IncrementKillsCounter(Fighter fighter, Fighter fighterToKill)
    {
        levelControllers.ArenaTimerController.MarkFighterDeath();

        UILevel uiLevel = UiScreenManager.Instance.LoadedScreen<UILevel>(Fighter5011.ScreenType.Ingame);
        uiLevel.UILevelCounter.SetKillsCount(fighter, fighter.KillsCounter);
    }


    protected override void Hided(ArenaResult result)
    {
        CurrentState = State.Playing;
        levelControllers.NPCFightersController.DestroyLevelNPCs();
        levelControllers.Deinitialize();

        base.Hided(result);
    }


    private void OnLevelCompleted(float coinsAmount)
    {
        CurrentState = State.Win;

        FinishLevel();
    }


    private void FinishLevel()
    { // find out who win
        // maybe deinitialize all

        UILevel uiLevel = UiScreenManager.Instance.LoadedScreen<UILevel>(Fighter5011.ScreenType.Ingame);
        uiLevel.UILevelCounter.Deinitialize();
        uiLevel.OnShouldFinishLevel -= FinishLevel;

        foreach (var fighter in currentFighters)
        {
            fighter.FinishLevel();
            fighter.CombatController.OnGottaKill -= IncrementKillsCounter;

            fighter.Deinitialize();
            Destroy(fighter.gameObject);
        }

        uiLevel.Hide();
        
        BattleResult battleResult = default;

        if (IsDrawStateDetected)
        {
            battleResult = BattleResult.Draw;
        }
        else
        {
            Fighter winner = currentFighters.OrderByDescending(element => element.KillsCounter).FirstOrDefault();

            battleResult = playerFighter.Equals(winner) ? BattleResult.Win : BattleResult.Lose;
        }

        float coins = PlayerConfig.GetResultCoins(battleResult);

        Hided(new ArenaResult
        {
            battleResult = battleResult,
            Coins = coins
        });
    }


    private void ShowBack(GameObject prefab)
    {
        if (!backs.ContainsKey(prefab))
        {
            backs[prefab] = Instantiate(prefab, transform);
        }

        GameObject back = backs[prefab];
        back.SetActive(true);
    }


    private void HideBack()
    {
        foreach (GameObject back in backs.Values)
        {
            back.SetActive(false);
        }
    }
}
