﻿using System;
using UnityEngine;


public class ParallaxBackground : MonoBehaviour
{
    [Serializable]
    private class Body
    {
        public Transform body = default;
        [NonSerialized]
        public Vector3 InitialPosition = default;
    }

    [Serializable]
    private class Layer
    {
        public float OffsetX = default;
        public float OffsetY = default;
        public Body[] Bodies = default;
    }


    [SerializeField]
    private Layer[] layers = default;

    private Vector3 layerPosition; // to avoid allocations in Update()


    private void Awake()
    {
        RefreshInitialValues();
    }


    private void Update()
    {
        foreach (Layer layer in layers)
        {
            float layerDistanceX = Arena.Border * layer.OffsetX;
            float layerDistanceY = Arena.TopMaxBorder * layer.OffsetY;

            foreach (var body in layer.Bodies)
            {
                layerPosition = body.body.position;
                layerPosition.x = body.InitialPosition.x + PlayerFighter.Offset.x * layerDistanceX;
                layerPosition.y = body.InitialPosition.y + PlayerFighter.Offset.y * layerDistanceY;

                body.body.position = layerPosition;
            }
        }
    }


    public void RefreshInitialValues()
    {
        foreach (Layer layer in layers)
        {
            foreach (var body in layer.Bodies)
            {
                body.InitialPosition = body.body.position;
            }
        }
    }
}
