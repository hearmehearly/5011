﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class BlackHole : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerFighter>())
        {
            UITrainingRoom.Prefab.Instance.Hide();
        }
    }
}
