﻿using System;
using System.Collections.Generic;


public class ArenaRunesController : ArenaController
{
    private RunesSettings settings;
    
    private readonly List<RuneObject> runesOnArena = new List<RuneObject>();

    public override void Initialize()
    {
        settings = IngameData.asset.Value.Settings.RunesSettings;
        
        TaskInvoker.Instance.CallMethodWithDelay(this, SpawnMainRune, settings.MainRuneSpawnDelay);
    }

    private void SpawnMainRune() =>
        CreateRandomRune();
    
    public override void Deinitialize()
    {
        TaskInvoker.Instance.UnscheduleAllMethodForTarget(this);

        foreach (var runeObject in runesOnArena)
        {
            runeObject.OnApplied -= DestroyRune;

            runeObject.Deinitialize();
            ContentStorage.Destroy(runeObject.gameObject);
        }

        runesOnArena.Clear();
    }

    private void CreateRandomRune()
    {
        var data = settings.FindRandomSpawnData();

        var alreadyCreatedRune = runesOnArena.Find(e => Array.Exists(data.positions, dpos => dpos == e.transform.position));
        if (alreadyCreatedRune != null)
        {
            DestroyRune(alreadyCreatedRune);
        }

        var runeObject = ContentStorage.CreateRuneObject(data.types.RandomObject(), data.positions.RandomObject());
        runeObject.Initialize();

        runeObject.OnApplied += DestroyRune;

        runesOnArena.Add(runeObject);
    }


    public void DestroyRune(RuneObject runeObject)
    {
        runesOnArena.Remove(runeObject);

        runeObject.OnApplied -= DestroyRune;

        runeObject.Deinitialize();
        ContentStorage.Destroy(runeObject.gameObject);


        TaskInvoker.Instance.CallMethodWithDelay(this, SpawnMainRune, settings.MainRuneSpawnDelay);
    }
}
