﻿using System.Collections.Generic;
using UnityEngine;


public class ArenaOutOfZoneController : ArenaController
{
    private static readonly Vector2 gameRectSize = Vector2.one * 100f;
    private static readonly Rect gameRect = new Rect(-gameRectSize * 0.5f, gameRectSize);

    private IList<Fighter> fighters;

    public override void Initialize()
    {
        MonoBehaviourLifecycle.OnUpdate += MonoBehaviourLifecycle_OnUpdate;
    }


    public override void Deinitialize()
    {
        MonoBehaviourLifecycle.OnUpdate -= MonoBehaviourLifecycle_OnUpdate;
    }

    public void SetupFighters(IList<Fighter> _fighters)
    {
        fighters = _fighters;
    }


    private void MonoBehaviourLifecycle_OnUpdate(float deltaTime)
    {
        if (fighters == null)
        {
            return;
        }

        foreach (var fighter in fighters)
        {
            if (!gameRect.Contains(fighter.transform.position))
            {
                fighter.Die();
            }
        }
    }
}
