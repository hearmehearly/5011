﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;


public class ArenaFightersController : ArenaController
{

    [Serializable]
    public class Settings
    {
        public float respawnDuration = default;
        public Vector3[] respawnPositions = default;
    }


    private Settings currentSettings;



    public Vector3 RandomRespawnPosition => currentSettings.respawnPositions[Random.Range(0, currentSettings.respawnPositions.Length)];



    public ArenaFightersController()
    {
        currentSettings = IngameData.asset.Value.Settings.FightersControllerSettings;
    }


    public void RespawnFighter(Fighter deadFighter)
    {
        TaskInvoker.Instance.CallMethodWithDelay(this, () =>
        {
            deadFighter.transform.position = RandomRespawnPosition;

            deadFighter.Initialize(deadFighter.CurrentName);

            if (deadFighter is PlayerFighter player)
            {
                player.SetupCamera(GameManager.Instance.GameCamera);
            }

            deadFighter.AddOnDieCallback(RespawnFighter);

            (deadFighter as NPCFighter)?.InitializeAI();
            (deadFighter as NPCFighter)?.SetAIEnabled(true);
            deadFighter.StartLevel();

        }, currentSettings.respawnDuration);
    }


    public override void Initialize()
    {
    }


    public override void Deinitialize()
    {
        TaskInvoker.Instance.UnscheduleAllMethodForTarget(this);
    }
}

