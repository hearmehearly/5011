﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


public class NPCFightersController : ArenaController
{
    [Serializable]
    public struct FighterInfo
    {
        public Vector3 position;
    }

    [Serializable]
    public class Settings
    {
        public int count = default;

        public FighterInfo[] fighterInfos = default;

        public Fighter.Name[] allowFighters = default;

        public Fighter.Name GenerateFighterName() =>
            allowFighters.RandomObject();
    }


    private Settings currentSettings;

#warning move in correct place
    public Vector3 RandomSpawnPosition => currentSettings.fighterInfos[Random.Range(0, currentSettings.fighterInfos.Length)].position;


    public override void Initialize()
    {
    }

    public override void Deinitialize()
    {
    }


    public void SetupSettings(Settings settings) =>
        currentSettings = settings;



    public List<Fighter> CreateLevelNPCs()
    {
        List<Fighter> createdFighters = new List<Fighter>(currentSettings.fighterInfos.Length);

        for (int i = 0; i < currentSettings.count; i++)
        {
            Vector3 position = currentSettings.fighterInfos[i].position;
            Fighter fighter = ContentStorage.CreateNPCFighter(position);

            Fighter.Name fighterName = currentSettings.GenerateFighterName();
            fighter.Initialize(fighterName);

            fighter.StartLevel();
            (fighter as NPCFighter)?.InitializeAI();

            createdFighters.Add(fighter);
        }

        return createdFighters;

    }


    public void DestroyLevelNPCs()
    {
        Debug.Log("DestroyLevelNPCs");
    }
}
