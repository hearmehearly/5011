﻿using System;
using System.Collections;
using UnityEngine;


public class ArenaTimerController : ArenaController
{
    public static event Action<float> OnTimeChanged;
    public static event Action OnAdditionalTimeAdded; 

    private float levelTimer;
    private float secondForEnd;
    private bool wasAdditionalTimeAdded;

    private float secondForEndIfNoKills;
    private bool wasAnyFighterKilled;

    private Arena currentArena;

    private Action OnTimeOutCallback;
    private Coroutine timerRoutine;



    public override void Initialize()
    {
        levelTimer = default;
        wasAnyFighterKilled = default;
        wasAdditionalTimeAdded = default;
        secondForEnd = Levels.GetLevelDuration(Player.Level);
        OnTimeChanged?.Invoke(secondForEnd);

        secondForEndIfNoKills = Levels.GetLoseIfNoKillsLevelDuration(Player.Level);
    }


    public override void Deinitialize()
    {
        Scheduler.StopPlayingCoroutine(ref timerRoutine);
        OnTimeOutCallback = null;
        timerRoutine = null;
    }


    public void SetupArena(Arena arena) =>
        currentArena = arena;


    public void StartTimer()
    {
        timerRoutine = Scheduler.PlayCoroutine(LevelTimer());
    }


    public void AddOnTimeOutCallback(Action callback)
    {
        OnTimeOutCallback += callback;
    }



    public void MarkFighterDeath()
    {
        wasAnyFighterKilled = true;
        
        if (wasAdditionalTimeAdded)
        {
            OnTimeOutCallback?.Invoke();
        }
    }


    private IEnumerator LevelTimer()
    {
        while (true)
        {
            levelTimer += Time.deltaTime;

            // if no kills fast end
            if (!wasAnyFighterKilled && levelTimer > secondForEndIfNoKills)
            {
                OnTimeChanged?.Invoke(secondForEnd - levelTimer);
                OnTimeOutCallback?.Invoke();
                break;
            }

            if (levelTimer > secondForEnd)
            {
                // anyway end
                if (wasAdditionalTimeAdded)
                {
                    OnTimeChanged?.Invoke(0);
                    OnTimeOutCallback?.Invoke();
                    break;
                }

                // if firstly >
                if (!wasAdditionalTimeAdded)
                {
                    // if can add
                    if (currentArena.IsDrawStateDetected)
                    {
                        secondForEnd += Levels.GetAdditionalLevelDuration(Player.Level);
                        wasAdditionalTimeAdded = true;

                        OnAdditionalTimeAdded?.Invoke();
                    } // end
                    else
                    {
                        OnTimeChanged?.Invoke(0);
                        OnTimeOutCallback?.Invoke();
                        break;
                    }
                }
            }
            else
            {
                OnTimeChanged?.Invoke(secondForEnd - levelTimer);
            }
            
            yield return null;
        }
    }
}
