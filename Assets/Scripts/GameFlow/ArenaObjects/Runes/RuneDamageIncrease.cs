﻿public class RuneDamageIncrease : Rune
{
    private float addedDamage;

    protected override void OnApply(Fighter fighter)
    {
        addedDamage = settings.runeAdditionalDamage;

        fighter.CombatController.AddAdditionalDamage(addedDamage);
    }


    protected override void OnReset(Fighter fighter)
    {
        fighter.CombatController.RemoveAdditionalDamage(addedDamage);
    }
}
