﻿using Fighter5011;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[CreateAssetMenu(fileName ="RuneSettings", menuName = NamingUtility.MenuItems.IngameSettings + "RuneSettings")]
public class RunesSettings : ScriptableObject
{
    [Serializable]
    private class VisualData
    {
        public RuneType type = default;

        public Sprite sprite = default;
        public float duration = default;

        //public float spawnWeight = default;
    }

    [Serializable]
    public class SpawnData
    {
        public RuneType[] types = default;
        public Vector3[] positions = default;

        //public float minSpawnDelay = default;
        //public float maxSpawnDelay = default;
    }


    [SerializeField] private VisualData[] visualData = default;
    [SerializeField] private SpawnData[] runesSpawnData = default;
    
    [Header("Concrete settings")]

    [Tooltip("после принятия руны скороть будет фактор*предыдущую скроть")]
    public float runeSpeedFactor = default;
    [Tooltip("после принятия руны скороть дамаг будет как дмг+значение")]
    public float runeAdditionalDamage = default;

    [Tooltip("абилки")]
    public float abilitiesReduceCooldownFactor = default;
    [Tooltip("всего вссотановление здоровьяза время действия руны")]
    public float totalHealth = default;
    [Tooltip("количество тиков")]
    public int healthRuneTickCount = default;


    [Header("Эти значения рассчитываютя на основе верхних для руны здоровья")]
    public float healthRuneTickDuration = default;
    public float healthPerTic = default;

    public float MainRuneSpawnDelay => 30f;

    public float FindDuration(RuneType type)
    {
        var data = FindData(type);
        return data == null ? default : data.duration;
    }


    public Sprite FindSprite(RuneType type) =>
        FindData(type)?.sprite;
        

    private VisualData FindData(RuneType type)
    {
        var result = Array.Find(visualData, e => e.type == type);

        if (result == null)
        {
            Debug.Log($"No Data Find for type {type} in {this}");
        }

        return result;
    }


    public SpawnData FindRandomSpawnData()
    {
        var result = runesSpawnData.RandomObject();

        if (result == null)
        {
            Debug.Log($"No Data Find in {this}");
        }

        return result;
    }

    private void OnValidate()
    {
        healthPerTic = healthRuneTickCount == 0 ? default : totalHealth / healthRuneTickCount;
        healthRuneTickDuration = totalHealth == 0 ? default : healthPerTic * FindDuration(RuneType.Health) / totalHealth;
    }
}
