﻿using System;

// если обратишься к RuneController'y у файтера - я тебя ебну потому что архитектура получилась хуйня
public abstract class Rune
{
    public event Action<Rune> OnResetted;

    private Fighter fighter;
    private RuneType runeType;

    private float duration;

    protected RunesSettings settings;


    public void SetFighterOwner(Fighter _owner) =>
        fighter = _owner;


    public void SetRuneType(RuneType _runeType) =>
        runeType = _runeType;
    

    public void Apply()
    {
        settings = IngameData.asset.Value.Settings.RunesSettings;
        
        duration = settings.FindDuration(runeType);
        TaskInvoker.Instance.CallMethodWithDelay(this, Reset, duration);

        OnApply(fighter);
    }


    public void Reset()
    {
        TaskInvoker.Instance.UnscheduleMethod(this, Reset);

        OnReset(fighter);
        OnResetted?.Invoke(this);
    }

    protected abstract void OnApply(Fighter fighter);

    protected abstract void OnReset(Fighter fighter);
}
