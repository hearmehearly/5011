﻿using System.Collections;
using UnityEngine;


public class RuneHealth : Rune
{
    private Coroutine healthRoutine;
    private WaitForSeconds healthTickWait;
    

    protected override void OnApply(Fighter fighter)
    {
        healthTickWait = new WaitForSeconds(settings.healthRuneTickDuration);
        healthRoutine = healthRoutine ?? MonoBehaviourLifecycle.PlayCoroutine(AddHelthRoutine(fighter));
    }

    protected override void OnReset(Fighter fighter)
    {
        MonoBehaviourLifecycle.StopPlayingCorotine(healthRoutine);
        healthRoutine = null;
    }


    private IEnumerator AddHelthRoutine(Fighter fighter)
    {
        float currentAddedHealth = default;

        while (currentAddedHealth < settings.totalHealth)
        {
            fighter.HealthController.AddHealth(settings.healthPerTic);
            currentAddedHealth += settings.healthPerTic;

            yield return healthTickWait;
        }

        healthRoutine = null;
    }
}
