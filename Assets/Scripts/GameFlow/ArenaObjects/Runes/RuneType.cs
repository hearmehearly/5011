﻿public enum RuneType
{
    None        = 0,
    Health      = 1,
    Damage      = 2,
    Haste       = 3,
    Abilities   = 4
}
