﻿using System;
using UnityEngine;


public class RuneObject : MonoBehaviour
{
    public event Action<RuneObject> OnApplied;

    [SerializeField] private SpriteRenderer sprite = default;
    [SerializeField] private CollidableObject collidableObject = default;

    public RuneType RuneType { get; private set; }


    public void Initialize()
    {
        Sprite spriteToSet = IngameData.asset.Value.Settings.RunesSettings.FindSprite(RuneType);
        sprite.sprite = spriteToSet;

        collidableObject.OnCustomCollisionEnter2D += CollidableObject_OnCustomCollisionEnter2D;
    }


    public void Deinitialize()
    {
        collidableObject.OnCustomCollisionEnter2D -= CollidableObject_OnCustomCollisionEnter2D;
    }
    

    public void SetType(RuneType type)
    {
        RuneType = type;
    }


    private void CollidableObject_OnCustomCollisionEnter2D(Collision2D col, CollidableObject.Type arg2)
    {
        Fighter collidedFighter = col.collider.GetComponent<Fighter>();

        if (collidedFighter != null)
        {
            var rune = CreateRune();
            rune.SetRuneType(RuneType);
            rune.SetFighterOwner(collidedFighter);

            collidedFighter.RuneController.Apply(rune);

            OnApplied?.Invoke(this);
        }
    }


    private Rune CreateRune()
    {
        Rune result = default;

        switch (RuneType)
        {
            case RuneType.Health:
                result = new RuneHealth();
                break;
            case RuneType.Damage:
                result = new RuneDamageIncrease();
                break;
            case RuneType.Haste:
                result = new RuneHaste();
                break;
            case RuneType.Abilities:
                result = new RuneAbilities();
                break;
            default:
                Debug.Log($"no logic for rune type {RuneType}");
                break;
        }
        
        return result;
    }
}
