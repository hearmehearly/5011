﻿public class RuneHaste : Rune
{
    private float addedSpeedFactor;

    protected override void OnApply(Fighter fighter)
    {
        float currentSpeedFactor = fighter.MovementController.SpeedFactor;
        addedSpeedFactor = currentSpeedFactor * settings.runeSpeedFactor - currentSpeedFactor;

        fighter.MovementController.SpeedFactor += addedSpeedFactor;
    }

    protected override void OnReset(Fighter fighter)
    {
        fighter.MovementController.SpeedFactor -= addedSpeedFactor;
    }
}
