﻿using System.Collections.Generic;
using System.Linq;


public class RuneAbilities : Rune
{
    private List<ActiveAbility> affectedAbilities = new List<ActiveAbility>();

    protected override void OnApply(Fighter fighter)
    {
        affectedAbilities = fighter.AbilityController.currentAbilities.Where(e => e is ActiveAbility).Select(a => a as ActiveAbility).ToList();

        foreach (var ability in affectedAbilities)
        {
            ability.FastResetCoolDown();
            ability.DecreaseCooldownPercent(settings.abilitiesReduceCooldownFactor);
        }
    }

    protected override void OnReset(Fighter fighter)
    {
        foreach (var ability in affectedAbilities)
        {
            ability.IncreaseCooldownPercent(settings.abilitiesReduceCooldownFactor);
        }
    }
}
