﻿using System;
using System.Collections;
using UnityEngine;
using Fighter5011.Ui;
using Fighter5011;


public class SceneArena : MonoBehaviour
{
    [SerializeField] private float delayBeforeLevelResult = 2.0f;

    private Action onFinishArena;
    private ArenaResult arenaResult;
    

    public void Play(IArena arenaToPlay, UIPlayerMenu.Result result, Action onFinish)
    {
        UiScreenManager.Instance.ShowScreen(ScreenType.Ingame);

        onFinishArena = onFinish;

        arenaToPlay.Show(result, OnFinishArena);

        AudioManager.Instance.Play(IngameData.asset.Value.Settings.audioSettings.RandomArenaMusic, AudioType.Music);
        AudioManager.Instance.Play(IngameData.asset.Value.Settings.audioSettings.RandomArenaStartClip);
    }


    private void OnFinishArena(ArenaResult result)
    {
        arenaResult = result;
        FinishLevel();
    }


    private void FinishLevel()
    {
        StartCoroutine(ShowLevelResult());
    }


    private IEnumerator ShowLevelResult()
    {
        yield return new WaitForSeconds(delayBeforeLevelResult);

        UILevelResult resultScreen = UiScreenManager.Instance.ShowScreen(ScreenType.Result) as UILevelResult;
        resultScreen.SetupResult(arenaResult);
        resultScreen.OnHideEnd += (_) => OnFinish(arenaResult);
    }


    private void OnFinish(ArenaResult result)
    {
        bool isRateUsAvailable = !RateUs.IsAppRated && RateUs.CanShow(Player.Level);

        if (isRateUsAvailable)
        {
            UiScreenManager.Instance.ShowScreen(ScreenType.RateUsScreen, onHided: (hided) =>
            {
                UiScreenManager.Instance.HideScreen(ScreenType.Ingame);
                onFinishArena();

            });
        }
        else
        {
            UiScreenManager.Instance.HideScreen(ScreenType.Ingame);

            UnityAds.ShowInterstitial();

            onFinishArena();
        }
    }
}
