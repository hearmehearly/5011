﻿using UnityEngine;
using Object = UnityEngine.Object;


public static class DevContent
{
    private static DevContentSettings settings;

    private static bool enabled;

    static DevContent()
    {
        settings = IngameData.asset.Value.Settings.devContentSettings;
    }

    public static void SetEnabled(bool _enabled)
    {
        enabled = _enabled;
    }

    public static void CreateVisualHitBox(Fighter fighter, FighterMeleeHitZone hitZone)
    {
        if (!enabled || !settings.enabledInTrainingRoom)
        {
            return;
        }

        var devHitVisual = Object.Instantiate(settings.devHitVisual);
        devHitVisual.SetSize(hitZone.HitCollider.size);

        Vector3 offset = hitZone.HitCollider.offset.SetX(hitZone.HitCollider.offset.x * fighter.MovementController.VelocityMultiplier);
        Vector3 hitBoxPosition = hitZone.transform.position + offset;

        devHitVisual.SetPosition(hitBoxPosition);

        Color color = Random.ColorHSV().SetA(settings.colorAlpha);

        devHitVisual.SetColor(color);

        TaskInvoker.Instance.CallMethodWithDelay(devHitVisual, () => Object.Destroy(devHitVisual.gameObject), settings.hitDuration);
    }
}
