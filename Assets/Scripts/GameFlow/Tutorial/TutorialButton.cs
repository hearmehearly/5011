﻿using System;
using UnityEngine;
using UnityEngine.UI;


public abstract class TutorialButton : MonoBehaviour
{
    private const string sortingLayerName = "UI";
    private const int MaxSortingOrder = 32767;
    
    public event Action<TutorialButton> OnFinished;
    
    [SerializeField] private TutorialType tutorialType = default;

    private Canvas currentCanvas;
    private GraphicRaycaster currentRaycaster;



    public TutorialType TutorialType => tutorialType;



    public virtual void StartTutorial()
    {
        currentCanvas = gameObject.AddComponent<Canvas>();
        currentCanvas.overrideSorting = true;
        currentCanvas.sortingLayerName = sortingLayerName;
        currentCanvas.sortingOrder = MaxSortingOrder - 1;

        currentRaycaster = gameObject.AddComponent<GraphicRaycaster>();
    }


    public virtual void FinishTutorial()
    {
        if (currentRaycaster != null)
        {
            Destroy(currentRaycaster);
        }

        if (currentCanvas != null)
        {
            Destroy(currentCanvas);
        }
        
        OnFinished?.Invoke(this);
    }
}
