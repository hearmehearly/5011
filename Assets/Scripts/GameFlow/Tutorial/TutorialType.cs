﻿public enum TutorialType
{
    None            = 0,
    Move            = 1,
    Attack          = 2,
    Ability         = 3,
    Charge          = 4,
    DoubleJump      = 5,
    Ultimate        = 6,
    SeatDown        = 7,
    JumpAttack      = 8
}
