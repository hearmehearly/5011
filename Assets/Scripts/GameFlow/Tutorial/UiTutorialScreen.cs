﻿using System;
using DG.Tweening;
using Fighter5011;
using Fighter5011.Ui;
using UnityEngine;
using UnityEngine.UI;


public class UiTutorialScreen : AnimatorScreen
{
    [Serializable]
    private class Data
    {
        public TutorialType tutorialType = default;
        public GameObject root = default;
    }

    [SerializeField] private GameObject root = default;
    [SerializeField] private Image fadeImage = default;
    [SerializeField] private Data[] data = default;

    [SerializeField] private FactorAnimation showFadeAnimation = default;
    [SerializeField] private FactorAnimation hideFadeAnimation = default;

    public override ScreenType ScreenType => ScreenType.Tutorial;


    public override void Deinitialize()
    {
        DOTween.Kill(this);

        base.Deinitialize();
    }

    public void ShowFade()
    {
        fadeImage.color = fadeImage.color.SetA(default);
        showFadeAnimation.Play((value) => fadeImage.color = fadeImage.color.SetA(value), this);
    }


    public void HideFade(Action callback = null)
    {
        hideFadeAnimation.Play((value) => fadeImage.color = fadeImage.color.SetA(value), this, callback);
    }

    public void SetEnabled(bool value) =>
        root.SetActive(value);

    public void SetupHint(TutorialType tutorialType)
    {
        string hint = string.Empty;

        switch (tutorialType)
        {
            case TutorialType.Move:
                hint = "Move joystick for moving";
                break;
            case TutorialType.Attack:
                hint = "Press button for attack";
                break;
            case TutorialType.Ability:
                hint = "Press button for ability";
                break;
            case TutorialType.Charge:
                hint = "Press button for charge while standing";
                break;
            case TutorialType.DoubleJump:
                hint = "Press button for double jump while jumping";
                break;
            case TutorialType.Ultimate:
                break;
            case TutorialType.JumpAttack:
                hint = "Press button for jump";
                break;
            default:
                break;
        }

        // hintText.text = hint;

        foreach (var d in data)
        {
            d.root.SetActive(d.tutorialType == tutorialType);
        }
    }
}
