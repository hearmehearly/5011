﻿using Fighter5011.Ui;
using System;
using UnityEngine;
using UnityEngine.UI;


public static class TutorialManager
{
    [Serializable]
    public class Data
    {
        public bool IsShootTutorialStarted;
        public bool IsShootTutorialPassed;
        public bool IsUpgradeWeaponTutorialPassed;
        public bool IsBuyWeaponTutorialPassed;
        public bool IsUpgradeAbilityTutorialPassed;
        public bool IsPrestigeTutorialPassed;
        public bool IsBuySkinTutorialPassed;
    }


    private const string PREFS_KEY = "tutorials_prefs";

    public static event Action<TutorialType> OnShouldTiggerTutorial;
    public static event Action<TutorialType> OnTutorialFinished;

    private static Data data;
    private static Guid instaceGuid = Guid.NewGuid();

    private static UiTutorialScreen screen;

    public static bool ShouldPlayTutorial => false; 

    static TutorialManager()
    {
        data = CustomPlayerPrefs.GetObjectValue<Data>(PREFS_KEY);
        data = data ?? new Data();
    }

    public static void Initialize()
    {
        var settings = IngameData.asset.Value.Settings.tutorialSettings;

        screen = UiScreenManager.Instance.ShowScreen(Fighter5011.ScreenType.Tutorial) as UiTutorialScreen;
        screen.SetEnabled(false);

        if (Player.Level > 1)
        {
            foreach (var type in (TutorialType[])Enum.GetValues(typeof(TutorialType)))
            {
                MarkTutorialPassed(type);
            }
        }
    }


    public static bool IsTutorialPassed(TutorialType type) =>
        CustomPlayerPrefs.GetBool(string.Concat(PREFS_KEY, type), false);


    public static void MarkTutorialPassed(TutorialType type) =>
        CustomPlayerPrefs.SetBool(string.Concat(PREFS_KEY, type), true);


    public static void TriggerTutorial(TutorialType type) =>
        OnShouldTiggerTutorial?.Invoke(type);


    public static void StartTutorial(TutorialButton tutorialButton)
    {
        screen.SetupHint(tutorialButton.TutorialType);
        ShowFade();
        GameManager.Instance.GamePause.SetGamePaused(true, tutorialButton);

        tutorialButton.StartTutorial();
        tutorialButton.OnFinished += TutorialButton_OnFinished;
    }

    private static void TutorialButton_OnFinished(TutorialButton tutorialButton)
    {
        GameManager.Instance.GamePause.SetGamePaused(false, tutorialButton);
        HideFade();

        OnTutorialFinished?.Invoke(tutorialButton.TutorialType);
    }


    private static void ShowFade()
    {
        screen.SetEnabled(true);
        screen.ShowFade();
    }

    private static void HideFade()
    {
        screen.SetEnabled(true);
        screen.HideFade(() => screen.SetEnabled(false));
    }
}

