﻿using System.Collections;
using UnityEngine;


public class ArenaTutorialSequence : IAction
{
    private Fighter fighter;


    public void Initialize()
    {
        TutorialManager.OnTutorialFinished += TutorialManager_OnTutorialFinished;
    }


    public void Deinitialize()
    {
        TutorialManager.OnTutorialFinished -= TutorialManager_OnTutorialFinished;
    }


    public void SetupFighter(Fighter _fighter)
    {
        fighter = _fighter;
    }

    public void PerfomAction()
    {
        TutorialManager.TriggerTutorial(TutorialType.Move);
    }


    private IEnumerator StartTutorialSequence(TutorialType type)
    {
        SetControlEnabled(false);
        yield return new WaitForSeconds(2.0f);
        SetControlEnabled(true);
        
        TutorialManager.TriggerTutorial(type);   
    }

    private void TutorialManager_OnTutorialFinished(TutorialType type)
    {
        TutorialType next = TutorialType.None;

        switch (type)
        {
            case TutorialType.None:
                break;
            case TutorialType.Move:
                next = TutorialType.Attack;
                break;
            case TutorialType.Attack:
                next = TutorialType.DoubleJump;
                break;
            case TutorialType.Ability:
                break;
            case TutorialType.Charge:
                break;
            case TutorialType.DoubleJump:
                break;
            case TutorialType.Ultimate:
                break;
            case TutorialType.SeatDown:
                break;
            case TutorialType.JumpAttack:
                break;
        }

        if (next != TutorialType.None)
        {
            MonoBehaviourLifecycle.PlayCoroutine(StartTutorialSequence(next));
        }
        else
        {
            Deinitialize();
        }
    }

    private void SetControlEnabled(bool enabled)
    {
        if (enabled)
        {
            fighter.ControlController.FinishControl(this);
        }
        else
        {
            if (!fighter.ControlController.IsControlActive(ControlType.InputDisable))
            {
                fighter.ControlController.ApplyControl(ControlType.InputDisable, float.MaxValue, this);
            }
        }
    }
}
