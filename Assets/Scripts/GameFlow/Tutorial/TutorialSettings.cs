﻿using Fighter5011;
using UnityEngine;
using UnityEngine.UI;


[CreateAssetMenu(fileName = "TutorialSettings", 
    menuName = NamingUtility.MenuItems.IngameSettings + "TutorialSettings")]
public class TutorialSettings : ScriptableObject
{
    public Image fadeImage = null;
    public RectTransform tapImage = null;
}
