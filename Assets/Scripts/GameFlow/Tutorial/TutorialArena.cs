﻿using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Fighter5011.Ui;
using Fighter5011;

public class TutorialArena : Unit<ArenaResult>, IArena
{
    public enum State
    {
        Playing,
        Win,
        Lose,
        Draw,
        Tutorial
    }

    public const float Border = 52.0f;
    public const float TopMaxBorder = 15.0f;
    
    private LevelControllers levelControllers;
    private PlayerFighter playerFighter;
    private GameObject back;
    


    private Action<float> OnFighterWin;
    private readonly List<Fighter> currentFighters = new List<Fighter>();

    private State CurrentState
    {
        get;
        set;
    }


    public void Initialize()
    {
        levelControllers = new LevelControllers();
    }


    public void Show(UIPlayerMenu.Result result, Action<ArenaResult> onHided, Action onShowed = null)
    {
        Show(onHided, onShowed);

        HideBack();
        ShowBack(Levels.TutorialLevelSettings.Background);

        levelControllers.Initialize();
        levelControllers.NPCFightersController.SetupSettings(Levels.TutorialLevelSettings.NPCFightersControllerSettings);


        if (playerFighter != null)
        {
            Destroy(playerFighter.gameObject);
        }

        playerFighter = ContentStorage.CreatePlayerFighter();

        playerFighter.SetOnWinCallback(() => OnFighterWin(200));

        playerFighter.Initialize(result.ChoosedFighterName);
        playerFighter.SetupCamera(GameManager.Instance.GameCamera);

        playerFighter.StartLevel();

        currentFighters.Clear();
        currentFighters.Add(playerFighter);

        List<Fighter> createdFighters = levelControllers.NPCFightersController.CreateLevelNPCs();

        foreach (var target in createdFighters)
        {
            target.HealthController.AppliedDamageMultiplier = 2.0f;
        }

        currentFighters.AddRange(createdFighters);

        UILevel uiLevel = UiScreenManager.Instance.LoadedScreen<UILevel>(Fighter5011.ScreenType.Ingame);
        uiLevel.UILevelCounter.InitializeKillsCounter(currentFighters);

        OnFighterWin = OnLevelCompleted;

        foreach (var fighter in currentFighters)
        {
            fighter.CombatController.OnGottaKill += IncrementKillsCounter;
            fighter.AddOnDieCallback((f) => FinishLevel(200));
        }

        GameManager.Instance.GamePause.SetGamePaused(true, this);
        ShowTutorial(TutorialType.Move, () => ShowTutorial(TutorialType.Attack, () => ShowTutorial(TutorialType.DoubleJump, () => ShowTutorial(TutorialType.SeatDown, () => ShowTutorial(TutorialType.Ultimate, () =>
        GameManager.Instance.GamePause.SetGamePaused(false, this))))));
    }


    private void ShowTutorial(TutorialType type, Action hided)
    {
        UiScreenManager.Instance.ShowScreen(ScreenType.TutorialPopup, onShowBegin: (showed) =>
        {
            (showed as TutorialPopup).SetTutorial(type);
        }, onHided: (hidedView) =>
        {
            hided?.Invoke();
        });
    }

    private void IncrementKillsCounter(Fighter fighter, Fighter fighterToKill)
    {
        levelControllers.ArenaTimerController.MarkFighterDeath();

        //UILevel uiLevel = UiScreenManager.Instance.LoadedScreen<UILevel>(Fighter5011.ScreenType.Ingame);
        //uiLevel.UILevelCounter.SetKillsCount(fighter, fighter.KillsCounter);
    }


    protected override void Hided(ArenaResult result)
    {
        CurrentState = State.Playing;
        levelControllers.NPCFightersController.DestroyLevelNPCs();
        levelControllers.Deinitialize();

        Player.UpLevel();
        base.Hided(result);

    }


    private void OnLevelCompleted(float coinsAmount)
    {
        CurrentState = State.Win;

        StartCoroutine(FinishLevel(1f, coinsAmount));
    }


    private void FinishLevel(int c)
    {
        StartCoroutine(FinishLevel(default, c));
    }


    private IEnumerator FinishLevel(float delay, float coins = 0.0f)
    {
        // find out who win
        // maybe deinitialize all

        UILevel uiLevel = UiScreenManager.Instance.LoadedScreen<UILevel>(ScreenType.Ingame);
        uiLevel.UILevelCounter.Deinitialize();

        foreach (var fighter in currentFighters)
        {
            fighter.FinishLevel();
            fighter.CombatController.OnGottaKill -= IncrementKillsCounter;
        }

        uiLevel.Hide();

        yield return new WaitForSeconds(delay);

        for (int i = currentFighters.Count - 1; i >= 0; i--)
        {
            currentFighters[i].Deinitialize();

            Destroy(currentFighters[i].gameObject);
        }

        BattleResult battleResult = default;
        Fighter winner = currentFighters.OrderByDescending(element => element.KillsCounter).FirstOrDefault();
        battleResult = playerFighter.Equals(winner) ? BattleResult.Win : BattleResult.Lose;

        Hided(new ArenaResult
        {
            battleResult = battleResult,
            Coins = coins
        });
    }


    private void ShowBack(GameObject prefab)
    {
        HideBack();

        back = Instantiate(prefab, transform);
        back.SetActive(true);
    }


    public void HideBack()
    {
        if (back != null)
        {
            back.SetActive(false);
        }
    }
}
