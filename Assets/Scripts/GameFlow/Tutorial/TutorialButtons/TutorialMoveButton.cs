﻿using UnityEngine.UI;
using UnityEngine;


public class TutorialMoveButton : TutorialButton
{
    [SerializeField] private VariableJoystick joystick = default;

    public override void StartTutorial()
    {
        base.StartTutorial();

        joystick.onPressed.AddListener(FinishTutorial);
    }

    public override void FinishTutorial()
    {
        joystick.onPressed.RemoveListener(FinishTutorial);

        base.FinishTutorial();
    }


    private void Button_OnClick()
    {
        FinishTutorial();
    }
}
