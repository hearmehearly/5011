﻿using UnityEngine.UI;
using UnityEngine;


public class TutorialClickButton : TutorialButton
{
    [SerializeField] private Button button = default;

    public override void StartTutorial()
    {
        base.StartTutorial();

        button.onClick.AddListener(Button_OnClick);
    }

    public override void FinishTutorial()
    {
        button.onClick.RemoveListener(Button_OnClick);

        base.FinishTutorial();
    }


    private void Button_OnClick()
    {
        FinishTutorial();
    }
}
