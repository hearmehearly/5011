﻿using System;
using UnityEngine;


public class ScenePlayer : MonoBehaviour
{
    public void Show(Action<UIPlayerMenu.Result> OnHidedCallback)
    {
        UIPlayerMenu.Prefab.Instance.Show((result) => OnHidedCallback(result), OnMenuShowed);
    }


    private void OnMenuShowed()
    {
        SocialController.TryProposeOffer();
    }
}