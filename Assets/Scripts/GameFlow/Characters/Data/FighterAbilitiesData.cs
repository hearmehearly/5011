﻿using UnityEngine;


[CreateAssetMenu(fileName = "Fighter Abilities Data", menuName = "Settings/Fighter/AbilitiesData")]
public class FighterAbilitiesData : ScriptableObject
{
    public Ability[] Abilities = default; 
}
