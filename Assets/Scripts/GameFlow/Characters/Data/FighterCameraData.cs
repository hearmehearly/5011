﻿using UnityEngine;


[CreateAssetMenu(fileName = "FighterCameraData", menuName = "Settings/Fighter/FighterCameraData")]
public class FighterCameraData : ScriptableObject
{
    [Header("Scale")]
    public float scaleDeltaIncreateDuration = 0.5f;
    public float scaleDeltaDecreaseDuration = 0.5f;
    public float additionalSizeOnJump = 5;

    [Header("Movement")]
    public float smoothTime = 0.5F;
    public float smoothTimeInJump = 0.5F;
    public float targetPositionXCoefficient = 1.0f;

    public float maxOffset = 10.0f;
}
