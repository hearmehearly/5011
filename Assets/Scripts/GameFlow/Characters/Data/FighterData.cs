﻿using UnityEngine;


[CreateAssetMenu(fileName = "Fighter Data", menuName = "Settings/Fighter/FighterData")]
public class FighterData : ScriptableObject
{
    public Fighter.Name Name = Fighter.Name.None;

    public FighterCombatData CombatData = default;
    public FighterMovementData MovementData = default;
    public FighterHealthData HealthData = default;
    public FighterAbilitiesData AbilitiesData = default;
    public FighterVisualData VisualData = default;
}
