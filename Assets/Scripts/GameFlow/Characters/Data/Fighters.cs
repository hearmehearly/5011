﻿using System;
using UnityEngine;


[CreateAssetMenu(fileName = "Fighters", menuName = "Settings/Fighter/Fighters")]
public class Fighters : ScriptableObject
{
    private static readonly ResourceAsset<Fighters> asset = new ResourceAsset<Fighters>("Game/Fighters");

    [SerializeField]
    private FighterData[] configs = null;

    public static FighterData[] Configs => asset.Value.configs;



    public static FighterCombatData GetCombatData(Fighter.Name fighterName)
    {
        return GetData(fighterName)?.CombatData;
    }


    public static FighterMovementData GetMovementData(Fighter.Name fighterName)
    {
        return GetData(fighterName)?.MovementData;
    }


    public static FighterHealthData GetHealthData(Fighter.Name fighterName)
    {
        return GetData(fighterName)?.HealthData;
    }


    public static FighterAbilitiesData GetAbilityData(Fighter.Name fighterName)
    {
        return GetData(fighterName)?.AbilitiesData;
    }


    public static FighterVisualData GetVisualData(Fighter.Name fighterName)
    {
        return GetData(fighterName)?.VisualData;
    }


    private static FighterData GetData(Fighter.Name fighterName)
    {
        return Array.Find(asset.Value.configs, data => data.Name == fighterName);
    }
}
