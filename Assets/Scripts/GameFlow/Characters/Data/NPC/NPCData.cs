﻿using UnityEngine;


[CreateAssetMenu(fileName = "NPCData", menuName = "Settings/Fighter/NPCData")]
public class NPCData : ScriptableObject
{
    public bool IsPassive = false;
    public float AttackCooldown = 0.2f;
    public float DistanceToStop = 1f;
}
