﻿using Spine.Unity;
using UnityEngine;
using System;


[CreateAssetMenu(fileName = "Fighter Visual Data", menuName = "Settings/Fighter/VisualData")]
public class FighterVisualData : ScriptableObject
{
    [Serializable]
    private class HitComboAnimations
    {
        [SpineAnimation(dataField = "skeletonDataAsset")]
        public string[] hitAnimations = default;

        public string HitAnimation => hitAnimations.RandomObject();
    }

    public Sprite UiMainSprite = default;
    public Sprite UiCounterSpriteNPC = default;
    public Sprite UiCounterSpritePlayer = default;

    public SkeletonDataAsset skeletonDataAsset = default;

    [Header("Animation keys")]

    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string idleAnimation = default;

    [Header("Common")]
    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string deathAnimation = default;

    [Header("Movement")]
    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string runAnimation = default;

    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string seatAnimation = default;

    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string standUpAnimation = default;

    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string knockOverAnimation = default;

    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string knockStandAnimation = default;

    [Header("Jump")]
    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string jumpAnimation = default;
    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string jumpLoopAnimation = default;
    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string jumpEndAnimation = default;

    [Header("Combat")]

    [SerializeField] private HitComboAnimations[] hitComboAnimations = default;

    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string[] hitAnimations = default;

    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string takeDamageAnimation = default;

    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string[] hitInJumpAnimation = default;
    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string[] hitInSeatDownAnimation = default;

    [Header("Charge")]

    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string chargeBeginAnimation = default;

    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string chargeAnimation = default;

    [Header("Bones")]
    [SpineBone(dataField = "skeletonDataAsset")]
    public string attackRoot = default;


    [Header("VFX")]
    public FxHandler showFx = default;
    public FxHandler hideFx = default;
    public FxHandler hitFx = default;

    public string RandomHitInSeatDownAnimation => hitInSeatDownAnimation.RandomObject();
    public string RandomHitInJumpAnimation => hitInJumpAnimation.RandomObject();

    public string GetHitAnimation(int combo)
    {
        HitComboAnimations comboData = hitComboAnimations[combo % hitComboAnimations.Length];

        return comboData.HitAnimation;
    }
}
