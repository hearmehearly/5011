﻿using UnityEngine;


[CreateAssetMenu(fileName = "Fighter Health Data", menuName = "Settings/Fighter/HealthData")]
public class FighterHealthData : ScriptableObject
{
    public float Health = 0.0f;
}
