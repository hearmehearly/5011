﻿using UnityEngine;


[CreateAssetMenu(fileName = "Fighter Movement Data", menuName = "Settings/Fighter/MovementData")]
public class FighterMovementData : ScriptableObject
{
    public float[] JumpForces = default;
    [Tooltip("на прыжки после второго прыжка (кд дабл джампа иначе)")]
    public float DoubleJumpCooldown = 0.0f;

    public ChargeAbility Charge = default;

    public float MoveSpeed = 0.2f;
    public float MaxMoveSpeed = 10f;

    [Tooltip("Столько будет тормозить тело после остановки")]
    [Range(0.0f, 1.0f)]
    public float DurationToTotalStop = 0.5f;

    [Tooltip("На сколько процентов уменьшится коллайдер по х и по у во время присеста. Пока сделал по фасту чтобы и по иксу менялось, потому что скоро будет новый арт и оффесы пойдут по пизде. Передалаю когда будет арт")]
    [Range(0,1)]
    public float PercentToReduceColliderOnSeatDown = 0.5f;

    [Header("Опрокидывание")]

    [Tooltip("значение баунсинга тела в момент, когда случилось опрокидование (при коллизии с землей - уменьшается и после того, как будет меньше 0 - установится в 0 и опрокидывание кончится")]
    public float OverKnockBounciness = 2.0f;

    [Tooltip("то значение, на которое будет уменьшено.")]
    public float OverKnockReduceBounciness = 0.9f;

    [Tooltip("вектор (длина) для скорости по полученному уддару направления.")]
    public float OverKnockMagnitude = 10.0f;

    [Tooltip("Длительность остановки.")]
    public float StopDurtionOnAttack = 0.2f;
}
