﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;


[Serializable]
public class DamageValue
{
    public DamageType damageType = default;
    [Tooltip("минимальное значение урона удара")]
    public float MinHitDamage = default;
    [Tooltip("максимальное значение урона удара")]
    public float MaxHitDamage = default;
    [Tooltip("вектор на который сместится враг при получении удара")]
    public Vector2 HitImpuls = default;

    [Tooltip("Сбивает ли с ног")]
    public bool ShouldKnockOver = false;

    [Tooltip("Задержка появления хитбокса (или выстрела)")]
    public float HitBoxAppearDelay = default;
    [Tooltip("вектор на который смещается игрок во время исполнения автоатаки стоя на земле.")]
    public Vector2 HitMove = Vector2.right;

    [Tooltip("отмаем котроль у героя на столько секунд")]
    public float InputDisableOnHitDuration = default;

    public float GetDamage() => Random.Range(MinHitDamage, MaxHitDamage);
}

[Serializable]
public class DataDamageValue : DamageValue
{
    public float StunDurationPerHit = 0.2f;
    public Vector2 hitColliderOffset = default;
    public Vector2 hitColliderSize = new Vector2(0.8f, 1.4f);
}

[Serializable]
public class PercentDamageValue
{
    public bool ShouldKnockOver = false;
    public DamageType DamageType = DamageType.Simple;

    [Tooltip("% от текущего урона")]
    public float PercentFromHealth = default;

    [Tooltip("вектор на который сместится враг при получении удара")]
    public Vector2 HitImpuls = default;
    public float StunDurationPerHit = 0.2f;
}


[CreateAssetMenu(fileName = "Fighter Combat Data", menuName = "Settings/Fighter/CombatData")]
public class FighterCombatData : ScriptableObject
{
    public enum Type
    {
        None = 0,
        Melee = 1,
        Range = 2
    }

    public Type CombatType = Type.Melee;

    [Header("Common")]


    [Tooltip("Mинимальное время на выполнение следующего удара. (После первого удара в течение этого времени нельзя сделать второй удар)")]
    public float DelayBetweenHits = 0.2f;
    [Tooltip("Mаксимально допустимое время на продолжение комбо для следующего удара. (Если игрок нанесет второй удар и не успее в течение этого времени ударить, то комбо скинется)")]
    public float MaxDelayBetweenCombo = 0.2f;
    [Tooltip("Максимальное количество комбо ударов (После достижения значения счетчик комбо сбрасывается в нуль)")]
    public float MaxComboCount = 4;
    [Tooltip("Длительность хитбокса")]
    public float HitDuration = 0.1f;
    [Tooltip("Длительность хитбокса в прыжке")]
    public float JumpHitDuration = 0.1f;
    [Tooltip("Задержка хитбокса в прыжке")]
    public float JumpHitDelay = default;
    [Tooltip("Длительность хитбокса в приседе")]
    public float SeatDownHitDuration = 0.1f;
    [Tooltip("Задержка хитбокса в приседе")]
    public float SeatDownHitDelay = default;
    
    [Tooltip("Сколько максимум можно сделать ударов в прыжке. ЛОГИКА ДЛЯ ОДНОГО МАКСИМУМ!")]
    public int MaxHitCountPerJump = 1;
    [Header("Move")]
    //[Tooltip("Макс длительность сбития с ног (после этого времени сбросится стан и игрок может муваться, иначе ждет когда достигнет земли")]
    //public float MaxOverKnockDuration = 0.1f;

    [Tooltip("вектор на который смещается игрок во время исполнения автоатаки сидя.")]
    public Vector3 SeatDownHitMove = default;
    [Tooltip("вектор на который смещается игрок во время исполнения автоатаки в прыжжке.")]
    public Vector3 JumpHitMove = default;

    [Header("Damage")]
    public DataDamageValue[] DamageValues = default;
    public DataDamageValue JumpDamageValue = default;
    public DataDamageValue SeatDownDamageValue = default;


    [Header("Range Settings")]
    public float BulletSpeed = 5.0f;

    public float BulletMaxDistance = 5.0f;

    [Tooltip("сколько времени перс будет висеть в воздухе после удара в прыжке")]
    public float LevitationDuration = 5.0f;

    [Header("Effects")]
    public FxHandler hitFx = default;

    public FighterBullet FighterBullet = default;

    public float GetDamage(int combo)
    {
        float result = default;

        DamageValue damageValue = GetDamageValue(combo);

        if (damageValue != null)
        {
            result = damageValue.GetDamage();
        }

        return result;
    }


    public bool IsKnockOver(int combo)
    {
        DataDamageValue damageValue = GetDamageValue(combo);

        return damageValue == null ? default : damageValue.ShouldKnockOver;
    }


    public float StunDuration(int combo)
    {
        DataDamageValue damageValue = GetDamageValue(combo);

        return damageValue == null ? default : damageValue.StunDurationPerHit;
    }

    public float FindHitBoxAppearDelay(int combo)
    {
        DataDamageValue damageValue = GetDamageValue(combo);
        return damageValue == null ? default : damageValue.HitBoxAppearDelay;
    }

    public float FindInputDisableOnHitDuration(int combo)
    {
        DataDamageValue damageValue = GetDamageValue(combo);
        return damageValue == null ? default : damageValue.InputDisableOnHitDuration;
    }


    public Vector2 FindHitMove(int combo)
    {
        DataDamageValue damageValue = GetDamageValue(combo);
        return damageValue == null ? default : damageValue.HitMove;
    }


    public Vector2 GetHitImpuls(int combo)
    {
        DamageValue damageValue = GetDamageValue(combo);

        return damageValue == null ? Vector2.zero : damageValue.HitImpuls;
    }

    public Vector2 GetHitColliderOffset(int combo)
    {
        DataDamageValue damageValue = GetDamageValue(combo);

        return damageValue == null ? Vector2.zero : damageValue.hitColliderOffset;
    }


    public Vector2 GetHitColliderSize(int combo)
    {
        DataDamageValue damageValue = GetDamageValue(combo);

        return damageValue == null ? Vector2.zero : damageValue.hitColliderSize;
    }


    private DataDamageValue GetDamageValue(int combo)
    {
        DataDamageValue result = default;

        if (DamageValues == null)
        {
            Debug.LogError("DamageValues is null. Check it out");
        }
        else if ((combo - 1) < DamageValues.Length && (combo - 1) >= 0)
        {
            result = DamageValues[combo - 1];
        }
        else
        {
        //    Debug.LogError("DamageValues data length is less than combo count. Check it out. Combo " + combo);
            result = DamageValues[default];
        }

        return result;
    }


    private void OnValidate()
    {
        // для всех - ударов
        Vector2 hitColliderOffset = new Vector2(0.4f, 0f);
        Vector2 hitColliderSize = new Vector2(0.8f, 1.4f);

        // для всех - прыжки
        Vector2 jumpColliderOffset = new Vector2(0.4f, 0f);
        Vector2 jumpColliderSize = new Vector2(0.8f, 3f);

        // для всех - присед
        Vector2 seatDownColliderOffset = new Vector2(1.35f, -0.4f);
        Vector2 seatDownColliderSize = new Vector2(2.7f, 0.6f);

        //foreach (var i in DamageValues)
        //{
        //    i.hitColliderOffset = hitColliderOffset;
        //    i.hitColliderSize = hitColliderSize;
        //}

        //JumpDamageValue.hitColliderOffset = jumpColliderOffset;
        //JumpDamageValue.hitColliderSize = jumpColliderSize;

        //SeatDownDamageValue.hitColliderOffset = seatDownColliderOffset;
        //SeatDownDamageValue.hitColliderSize = seatDownColliderSize;
    }
}
