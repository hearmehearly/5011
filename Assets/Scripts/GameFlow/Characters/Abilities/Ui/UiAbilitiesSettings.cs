﻿using System;
using UnityEngine;


[CreateAssetMenu]
public class UiAbilitiesSettings : ScriptableObject
{
    [Serializable]
    public class Data
    {
        public Ability.Name abilityName = default;

        public Sprite uiSprite = default;
        public string headerText = default;
        public string descText = default;
    }

    [SerializeField] private Data[] data = default;

    public Data GetData(Ability.Name title)
    {
        var result = Array.Find(data, e => e.abilityName == title);

        if (result == null)
        {
            Debug.Log($"Ui data in null for {title} in {this}");
        }

        return result;
    }


    private void AssignDefaultValues()
    {
        data = new Data[Enum.GetValues(typeof(Ability.Name)).Length];

        int j = 0;
        foreach (var i in (Ability.Name[])Enum.GetValues(typeof(Ability.Name)))
        {
            data[j] = new Data();
            data[j].abilityName = i;
            data[j].headerText = i.ToString();

            j++;
        }
    }
}
