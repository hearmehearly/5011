﻿using UnityEngine;
using UnityEngine.UI;


public class UiAbilityCell : MonoBehaviour
{
    [SerializeField] private Text header = default;
    [SerializeField] private UIButton showDescButton = default;

    [SerializeField] private GameObject descRoot = default;
    [SerializeField] private Text desc = default;
    [SerializeField] private Image image = default;



    public void Initialize(Ability.Name name)
    {
        var data = IngameData.asset.Value.Settings.FighterVisualData.GetData(name);

        if (data == null)
        {
            return;
        }

        header.text = data.headerText;

        string descText = data.descText.Replace("\\n", "\n");
        desc.text = descText;
        
        image.sprite = data.uiSprite;

        showDescButton.onPressed.AddListener(ShowDesc);
        showDescButton.onClickUp.AddListener(HideDesc);

        HideDesc();
    }


    public void Deinitialize()
    {
        showDescButton.onPressed.RemoveListener(ShowDesc);
        showDescButton.onClickUp.RemoveListener(HideDesc);
    }


    private void HideDesc(float secs = default)
    {
        descRoot.gameObject.SetActive(false);
    }


    private void ShowDesc()
    {
        descRoot.gameObject.SetActive(true);
    }
}
