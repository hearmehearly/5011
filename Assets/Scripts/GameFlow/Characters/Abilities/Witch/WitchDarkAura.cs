﻿using System;
using UnityEngine;
using DG.Tweening;
using System.Collections.Generic;

#warning будет баг, что если враг съебался из первой, а потом зашел во время воторой фазы, то кровь не создаться, чтобы создалась - в момент смены фазы нужно сделать оверлапс и оставить в листе только тех врагов, которые были оверлапнуты
public class WitchDarkAura : MonoBehaviour
{
    [Serializable]
    public class Data
    {
        [Tooltip("длительность фазы увеличения")]
        public float FirstPhaseDuration = default;
        [Tooltip("конечны скел 1 фазы")]
        public Vector3 EndScaleDuration = default;
        [Tooltip("длительность фазы уменьшения")]
        public float SecondPhaseDuration = default;

        [Header("First Phase")]
        public DamageValue damageValue = default;
        public ControlValue[] controlTypes = default;
        [Header("Second Phase")]
        public DamageValue damageSecondPhaseValue = default;
        public ControlValue[] secondControlTypes = default;
        [Tooltip("с какой силой оттолкнуть к ведьме (НУЖНО ЗАМЕНИТЬ НА ДИСНТАЦИЮ)!")]
        public float gripForce = default;
    }

    public event Action<Fighter> OnFighterEnterZone;

    [SerializeField]
    private CollidableObject collidableObject = default;

    private bool isFirstPhase;
    private bool isSecondPhase;
    private Action deinitializeCallback;
    private Fighter currentOwner;
    private Data currentData;
    private Vector3 targetRootCenter;

    private readonly List<Fighter> alreadyEnteredFightersOnFirstPhase = new List<Fighter>();


    public void Initialize(Data data, Fighter ownerValue, Action deinitializeCallbackValue)
    {
        currentOwner = ownerValue;
        currentData = data;
        deinitializeCallback = deinitializeCallbackValue;
        targetRootCenter = currentOwner.transform.position;

        transform.localScale = Vector3.zero;

        isFirstPhase = true;

        DOTween.Sequence()
            .Append(transform.DOScale(data.EndScaleDuration, data.FirstPhaseDuration))
            .AppendCallback(() =>
            {
                var currentCollider = GetComponent<Collider2D>();
                currentCollider.enabled = false;
                currentCollider.isTrigger = false;

                isFirstPhase = false;
                isSecondPhase = true;

                currentCollider.enabled = true;
                currentCollider.isTrigger = true;
            })
            .Append(transform.DOScale(Vector3.zero, data.SecondPhaseDuration))
            .AppendCallback(Deinitialize)
            .SetId(this);

        collidableObject.OnCustomTriggerEnter2D += CollidableObject_OnCustomTriggerEnter2D;
    }


    private void CollidableObject_OnCustomTriggerEnter2D(Collider2D arg1, CollidableObject.Type arg2)
    {
        Fighter enemyFighter = arg1.GetComponent<Fighter>();

        bool shouldTriggerEnterZoneEvent = true;

        if (enemyFighter != null && !enemyFighter.Equals(currentOwner))
        {
            if (isFirstPhase)
            {
                enemyFighter.ApplyHit(null,
                                currentData.damageValue.GetDamage(),
                                currentData.damageValue.damageType,
                                currentData.damageValue.HitImpuls * transform.lossyScale.normalized,
                                default,
                                false,
                                currentOwner);

                foreach (var control in currentData.controlTypes)
                {
                    enemyFighter.ControlController.ApplyControl(control, currentOwner);
                }

                alreadyEnteredFightersOnFirstPhase.Add(enemyFighter);
            }
            else if (isSecondPhase)
            {
                enemyFighter.ApplyHit(null,
                                 currentData.damageSecondPhaseValue.GetDamage(),
                                 currentData.damageSecondPhaseValue.damageType,
                                 currentData.damageSecondPhaseValue.HitImpuls * transform.lossyScale.normalized,
                                 default,
                                 false,
                                 currentOwner);

                Vector3 direction = (enemyFighter.transform.position - targetRootCenter).normalized;
                enemyFighter.MovementController.ForceStop();

                enemyFighter.MovementController.AddForce(currentData.gripForce * direction);

                shouldTriggerEnterZoneEvent = !alreadyEnteredFightersOnFirstPhase.Contains(enemyFighter);

                foreach (var control in currentData.secondControlTypes)
                {
                    enemyFighter.ControlController.ApplyControl(control, currentOwner);
                }
            }

            if (shouldTriggerEnterZoneEvent)
            {
                OnFighterEnterZone?.Invoke(enemyFighter);
            }
        }
    }


    public void Deinitialize()
    {
        isSecondPhase = false;
        collidableObject.OnCustomTriggerEnter2D -= CollidableObject_OnCustomTriggerEnter2D;

        deinitializeCallback?.Invoke();
        DOTween.Kill(this);

        if (this != null && gameObject != null)
        {
            Destroy(gameObject);
        }
    }
}
