﻿using System;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "AwakeningPuppetsAbility", menuName = "Settings/Ability/Witch/AwakeningPuppetsAbility")]
public class AwakeningPuppetsAbility : OnceResponceAbility
{
    public FxHandler fxHandler = default;

    [Tooltip("абилки которые будут к кукол")]
    public Name[] abilitiesForPuppetToCopy = default;
    public WitchPuppet witchPuppetPrefab = default;
    [Tooltip("время жизни кукол")]
    public float puppetExistsDuration = default;
    [Tooltip("сколько макс будет кукол")]
    public int bloodsCountForPuppet = default;
    [Tooltip("на скоко процентов уменьшить кулдаун у абилок во время ульты")]
    [Range(0.0f, 1.0f)]
    public float coolDownReducePercent = default;

    private List<ActiveAbility> abilitiesForPuppet;
    private readonly Dictionary<ActiveAbility, float> reducedCooldownValue = new Dictionary<ActiveAbility, float>();

    private BloodCurseAbility bloodCurseAbility;

    private List<WitchPuppet> puppets = new List<WitchPuppet>();

    

    public bool IsAbilityActive { get; private set; }

    public override void Initialize(Fighter fighter)
    {
        base.Initialize(fighter);

        abilitiesForPuppet = null; // serialized bug in scriptable object
    }

    public override void UseAbility()
    {
        base.UseAbility();

        ResetAbilitiesCooldown();
        ReduceAbilitiesCooldown();

        CreatePuppets();

        bloodCurseAbility.DestroyActiveBloods();
        bloodCurseAbility.RefreshInfoUI();

        IsAbilityActive = true;
        TaskInvoker.Instance.CallMethodWithDelay(this, () =>
        {
            ReturnAbilitiesCooldown();
            IsAbilityActive = false;
        }
        , puppetExistsDuration);
    }


    private void CreatePuppets()
    {
        DestroyAllPuppets();

        bloodCurseAbility = bloodCurseAbility ?? (ownerFighter.AbilityController.currentAbilities.Find(element => element.AbilityName == Name.BloodCurse) as BloodCurseAbility);

        for (int i = 0; i < bloodsCountForPuppet; i++)
        {
            WitchCursedBlood nearestBlood = bloodCurseAbility.FindNearestBlood(ownerFighter.transform.position);

            if (nearestBlood != null)
            {
                WitchPuppet witchPuppet = Instantiate(witchPuppetPrefab, nearestBlood.transform.position, Quaternion.identity);

                if (abilitiesForPuppet == null)
                {
                    abilitiesForPuppet = new List<ActiveAbility>();
                    
                    foreach (var ability in ownerFighter.AbilityController.currentAbilities)
                    {
                        if (Array.Exists(abilitiesForPuppetToCopy, element => element == ability.AbilityName) && ability is ActiveAbility activeAbility)
                        {
                            activeAbility.FastResetCoolDown();
                            abilitiesForPuppet.Add(activeAbility);
                        }
                    }

                }

                witchPuppet.Initialize(ownerFighter, abilitiesForPuppet);
                
                fxHandler.PlayOnce(witchPuppet.transform.position);

                Destroy(nearestBlood.gameObject);
                bloodCurseAbility.CurrentBoolds.Remove(nearestBlood);
                bloodCurseAbility.RefreshInfoUI();

                puppets.Add(witchPuppet);

                TaskInvoker.Instance.CallMethodWithDelay(this, () =>
                {
                    witchPuppet.Deinitialize();
                }, puppetExistsDuration);
            }
        }
    }


    public override void Deinitialize()
    {
        DestroyAllPuppets();

        base.Deinitialize();
    }

    private void ResetAbilitiesCooldown()
    {
        foreach (var ability in ownerFighter.AbilitiesData.Abilities) // reset all but this abilities
        {
            if (!ability.Equals(this))
            {
                ownerFighter.AbilityController.FastResetCooldown(ability);
            }
        }
    }


    private void ReduceAbilitiesCooldown()
    {
        foreach (var ability in ownerFighter.AbilityController.currentAbilities) // reset all but this abilities
        {
            if (!ability.Equals(this) && ability is ActiveAbility activeAbility)
            {
                if (!reducedCooldownValue.ContainsKey(activeAbility))
                {
                    float reducedValue = activeAbility.CoolDown * coolDownReducePercent;
                    reducedCooldownValue.Add(activeAbility, reducedValue);
                }

                ownerFighter.AbilityController.DecreaseCooldownPercent(activeAbility, coolDownReducePercent);
            }
        }
    }


    private void ReturnAbilitiesCooldown()
    {
        foreach (var ability in ownerFighter.AbilityController.currentAbilities) // reset all but this abilities
        {
            if (!ability.Equals(this) && ability is ActiveAbility activeAbility)
            {
                if (reducedCooldownValue.TryGetValue(activeAbility, out float reducedValue))
                {
                    ownerFighter.AbilityController.IncreaseCooldown(activeAbility, reducedValue);
                }
            }
        }
    }

    private void DestroyAllPuppets()
    {
        TaskInvoker.Instance.UnscheduleAllMethodForTarget(this);

        foreach (var i in puppets)
        {
            if (i != null && i.gameObject != null && !i.isdeinit)
            i.Deinitialize();
        }

        puppets.Clear();
    }
}
