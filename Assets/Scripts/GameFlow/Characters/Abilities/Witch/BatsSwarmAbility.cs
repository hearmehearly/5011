﻿using System;
using UnityEngine;


[CreateAssetMenu(fileName = "BatsSwarmAbility", menuName = "Settings/Ability/Witch/BatsSwarmAbility")]
public class BatsSwarmAbility : OnceResponceAbility, IAbilityRootHitable
{
    public event Action<Fighter> OnHitFighter;

    public FighterAbilityProjectile swarm = default;
    public FighterAbilityProjectile.Data swarmData = default;

    public float batsSwarmDelay = default;

    private Transform hitRoot;


    public void InitializeHitRoot(Transform hitRootValue)
    {
        hitRoot = hitRootValue;
    }


    public override void UseAbility()
    {
        base.UseAbility();

        TaskInvoker.Instance.CallMethodWithDelay(this, () =>
        {
            FighterAbilityProjectile hitZone = Instantiate(swarm,
                                                           hitRoot.position,
                                                           Quaternion.identity);

            hitZone.OnHitFighter += HitZone_OnHitFighter;
            hitZone.Initialize(swarmData, ownerFighter, () => hitZone.OnHitFighter -= HitZone_OnHitFighter);

            Physics2D.IgnoreCollision(ownerFighter.MainCollider, hitZone.HitCollider);
        }, batsSwarmDelay);
    }
    

    private void HitZone_OnHitFighter(Fighter fighter)
    {
        foreach (var ability in fighter.AbilityController.currentAbilities)
        {
            if (ability is ActiveAbility activeAbility)
            {
                if (!activeAbility.isCoolDownExpired)
                {
                    activeAbility.FastResetCoolDown();
                    activeAbility.SetCoolDown();
                }
            }
        }

        fighter.MovementController.ForceStop();
        OnHitFighter?.Invoke(fighter);
    }
}
