﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[CreateAssetMenu(fileName = "BloodCurseAbility", menuName = "Settings/Ability/Witch/BloodCurseAbility")]
public class BloodCurseAbility : PassiveAbility
{
    public WitchCursedBlood bloodPrefab = default;

    [Tooltip("максимум существующих крове")]
    public int maxBloodCount = default;

    [Tooltip("максимум время жизни для одно крови")]
    public float existsDuration = default;
    [Tooltip("время, в течении которого из того же фатера не может быть создана кровь")]
    public float eachFighterBloodCreateDelay = default;

    private AwakeningPuppetsAbility awakeningPuppetsAbility;
    private BatsSwarmAbility batsSwarmAbility;
    private DarkAuraAbility darkAuraAbility;

    private readonly Dictionary<Fighter, float> fightersCooldownData = new Dictionary<Fighter, float>();

    public List<WitchCursedBlood> CurrentBoolds { get; private set; }



    public override void Initialize(Fighter fighter)
    {
        base.Initialize(fighter);

        CurrentBoolds = new List<WitchCursedBlood>(maxBloodCount);


        batsSwarmAbility = ownerFighter.AbilityController.GetAbility(Name.BatsSwarm) as BatsSwarmAbility;
        batsSwarmAbility.OnHitFighter += OnAbilityCollisionWithFighter;

        darkAuraAbility = ownerFighter.AbilityController.GetAbility(Name.DarkAura) as DarkAuraAbility;
        darkAuraAbility.OnFighterEnterZone += OnAbilityCollisionWithFighter;


        ownerFighter.CombatController.OnHitFighter += OnHitFighter;
        Scheduler.OnUpdate += Scheduler_OnUpdate;
    }


    public override void Deinitialize()
    {
        ownerFighter.CombatController.OnHitFighter -= OnHitFighter;
        batsSwarmAbility.OnHitFighter -= OnAbilityCollisionWithFighter;

        Scheduler.OnUpdate -= Scheduler_OnUpdate;

        foreach (var blood in CurrentBoolds)
        {
            blood.Deinitialize();
            Destroy(blood.gameObject);
        }

        CurrentBoolds.Clear();

        base.Deinitialize();
    }


    public void DestroyActiveBloods()
    {
        foreach (var blood in CurrentBoolds)
        {
            if (blood.IsAvailableForUse)
            {
                blood.Deinitialize();
                Destroy(blood.gameObject);
            }
        }

        CurrentBoolds.RemoveAll(element => element == null);
    }


    public WitchCursedBlood FindNearestBlood(Vector3 point)
    {
        WitchCursedBlood result = null;
        float nearestDistance = float.MaxValue;

        foreach (var blood in CurrentBoolds)
        {
            if (blood.IsAvailableForUse)
            {
                float distance = Vector3.Distance(point, blood.transform.position);

                if (distance < nearestDistance)
                {
                    nearestDistance = distance;
                    result = blood;
                }
            }
        }

        return result;
    }


    public void RefreshInfoUI()
    {
        TriggerDisplayInfoEvent(CurrentBoolds.Count.ToString());
    }


    private void OnHitFighter(Fighter fighter, float arg2, int arg3)
    {
        if (!fightersCooldownData.ContainsKey(fighter))
        {
            fightersCooldownData.Add(fighter, 0.0f);
        }


        if (fightersCooldownData.TryGetValue(fighter, out float currentFighterCooldown))
        {
            if (currentFighterCooldown <= 0.0f)
            {
                fightersCooldownData[fighter] = eachFighterBloodCreateDelay;
                CreateCursedBlood(fighter.transform.position);
            }
        }
    }


    private void CreateCursedBlood(Vector3 position)
    {
        awakeningPuppetsAbility = awakeningPuppetsAbility ?? (ownerFighter.AbilityController.GetAbility(Name.AwakeningPuppets) as AwakeningPuppetsAbility);

        if (awakeningPuppetsAbility != null && !awakeningPuppetsAbility.IsAbilityActive)
        {
            WitchCursedBlood blood = Instantiate(bloodPrefab, position, Quaternion.identity);
            blood.Initialize();

            CurrentBoolds.Add(blood);

            TaskInvoker.Instance.CallMethodWithDelay(this, () =>
            {
                WitchCursedBlood result = CurrentBoolds.Find(element => element.Equals(blood));

                if (result != null)
                {
                    result.Deinitialize();
                    Destroy(result.gameObject);
                    CurrentBoolds.Remove(result);

                    RefreshInfoUI();
                }

            }, existsDuration);

            if (CurrentBoolds.Count > maxBloodCount)
            {
                CurrentBoolds[0].Deinitialize();
                Destroy(CurrentBoolds[0].gameObject);
                CurrentBoolds.RemoveAt(0);
            }

            RefreshInfoUI();
        }
    }


    private void OnAbilityCollisionWithFighter(Fighter collidedFighter)
    {
        CreateCursedBlood(collidedFighter.transform.position);
    }


    private void Scheduler_OnUpdate(float deltaTime)
    {
        foreach (var data in fightersCooldownData.ToList())
        {
            fightersCooldownData[data.Key] = data.Value - deltaTime;
        }
    }
}
