﻿using System;
using UnityEngine;


[CreateAssetMenu(fileName = "DarkAuraAbility", menuName = "Settings/Ability/Witch/DarkAuraAbility")]
public class DarkAuraAbility : OnceResponceAbility
{
    public event Action<Fighter> OnFighterEnterZone;

    public WitchDarkAura.Data data;
    public WitchDarkAura witchDarkAuraPrefab;

    private WitchDarkAura darkAura;
    

    public override bool AllowUse => ownerFighter.MovementController.IsGrounded;
    

    public override void UseAbility()
    {
        base.UseAbility();

        ownerFighter.MovementController.ChargeBlockers++;
        ownerFighter.MovementController.JumpBlokers++;
        ownerFighter.MovementController.MovementBlokers++;

        ownerFighter.MovementController.Velocity = Vector2.zero;

        darkAura = Instantiate(witchDarkAuraPrefab, ownerFighter.transform.position, Quaternion.identity);
        darkAura.Initialize(data, ownerFighter, OnDeinitialize);

        ownerFighter.ControlController.OnControlStarted += TryStopAbility;
        darkAura.OnFighterEnterZone += TriggetEnterZoneEvent;

        void OnDeinitialize()
        {
            ownerFighter.MovementController.ChargeBlockers--;
            ownerFighter.MovementController.JumpBlokers--;
            ownerFighter.MovementController.MovementBlokers--;

            darkAura.OnFighterEnterZone -= TriggetEnterZoneEvent;
            ownerFighter.ControlController.OnControlStarted -= TryStopAbility;
        }
    }


    public override void Deinitialize()
    {
        ownerFighter.ControlController.OnControlStarted -= TryStopAbility;
        base.Deinitialize();
    }


    private void TryStopAbility(ControlType controlType, object handler)
    {
        ownerFighter.ControlController.OnControlStarted -= TryStopAbility;
        if (controlType == ControlType.Stun)
        {
            darkAura.Deinitialize();
        }
    }


    private void TriggetEnterZoneEvent(Fighter enteredFighter)
    {
        OnFighterEnterZone?.Invoke(enteredFighter);
    }
}
