﻿using Spine.Unity;
using System.Collections.Generic;
using UnityEngine;


public class WitchPuppet : MonoBehaviour
{
    [SerializeField] private SkeletonAnimation skeletonAnimation = default;
    [SerializeField] private Transform hitRoot = default;

    public bool isdeinit;

    Fighter ownerFighter;
    List<ActiveAbility> activeAbilitiesToCopy;

    private readonly List<ActiveAbility> availableAbilities = new List<ActiveAbility>();

    //public override Side LookingSide => throw new System.NotImplementedException();

    public void Initialize(Fighter fighter, List<ActiveAbility> activeAbilitiesToCopyValue)
    {
        isdeinit = false;
        ownerFighter = fighter;
        activeAbilitiesToCopy = activeAbilitiesToCopyValue;

        availableAbilities.AddRange(activeAbilitiesToCopy);

        foreach (var activeAbility in activeAbilitiesToCopy)
        {
            activeAbility.OnAbilityUsed += TryCopyAbility;
        }

        skeletonAnimation.skeleton.SetColor(skeletonAnimation.skeleton.GetColor().SetA(0.5f));
    }

    
    private void TryCopyAbility(Ability usedAbility)
    {
        ActiveAbility abilityToUse = usedAbility.Clone() as ActiveAbility;

        abilityToUse.Initialize(ownerFighter);

        if (abilityToUse is IAbilityRootHitable abilityRootHitable)
        {
            abilityRootHitable.InitializeHitRoot(hitRoot);
        }

        skeletonAnimation.AnimationState.SetAnimation(0, abilityToUse.abilityAnimation, false);
        abilityToUse.UseAbility();
    }

    public void Deinitialize()
    {
        foreach (var activeAbility in activeAbilitiesToCopy)
        {
            activeAbility.OnAbilityUsed -= TryCopyAbility;
        }

        activeAbilitiesToCopy = null;

        isdeinit = true;
        Destroy(gameObject);
    }
}
