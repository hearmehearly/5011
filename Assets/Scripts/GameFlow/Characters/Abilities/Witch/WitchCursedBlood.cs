﻿using UnityEngine;


public class WitchCursedBlood : MonoBehaviour
{
    [SerializeField]
    private CollidableObject collidableObject = default;



    public bool IsAvailableForUse { get; private set; }



    public void Initialize()
    {
        collidableObject.OnCustomCollisionEnter2D += CollidableObject_OnCustomCollisionEnter2D;
    }


    public void Deinitialize()
    {
        collidableObject.OnCustomCollisionEnter2D -= CollidableObject_OnCustomCollisionEnter2D;
    }

    private void CollidableObject_OnCustomCollisionEnter2D(Collision2D arg1, CollidableObject.Type arg2)
    {
        if (arg2 == CollidableObject.Type.Ground)
        {
            IsAvailableForUse = true;
            collidableObject.OnCustomCollisionEnter2D -= CollidableObject_OnCustomCollisionEnter2D;
        }
    }
}
