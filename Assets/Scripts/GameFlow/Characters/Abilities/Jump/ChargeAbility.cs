﻿using System;
using System.Collections;
using UnityEngine;


public class ChargeAbility : ScriptableObject, ICloneable
{
    public Action OnStart;
    public Action OnFinish;

    [Tooltip("КД зависит от дабл джампа (то есть если заюзать дабл джамп, то вешается кд")]
    public bool IsFamiliarCoolDownWithDoubleJump = true;
    public float CoolDown = default;

    [NonSerialized]
    public bool IsCoolDownExpired = true;
    [NonSerialized]
    public float coolDownTimer = default;
    private Coroutine cooldownExpireCorotine;

    protected Fighter ownerFighter;
    protected bool isCharging;


    // to override
    public virtual bool IsChargeAvailable => true;


    public void SetCoolDown()
    {
        IsCoolDownExpired = false;
        coolDownTimer = 0.0f;

        if (cooldownExpireCorotine == null)
        {
            cooldownExpireCorotine = Scheduler.PlayCoroutine(ResetCoolDown());
        }
    }


    private IEnumerator ResetCoolDown()
    {
        while (coolDownTimer < CoolDown)
        {
            yield return null;

            coolDownTimer += Time.deltaTime;
        }

        IsCoolDownExpired = true;
        cooldownExpireCorotine = null;
    }


    public virtual void Charge()
    {
        OnStart?.Invoke();
    }


    public virtual void Initialize(Fighter fighter)
    {
        ownerFighter = fighter;

        OnStart += OnStartCharge;
        OnFinish += OnFinishCharge;

        ownerFighter.MovementController.OnDoubleJump += MovementController_OnDoubleJump;
    }


    public virtual void Deinitialize()
    {
        OnStart -= OnStartCharge;
        OnFinish -= OnFinishCharge;

        ownerFighter.MovementController.OnDoubleJump -= MovementController_OnDoubleJump;
    }


    public object Clone()
    {
        return Instantiate(this);
    }


    protected virtual void OnStartCharge()
    {
        isCharging = true;
    }


    protected virtual void OnFinishCharge()
    {
        isCharging = false;
    }


    protected virtual void OnStopNearFighter(Fighter anotherFighter)
    {
    }


    public void MovementController_OnDoubleJump()
    {
        if (IsFamiliarCoolDownWithDoubleJump)
        {
            SetCoolDown();
        }
    }

}
