﻿using UnityEngine;


[CreateAssetMenu(fileName = "RalphChargeAbility", menuName = "Settings/Charge/RalphChargeAbility")]
public class RalphChargeAbility : MovableChargeAbility
{

    [Header("Melee Settings")]
    public Ability.Name abilityNameToResetOnFinish = default;

    [Header("Range Settings")]
    public int PointToAddOnFinish = default;
    public float AdditionalDamage = default;


    public override bool IsChargeAvailable => ownerFighter.MovementController.IsGrounded;



    private void OnAttack(FighterCombatData data)
    {
        ownerFighter.CombatController.RemoveAdditionalDamage(AdditionalDamage);

        ownerFighter.CombatController.OnAttack -= OnAttack;
    }


    protected override void OnFinishCharge()
    {
        base.OnFinishCharge();

        if (ownerFighter.CombatController.IsMelee)
        {
            if (ownerFighter.AbilityController.currentAbilities.Find(element => element.AbilityName == abilityNameToResetOnFinish) is ActiveAbility activeAbility)
            {
                activeAbility.FastResetCoolDown();
            }
        }
        else if (ownerFighter.CombatController.IsRange)
        {
            TechnologicalWeaponAbility technologicalWeaponAbility = ownerFighter.AbilityController.currentAbilities
                                                                    .Find(element => element.AbilityName == Ability.Name.TechnologicalWeapon) as TechnologicalWeaponAbility;

            for (int i = 0; i < PointToAddOnFinish; i++)
            {
                technologicalWeaponAbility.IncrementCurrentChargePoints();
            }

            ownerFighter.CombatController.AddAdditionalDamage(AdditionalDamage);
            ownerFighter.CombatController.OnAttack += OnAttack;
        }
    }
}
