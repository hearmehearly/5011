﻿using UnityEngine;


[CreateAssetMenu(fileName = "WitchChargeAbility", menuName = "Settings/Charge/WitchChargeAbility")]
public class WitchChargeAbility : ChargeAbility
{

    public FxHandler fxHandlerBegin = default;
    public FxHandler fxHandlerEnd = default;

    public float ChargeDistance = default;
    public float AstralDuration = default;


    [Tooltip("домаг который будет нанесен в радиусе(damageRadius) после окончания рывка")]
    public DamageValue damageValue = default;
    [Tooltip("радиус длядомага")]
    public float damageRadius = default;

    private Vector2 velocityDelta;
    private Vector2 currentChargeVelocity;



   // public override bool IsChargeAvailable => ownerFighter.MovementController.IsGrounded;

    protected override void OnStartCharge()
    {
        base.OnStartCharge();

        // move in astral TODO: LOGIC FOR BORDERS
        ownerFighter.MovementController.MainRigidbody.isKinematic = true;
        ownerFighter.MovementController.ForceStop();

        ownerFighter.MovementController.MovementBlokers++;
        ownerFighter.MovementController.JumpBlokers++;

        ownerFighter.MainCollider.enabled = false;
        ownerFighter.AnimationController.SetGraphicsEnabled(false);

        fxHandlerBegin.PlayOnce(ownerFighter.transform.position);

        TaskInvoker.Instance.CallMethodWithDelay(this, () =>
        {
            Vector3 targetPosition = ownerFighter.MovementController.MainRigidbody.position;
            targetPosition.x = targetPosition.x + (ChargeDistance * ownerFighter.MovementController.VelocityMultiplier);

            ownerFighter.transform.position = targetPosition;
            OnFinishCharge();
        }, AstralDuration);
    }


    protected override void OnFinishCharge()
    {
        base.OnFinishCharge();

        var fightersInRadius = PhysicsManager.Instance.OverlapFightersInRadius(ownerFighter.transform.position, damageRadius, ownerFighter);
        
        foreach (var fighter in fightersInRadius)
        {
            fighter.ApplyHit(damageValue, ownerFighter);
        }

        ownerFighter.MainCollider.enabled = true;
        ownerFighter.MovementController.MainRigidbody.isKinematic = false;
        ownerFighter.AnimationController.SetGraphicsEnabled(true);

        ownerFighter.MovementController.MovementBlokers--;
        ownerFighter.MovementController.JumpBlokers--;

        fxHandlerEnd.PlayOnce(ownerFighter.transform.position);
    }
}