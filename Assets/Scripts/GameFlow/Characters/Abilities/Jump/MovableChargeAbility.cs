﻿using UnityEngine;


public class MovableChargeAbility : ChargeAbility
{
    public float DistanceToStop = 2.5f;

    public float ChargeDistance = default;
    public Vector2 ChargeVelocty = default;

    private Vector2 velocityDelta;
    private Vector2 currentChargeVelocity;



    public override void Charge()
    {
        base.Charge();

        PhysicsManager.Instance.AddDistanceRigidbody(ownerFighter.MovementController.MainRigidbody, ChargeDistance, OnFinishCharge);
    }


    protected override void OnStartCharge()
    {
        base.OnStartCharge();

        Scheduler.OnFixedUpdate += Scheduler_OnFixedUpdate;

                ownerFighter.MovementController.MovementBlokers++;

        Vector2 chargeVelocityWithSide = ChargeVelocty * ownerFighter.MovementController.VelocityMultiplier;
        velocityDelta = chargeVelocityWithSide - ownerFighter.MovementController.MainRigidbody.velocity;

        ownerFighter.MovementController.MainRigidbody.velocity += velocityDelta;
        currentChargeVelocity = ownerFighter.MovementController.MainRigidbody.velocity;
    }


    protected override void OnFinishCharge()
    {
        base.OnFinishCharge();

        Scheduler.OnFixedUpdate -= Scheduler_OnFixedUpdate;

        ownerFighter.MovementController.MovementBlokers--;

        ownerFighter.MovementController.MainRigidbody.velocity -= velocityDelta;
        ownerFighter.MovementController.ForceStop();

        PhysicsManager.Instance.RemoveDistanceRigidbody(ownerFighter.MovementController.MainRigidbody);
    }


    protected virtual void Scheduler_OnFixedUpdate(float fixedDeltaTIme)
    {
        ownerFighter.MovementController.MainRigidbody.velocity = currentChargeVelocity;
#warning can be as common utility ??

        RaycastHit2D[] hits = Physics2D.RaycastAll(ownerFighter.transform.position, ownerFighter.transform.right * ownerFighter.transform.localScale.x);

        foreach (var hit in hits)
        {
            Fighter foundFighter = hit.collider.GetComponent<Fighter>();

            if (foundFighter != null && !foundFighter.Equals(ownerFighter))
            {
                float distance = Vector3.Distance(ownerFighter.transform.position, foundFighter.transform.position);

                if (distance < DistanceToStop)
                {
                    OnStopNearFighter(foundFighter);

                    OnFinish?.Invoke();
                }

                break;
            }
        }
    }
}
