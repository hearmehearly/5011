﻿using Spine.Unity;
using UnityEngine;


[CreateAssetMenu(fileName = "LeivChargeAbility", menuName = "Settings/Charge/LeivChargeAbility")]
public class LeivChargeAbility : MovableChargeAbility
{
    public DataDamageValue DamageValue = default;
    public ControlValue ControlValue = default;


#pragma warning disable 0414
    //only for reflection
    [SerializeField] private SkeletonDataAsset skeletonDataAsset = default;
#pragma warning restore 0414

    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string chargeLoopAnimationName = default;

    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string chargeEndAnimationName = default;


    public override bool IsChargeAvailable => ownerFighter.MovementController.IsGrounded;


    protected override void OnStopNearFighter(Fighter anotherFighter)
    {
        base.OnStopNearFighter(anotherFighter);

        if (anotherFighter != null)
        {
            anotherFighter.ControlController.ApplyControl(ControlValue, ownerFighter);
            anotherFighter.ApplyHit(null,
                DamageValue.GetDamage(), 
                DamageValue.damageType, 
                DamageValue.HitImpuls * ownerFighter.transform.lossyScale.normalized,
                DamageValue.StunDurationPerHit,
                DamageValue.ShouldKnockOver,
                ownerFighter);
        }
    }

    protected override void OnStartCharge()
    {
        ownerFighter.MovementController.ForceStop();
        ownerFighter.ControlController.ApplyControl(ControlType.InputDisable, float.MaxValue, this);

        base.OnStartCharge();

        ownerFighter.AnimationController.SetHitAnimation(chargeLoopAnimationName, true);
        ownerFighter.AnimationController.AllowChangeAnimation = false;

    }


    protected override void OnFinishCharge()
    {
        ownerFighter.ControlController.FinishControl(ControlType.InputDisable, this);

        ownerFighter.AnimationController.AllowChangeAnimation = true;
        ownerFighter.AnimationController.PlayAbilityAnimation(chargeEndAnimationName);

        base.OnFinishCharge();
    }

}
