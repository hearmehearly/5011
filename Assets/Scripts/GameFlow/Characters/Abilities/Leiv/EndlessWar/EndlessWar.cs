﻿using UnityEngine;


[CreateAssetMenu(fileName = "EndlessWar", menuName = "Settings/Ability/Leiv/EndlessWar")]
public class EndlessWar : PassiveAbility
{
    [Space]
    [Header("Ability Settings")]

    public int maxCount = default;
    public float percentToIncreaseHealth = default;
    public float deathRingDurationIncreasePerCount = default;

    public int CurrentCounter { get; private set; }



    public override void Initialize(Fighter fighter)
    {
        base.Initialize(fighter);

        fighter.CombatController.OnGottaKill += IncreaseCounter;
        fighter.OnDie += ResetCounter;
    }


    public override void Deinitialize()
    {
        base.Deinitialize();

        ownerFighter.CombatController.OnGottaKill -= IncreaseCounter;
        ownerFighter.OnDie -= ResetCounter;
    }


    private void IncreaseCounter(Fighter fighter, Fighter fighterToKill)
    {
        if (CurrentCounter < maxCount)
        {
            CurrentCounter++;
            ownerFighter.HealthController.IncreaseHealth(percentToIncreaseHealth);

            if (ownerFighter.AbilityController.GetAbility(Name.DeathRing) is DeathRingAbility deathRingAbility)
            {
                deathRingAbility.IncreaseDuration(deathRingDurationIncreasePerCount);
            };

            TriggerDisplayInfoEvent(CurrentCounter.ToString());
        }
    }


    private void ResetCounter()
    {
        ownerFighter.HealthController.DecreaseHealth(percentToIncreaseHealth * CurrentCounter);

        if (ownerFighter.AbilityController.GetAbility(Name.DeathRing) is DeathRingAbility deathRingAbility)
        {
            deathRingAbility.IncreaseDuration(deathRingDurationIncreasePerCount);
        };

        CurrentCounter = 0;
    }
}
