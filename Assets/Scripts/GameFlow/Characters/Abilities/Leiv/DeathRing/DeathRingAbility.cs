﻿using UnityEngine;


[CreateAssetMenu(fileName = "DeathRing ", menuName = "Settings/Ability/Leiv/DeathRing")]
public class DeathRingAbility : OnceResponceAbility
{
    [Space]
    [Header("Ability Settings")]
    public DeathRing.Data Data = default;
    public DeathRing deathRingPrefab = default;

    private DeathRing deathRing;

    public override bool AllowUse => base.AllowUse && ownerFighter.MovementController.IsGrounded;


    public override void UseAbility()
    {
        base.UseAbility();

        deathRing = Instantiate(deathRingPrefab, ownerFighter.transform.position, Quaternion.identity);
        deathRing.Initialize(Data, ownerFighter);

        ownerFighter.MovementController.ForceStop();
        
        foreach (var ability in ownerFighter.AbilitiesData.Abilities) // reset all but this abilities
        {
            if (!ability.Equals(this))
            {
                if (ability.AbilityName == Name.StoneArmor && (ability as DoubleUseAbility).IsSecondUseAvailable)
                {
                    return;
                }

                ownerFighter.AbilityController.FastResetCooldown(ability);
            }
        }
    }


    public void IncreaseDuration(float percent)
    {
        Data.Duration += Data.Duration * percent;
    }


    public void DecreaseDuration(float percent)
    {
        Data.Duration -= Data.Duration * percent;
    }

    protected override void PlayAbilityAnimation()
    {
        ownerFighter.AnimationController.PlayAbilityAnimation(abilityAnimation, null, index: 0);
    }
}
