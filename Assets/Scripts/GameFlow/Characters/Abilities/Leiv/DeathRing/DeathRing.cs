﻿using System;
using UnityEngine;


public class DeathRing : MonoBehaviour
{
    [Serializable]
    public struct Data
    {
        public float Duration;

        [Range(0.0f, 1.0f)]
        public float PartHealthToReduceOnCollision;
        public Vector2 Impuls;

        public float PartForAbilityCooldown;
        public float AdditionalDamageMultiplier;
    }

    [SerializeField]
    private CollidableObject collidableObject = default;
    [SerializeField]
    private CollidableObject collidableBorder = default; // border
    [SerializeField]
    private CircleCollider2D currentCollider = default;

    private Data currentData;
    private Fighter owner;
    private Coroutine autoDeinitializeRoutine;


    public void Initialize(Data data, Fighter ownerValue)
    {
        currentData = data;
        owner = ownerValue;

        collidableObject.OnCustomTriggerEnter2D += HandleEnterCollision;
        collidableObject.OnCustomTriggerExit2D += HandleExitCollision;

        collidableBorder.OnCustomTriggerEnter2D += MoveInCenter;

        owner.CombatController.DamageMultiplier += currentData.AdditionalDamageMultiplier;
        owner.OnDeinitialize += Deinitialize;

        if (autoDeinitializeRoutine == null)
        {
            autoDeinitializeRoutine = Scheduler.PlayMethodWithDelay(Deinitialize, currentData.Duration);
        }
    }


    public void Deinitialize()
    {
        if (autoDeinitializeRoutine != null)
        {
            Scheduler.StopPlayingCoroutine(ref autoDeinitializeRoutine);
        }

        collidableObject.OnCustomTriggerEnter2D -= HandleEnterCollision;
        collidableObject.OnCustomTriggerExit2D -= HandleExitCollision;

        collidableBorder.OnCustomTriggerEnter2D -= MoveInCenter;

        owner.CombatController.DamageMultiplier -= currentData.AdditionalDamageMultiplier;
        owner.OnDeinitialize -= Deinitialize;

        var fighters = PhysicsManager.Instance.OverlapFightersInRadius(transform.position, currentCollider.radius, owner);

        foreach (var fighter in fighters)
        {
            AllowMobility(fighter);
        }

        Destroy(gameObject);
    }


    public void SetDuration(float duration)
    {
        currentData.Duration = duration;
    }


    private void HandleEnterCollision(Collider2D collider, CollidableObject.Type type)
    {
        Fighter collidedFighter = collider.GetComponent<Fighter>();

        if (collidedFighter != null && !collidedFighter.Equals(owner)) // check if this is not owner
        {
            foreach (var ability in collidedFighter.AbilitiesData.Abilities) // increase cooldown
            {
                if (ability is ActiveAbility activeAbility)
                {
                    float addCoolDown = collidedFighter.AbilityController.CurrentCooldownTimer(activeAbility) * currentData.PartForAbilityCooldown;
                    collidedFighter.AbilityController.TryIncreaseCoolDown(ability, addCoolDown);
                }
            }

            ForbidMobility(collidedFighter);
        }

        //if (type == CollidableObject.Type.NPCFighter || type == CollidableObject.Type.Player)
        //{
        //   Fighter fighter = collider.GetComponent<Fighter>();

        //   RaycastHit2D hit2D = Physics2D.Raycast(fighter.transform.position, fighter.MovementController.Velocity, float.MaxValue, 1 << LayerMask.NameToLayer("Ability"));

        //    //Debug.Log(hit2D.normal);
        //    //Debug.DrawLine(hit2D.collider.transform.position, new Vector3(hit2D.collider.transform.position.x + 0.1f, hit2D.collider.transform.position.y + 0.1f, hit2D.collider.transform.position.z), Color.red, 1);
        //    //Debug.DrawRay(hit2D.collider.transform.position, hit2D.normal, Color.red, 1);
        //    Vector2 dir = new Vector2(transform.position.x, transform.position.y) - hit2D.point;

        //    bool isLeavingRing = Mathf.Sign(dir.x) == Math.Sign(hit2D.normal.x) || Mathf.Sign(dir.y) == Math.Sign(hit2D.normal.y);
        //    //Debug.Log(isLeavingRing);

        //    if (isLeavingRing)
        //    {
        //        fighter.MovementController.AddRelativeToSideForce(currentData.Impuls);

        //        float HealthHitValue = fighter.HealthController.CurrentHealth * currentData.PartHealthToReduceOnCollision;

        //        fighter.ApplyHit(HealthHitValue);
        //        fighter.ControlController.FinishControl(this);
        //    }
        //}
    }


    private void HandleExitCollision(Collider2D collider, CollidableObject.Type type)
    {
        Fighter collidedFighter = collider.GetComponent<Fighter>();

        if (collidedFighter != null)
        {
            float HealthHitValue = collidedFighter.HealthController.CurrentHealth * currentData.PartHealthToReduceOnCollision;

            collidedFighter.ApplyHit(HealthHitValue, DamageType.Instanc, owner);
            collidedFighter.ControlController.FinishControl(this);
            
            if (owner.Equals(collidedFighter))
            {
                Deinitialize();
            }
            else
            {
                AllowMobility(collidedFighter);
            }
        }
    }


    private void MoveInCenter(Collider2D collider, CollidableObject.Type type)
    {
        Fighter collidedFighter = collider.GetComponent<Fighter>();

        if (collidedFighter != null && !collidedFighter.Equals(owner)) // check if this is not owner
        {
            Vector2 directionToRingCenter = transform.position - collidedFighter.transform.position;
            collidedFighter.MovementController.Velocity = currentData.Impuls * directionToRingCenter;  // inside  add force to twist inside center
        }
    }


    private void ForbidMobility(Fighter fighter)
    {
        fighter.MovementController.ChargeBlockers++;
        fighter.MovementController.JumpBlokers++;
    }


    private void AllowMobility(Fighter fighter)
    {
        fighter.MovementController.ChargeBlockers--;
        fighter.MovementController.JumpBlokers--;
    }
}
