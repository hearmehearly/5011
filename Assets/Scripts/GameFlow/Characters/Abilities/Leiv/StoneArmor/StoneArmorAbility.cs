﻿using UnityEngine;


[CreateAssetMenu(fileName = "StoneArmor ", menuName = "Settings/Ability/Leiv/StoneArmor")]
public class StoneArmorAbility : DoubleUseAbility
{
    [Space]
    [Header("Ability Settings")]
    public StoneArmor.Data Data = default;
    public StoneArmor stoneArmorPrefab = default;

    private StoneArmor stoneArmor;



    public override void UseAbility()
    {
        base.UseAbility();

        stoneArmor = Instantiate(stoneArmorPrefab, ownerFighter.transform.position, Quaternion.identity, ownerFighter.transform);
        stoneArmor.Initialize(Data, ownerFighter);
        stoneArmor.OnAbsorbEnd += OnAbsorbEnd;
    }


    public override void UseAbilitySecond()
    {
        base.UseAbilitySecond();

        stoneArmor.Explode();
    }


    private void OnAbsorbEnd()
    {
        stoneArmor.OnAbsorbEnd -= OnAbsorbEnd;
        MarkUsedSecondary();
    }
}
