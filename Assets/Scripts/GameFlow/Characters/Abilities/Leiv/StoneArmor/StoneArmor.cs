﻿using System;
using UnityEngine;


public class StoneArmor : MonoBehaviour
{
    [Serializable]
    public struct Data
    {
        public DamageType explosionDamageType;
        public float Duration;

        public float MaxDamageToAbsorb;

        [Range(0.0f, 1.0f)]
        public float DamageToReducPart;

        public float ExplodeRadius;
        public ControlValue controlValueAfterExplode;
    }

    public event Action OnAbsorbEnd;

    private float currentAbsorbValue;

    private Data currentData;
    private Fighter owner;
    private Coroutine autoDeinitializeRoutine;


    public void Initialize(Data data, Fighter ownerValue)
    {
        currentData = data;
        owner = ownerValue;

        owner.HealthController.OnApplyHit += AbsorbDamage;
        owner.HealthController.AppliedDamageMultiplier -= currentData.DamageToReducPart;

        owner.OnDeinitialize += Deinitialize;

        if (autoDeinitializeRoutine == null)
        {
            autoDeinitializeRoutine = Scheduler.PlayMethodWithDelay(Deinitialize, currentData.Duration);
        }
    }



    public void Deinitialize()
    {
        if (autoDeinitializeRoutine != null)
        {
            Scheduler.StopPlayingCoroutine(ref autoDeinitializeRoutine);
        }

        owner.HealthController.OnApplyHit -= AbsorbDamage;
        owner.HealthController.AppliedDamageMultiplier += currentData.DamageToReducPart;

        owner.OnDeinitialize -= Deinitialize;

        OnAbsorbEnd?.Invoke();

        Destroy(gameObject);
    }


    public void Explode()
    {
        Collider2D[] overlapColliders = Physics2D.OverlapCircleAll(transform.position, currentData.ExplodeRadius);

        foreach (var collider in overlapColliders)
        {
            Fighter collidedFighter = collider.GetComponent<Fighter>();

            if (collidedFighter != null && !collidedFighter.Equals(owner)) // check if not owner
            {
                float restAbsorbValue = currentData.MaxDamageToAbsorb - currentAbsorbValue;
                collidedFighter.ApplyHit(restAbsorbValue, currentData.explosionDamageType, owner);

                collidedFighter.ControlController.ApplyControl(currentData.controlValueAfterExplode, owner);
            }
        }

        Deinitialize();
    }


    private void AbsorbDamage(float hitValue)
    {
        float fullDamagePerHit = hitValue / (1.0f - owner.HealthController.AppliedDamageMultiplier);
        currentAbsorbValue += fullDamagePerHit * owner.HealthController.AppliedDamageMultiplier;

        if (currentAbsorbValue > currentData.MaxDamageToAbsorb)
        {
            Deinitialize();
        }
    }
}
