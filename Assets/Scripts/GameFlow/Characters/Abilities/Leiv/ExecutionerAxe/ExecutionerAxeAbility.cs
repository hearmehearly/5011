﻿using Spine.Unity;
using System;
using UnityEngine;


[CreateAssetMenu(fileName = " ExecutionerAxe ", menuName = "Settings/Ability/Leiv/ExecutionerAxe")]
public class ExecutionerAxeAbility : SpriteSwapOnHoldAbility
{
    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string onPressAbilityAnimation = default;

    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string idleAbilityAnimation = default;

    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string[] onClickAbilityAnimations = default;

    public float secondForSecondPhase = default;

    public AbilityHitZone.Data FirstPhaseData = default;
    public AbilityHitZone.Data SecondPhaseData = default;

    public AbilityHitZone executionerAxePrefab = default;
    
    private Guid holdInputDisableGuid;


    public override bool AllowUse => base.AllowUse && ownerFighter.MovementController.IsGrounded;

    public override void Initialize(Fighter fighter)
    {
        base.Initialize(fighter);

        ownerFighter.ControlController.FinishControl(holdInputDisableGuid);
        holdInputDisableGuid = Guid.NewGuid();
}

    public override void UseAbility()
    {
        base.UseAbility();

        AbilityHitZone executionerAxe = Instantiate(executionerAxePrefab,
                                                    ownerFighter.CombatController.HitRoot.position,
                                                    Quaternion.identity,
                                                    ownerFighter.CombatController.HitRoot);

        if (holdTimer <= secondForSecondPhase)
        {
            executionerAxe.Initialize(ownerFighter, FirstPhaseData);
        }
        else
        {
            executionerAxe.Initialize(ownerFighter, SecondPhaseData);
        }


        TriggerShouldChangeSpriteEvent(UISprite);

        ownerFighter.ControlController.OnControlStarted -= StopUsingAbility;
        OnHoldTimerChange -= OnTimerChange;

        Physics2D.IgnoreCollision(ownerFighter.MainCollider, executionerAxe.HitCollider);
        TaskInvoker.Instance.UnscheduleAllMethodForTarget(this);
        
        ownerFighter.ControlController.FinishControl(holdInputDisableGuid);
    }


    public override void Deinitialize()
    {
        ownerFighter.ControlController.OnControlStarted -= StopUsingAbility;
        OnHoldTimerChange -= OnTimerChange;
        
        ownerFighter.ControlController.FinishControl(holdInputDisableGuid);
        TaskInvoker.Instance.UnscheduleAllMethodForTarget(this);

        base.Deinitialize();
    }


    public override void OnPress()
    {
        if (isCoolDownExpired && ownerFighter.MovementController.IsGrounded)
        {
            base.OnPress();

            Action idle = () =>
            {
                if (IsPreparedForUse)
                {
                    ownerFighter.AnimationController.SetHitAnimation(idleAbilityAnimation, true);
                }
            };
            ownerFighter.AnimationController.PlayAbilityAnimation(onPressAbilityAnimation, idle);

            ownerFighter.ControlController.ApplyControl(ControlType.InputDisable, float.MaxValue, holdInputDisableGuid);
            ownerFighter.MovementController.ForceStop();

            ownerFighter.ControlController.OnControlStarted += StopUsingAbility;
            OnHoldTimerChange += OnTimerChange;
        }
        else
        {
            IsPreparedForUse = false;
        }
    }


    private void StopUsingAbility(ControlType arg1, object arg2)
    {
        if (arg1 == ControlType.Stun)
        {
            ownerFighter.ControlController.OnControlStarted -= StopUsingAbility;
            OnHoldTimerChange -= OnTimerChange;
            TriggerShouldChangeSpriteEvent(UISprite);
        }

        TaskInvoker.Instance.UnscheduleAllMethodForTarget(this);
        ownerFighter.ControlController.FinishControl(ControlType.InputDisable, holdInputDisableGuid);
    }

    private void OnTimerChange()
    {
        if (holdTimer > secondForSecondPhase)
        {
            TriggerShouldChangeSpriteEvent(UIAnotherSprite);
            OnHoldTimerChange -= OnTimerChange;
        }
    }

    protected override void PlayAbilityAnimation()
    {
        ownerFighter.AnimationController.PlayAbilityAnimation(onClickAbilityAnimations.RandomObject());
        //base.PlayAbilityAnimation();
    }
}
