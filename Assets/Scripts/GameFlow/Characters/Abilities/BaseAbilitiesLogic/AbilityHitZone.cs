﻿using System;


public class AbilityHitZone : FighterMeleeHitZone
{
    [Serializable]
    public struct Data
    {
        public bool shouldOverKnockOnAction;

        public DamageValue DamageValue;
        public float HitDuration;

        public DebuffValue[] Debuf;
        public ControlValue[] Control;
    }

    event Action<Fighter> OnFighterEnterArea;

    Data currentData;
    Fighter owner;



    public void Initialize(Fighter fighter, Data data, Action deinitializeCallback = null)
    {
        owner = fighter;
        currentData = data;
        EnableZone();

        Scheduler.PlayMethodWithDelay(() =>
        {
            DisableZone();
            OnFighterEnterArea = null;
            deinitializeCallback?.Invoke();

            Destroy(gameObject);
        }, currentData.HitDuration);
    }


    public void AddOnFighterEnterAreaCallback(Action<Fighter> callback)
    {
        OnFighterEnterArea += callback;
    }


    protected override void ProcessNPCCollision(Fighter fighter)
    {
        base.ProcessNPCCollision(fighter);

#warning default??
        fighter.ApplyHit(null,
            currentData.DamageValue.GetDamage(),
            currentData.DamageValue.damageType,
            currentData.DamageValue.HitImpuls * transform.lossyScale.normalized,
            default,
            currentData.shouldOverKnockOnAction,
            owner);

        foreach (var control in currentData.Control)
        {
            fighter.ControlController.ApplyControl(control, owner);
        }

        foreach (var debuf in currentData.Debuf)
        {
            fighter.DebuffController.ApplyDebuff(debuf, owner);
        }

        OnFighterEnterArea?.Invoke(fighter);
    }
}
