﻿using System;


public class OnceResponceAbility : ActiveAbility
{
    public event Action OnHoldTimerChange;

    protected float holdTimer = 0.0f;



    public void StartHoldTimer()
    {
        Scheduler.OnUpdate += IncreaseHoldTimer;
    }


    public void StopHoldTimer()
    {
        Scheduler.OnUpdate -= IncreaseHoldTimer;
    }


    public void ResetHoldTimer()
    {
        holdTimer = default;

        OnHoldTimerChange?.Invoke();
    }


    public virtual void OnPress()
    {
    }


    protected virtual void IncreaseHoldTimer(float deltaTime)
    {
        holdTimer += deltaTime;

        OnHoldTimerChange?.Invoke();
    }
}
