﻿using Spine.Unity;
using System;
using System.Collections;
using UnityEngine;


public abstract class DoubleUseAbility : ActiveAbility
{
    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string secondAbilityAnimation = default;

    public event Action<Ability> OnFirstTimeUsed;
    public event Action<Ability> OnSecondTimeUsed;

    [Space]
    [Header("Double Use Ability Settings")]
    public Sprite SecondaryUISprite = default;
    public float SecondsForSecondPhase = default;

    public virtual bool IsSecondUseAvailable { get; protected set; }
    
    private float currentPhaseTimer;



    public override void UseAbility()
    {
        base.UseAbility();

        currentPhaseTimer = 0.0f;
        Scheduler.OnUpdate += Scheduler_OnUpdate;
        OnFirstTimeUsed?.Invoke(this);
    }


    public virtual void UseAbilitySecond()
    {
        MarkUsedSecondary();

        PlaySecondAbilityAnimation();
    }


    private void Scheduler_OnUpdate(float deltaTime)
    {
        currentPhaseTimer += deltaTime;

        if (currentPhaseTimer > SecondsForSecondPhase)
        {
            Scheduler.OnUpdate -= Scheduler_OnUpdate;
            IsSecondUseAvailable = true;
        }
    }


    protected override IEnumerator ResetCoolDown()
    {
        yield return base.ResetCoolDown();

        Scheduler.OnUpdate -= Scheduler_OnUpdate;
        IsSecondUseAvailable = false;

        yield return null;
    }


    protected void MarkUsedSecondary()
    {
        IsSecondUseAvailable = false;
        OnSecondTimeUsed?.Invoke(this);
    }


    protected virtual void PlaySecondAbilityAnimation() =>
        ownerFighter.AnimationController.PlayAbilityAnimation(secondAbilityAnimation);
}
