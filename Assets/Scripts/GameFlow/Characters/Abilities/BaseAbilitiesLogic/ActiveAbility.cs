﻿using Spine.Unity;
using System;
using System.Collections;
using UnityEngine;


public class ActiveAbility : Ability
{
    public enum Responce
    {
        None = 0,
        Press = 1,
        ClickUp = 2
    }

    public event Action<Ability> OnAbilityUsed;


    public event Action<Ability> OnAbilityBegin;
    public event Action<Ability> OnCooldownEnd;

    public float CoolDown = default;
    public bool IsPreparedForUse = default;

    [Header("Animation")]

    #pragma warning disable 0414
    //only for reflection
    [SerializeField] private SkeletonDataAsset skeletonDataAsset = default;
    #pragma warning restore 0414

    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string abilityAnimation = default;

    public float controlDisableAfterUseDuration = default;

    [Header("React Settings")]
    public Responce ResponceType = default;
    [NonSerialized]
    public bool isCoolDownExpired = true;
    [NonSerialized]
    public float coolDownTimer = default;

    private Coroutine cooldownExpireCorotine;



    public virtual void UseAbility()
    {
        ApplyOnUsedControl();
        PlayAbilityAnimation();

        OnAbilityUsed?.Invoke(this);
    }


    public virtual void UseAbility(float prepareDuration)
    {
        OnAbilityUsed?.Invoke(this);
    }


    public void SetCoolDown()
    {
        isCoolDownExpired = false;
        coolDownTimer = 0.0f;

        if (cooldownExpireCorotine == null)
        {
            OnAbilityBegin?.Invoke(this);
            cooldownExpireCorotine = Scheduler.PlayCoroutine(ResetCoolDown());
        }
    }
    

    public void FastResetCoolDown()
    {
        Scheduler.StopPlayingCoroutine(ref cooldownExpireCorotine);

        isCoolDownExpired = true;
        cooldownExpireCorotine = null;
        OnCooldownEnd?.Invoke(this);
    }


    public bool TryIncreaseCoolDown(float duration)
    {
        if (isCoolDownExpired)
        {
            return false;
        }
        else
        {
            coolDownTimer += duration;
            return true;
        }
    }


    protected virtual IEnumerator ResetCoolDown()
    {
        while (coolDownTimer < CoolDown)
        {
            yield return null;

            coolDownTimer += Time.deltaTime;
        }

        isCoolDownExpired = true;
        cooldownExpireCorotine = null;
        OnCooldownEnd?.Invoke(this);
    }


    public void IncreaseCooldown(float value) =>
        CoolDown += value;
    

    public void IncreaseCooldownPercent(float percent) =>
        CoolDown += CoolDown * percent;
    

    public void DecreaseCooldownPercent(float percent) =>
        CoolDown -= CoolDown * percent;

    protected virtual void PlayAbilityAnimation() =>
        ownerFighter.AnimationController.PlayAbilityAnimation(abilityAnimation);

    protected virtual void ApplyOnUsedControl()
    {
        ownerFighter.MovementController.ForceStop();
        ownerFighter.ControlController.ApplyControl(ControlType.InputDisable, controlDisableAfterUseDuration, this);
    }


    public override void Deinitialize()
    {
        ownerFighter.ControlController.FinishControl(ControlType.InputDisable, this);

        base.Deinitialize();
    }
}
