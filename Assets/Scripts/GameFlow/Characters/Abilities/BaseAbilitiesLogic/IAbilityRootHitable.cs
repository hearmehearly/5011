﻿using UnityEngine;


public interface IAbilityRootHitable
{
    void InitializeHitRoot(Transform hitRootValue);
}
