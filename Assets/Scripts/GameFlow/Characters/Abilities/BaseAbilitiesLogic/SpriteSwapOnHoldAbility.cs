﻿using UnityEngine;


public class SpriteSwapOnHoldAbility : OnceResponceAbility
{
    [Space]
    [Header("Sprite Swap Settings")]
    public Sprite UIAnotherSprite = default;
}
