﻿using UnityEngine;
using System;
using System.Collections.Generic;


public abstract class Ability : ScriptableObject, ICloneable
{
    public enum Name
    {
        None                    = 0,
        Test_1                  = 1,
        DeathRing               = 2, // Leiv
        ExecutionerAxe          = 3,
        EndlessWar              = 4,
        StoneArmor              = 5,
        TechnologicalWeapon     = 6, // Ralph
        ChoppingOffLimbs        = 7,
        PursuitAndDetention     = 8,
        Adaptation              = 9,
        BloodCurse              = 10, // Witch
        DarkAura                = 11,
        BatsSwarm               = 12,
        AwakeningPuppets        = 13,
        BurstHitPasive          = 14, // Mary
        ThrowKnife              = 15,
        SmokeBomb               = 16,
        SeriesOfMurders         = 17
    }

    public enum Type
    {
        Simple              = 0,
        Special             = 1
    }

    public event Action<Name, string> OnShouldDisplayTextInfo;
    public event Action<Ability, Sprite> OnShouldChangeUISprite;

    [Header("Common")]
    public Name AbilityName = default;
    public Type AbilityType = default;

    public Sprite UISprite = default;
    public bool shouldShowText = default;

    [Tooltip("Только если выполняется или во время подготовки")]
    public List<ControlType> ControlToStop = default;

    protected Fighter ownerFighter;


    public virtual bool AllowUse => true; // to override


    public object Clone()
    {
        return Instantiate(this);
    }


    public virtual void Initialize(Fighter fighter)
    {
        ownerFighter = fighter;
    }


    public virtual void Deinitialize()
    {
        TaskInvoker.Instance.UnscheduleAllMethodForTarget(this);
    }


    protected void TriggerDisplayInfoEvent(string text)
    {
        if (!shouldShowText)
        {
            Debug.LogWarning("Check text info settings in ability " + name);
        }

        OnShouldDisplayTextInfo?.Invoke(AbilityName, text);
    }

    protected void TriggerShouldChangeSpriteEvent(Sprite sprite)
    {
        OnShouldChangeUISprite?.Invoke(this, sprite);
    }
}
