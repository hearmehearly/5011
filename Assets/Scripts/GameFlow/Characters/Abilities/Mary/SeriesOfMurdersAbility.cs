﻿using Spine.Unity;
using UnityEngine;


[CreateAssetMenu(fileName = " SeriesOfMurders ", menuName = "Settings/Ability/Mary/SeriesOfMurders")]
public class SeriesOfMurdersAbility : OnceResponceAbility
{
    public DataDamageValue[] damageValues = default;

    public float delayBetweenHits = default;
    [Tooltip("длительность нахождения за спиной")]
    public float stayBackDuration = default;

    public float abilityRaduis = default;
    public float blinkOffset = default;

    private Fighter markedFighter;
    private Vector3 savedPosition;
    private Side savedSide;
    private int currentHitsCounter;

    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string returnAnimation = default;

    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string hitAnimation = default;

    public FxHandler fxHandler = default;

    private Fighter NearFighterToHit
    {
        get
        {
            Fighter result = default;

            var overlapedFighters = PhysicsManager.Instance.RaycastFightersInFaceArea(abilityRaduis, ownerFighter);

            if (overlapedFighters.Count != 0)
            {
                result = overlapedFighters[0];
            }

            return result;
        }
    }


    public override bool AllowUse => (NearFighterToHit != null);


    public override void UseAbility()
    {
        base.UseAbility();

        markedFighter = NearFighterToHit;
        savedPosition = ownerFighter.transform.position;
        savedSide = ownerFighter.MovementController.LookingSide;
        
        currentHitsCounter = 0;
      //  ownerFighter.AnimationController.AllowChangeAnimation = false;

        for (int i = 0; i < damageValues.Length; i++)
        {
            float delay = i * delayBetweenHits;
            TaskInvoker.Instance.CallMethodWithDelay(this, Hit, delay);
        }
    }


    public override void Deinitialize()
    {
        TaskInvoker.Instance.UnscheduleAllMethodForTarget(this);
      //  ownerFighter.AnimationController.AllowChangeAnimation = true;

        base.Deinitialize();
    }


    private void Hit()
    {
        if (markedFighter != null && markedFighter.HealthController.IsAlive)
        {
            //savedPosition = ownerFighter.transform.position;

            float vectorOffset = markedFighter.MovementController.VelocityMultiplier * blinkOffset;

            Vector3 blinkPosition = markedFighter.transform.position.SetX(markedFighter.transform.position.x + vectorOffset);
            ownerFighter.MovementController.MainRigidbody.position = blinkPosition;

            ownerFighter.MovementController.MoveSide(markedFighter.MovementController.LookingSide);
            ownerFighter.MovementController.Stop();

            markedFighter.ApplyHit(damageValues[currentHitsCounter], ownerFighter);
            
            currentHitsCounter++;

            ownerFighter.AnimationController.PlayAbilityAnimation(hitAnimation);

            fxHandler.PlayOnce(ownerFighter.transform.position);

            TaskInvoker.Instance.CallMethodWithDelay(this, () =>
            {
                ownerFighter.MovementController.Flip(savedSide);

                ownerFighter.MovementController.MainRigidbody.position = savedPosition;

                ownerFighter.AnimationController.PlayAbilityAnimation(returnAnimation);
                fxHandler.PlayOnce(ownerFighter.transform.position);
            }, stayBackDuration);
        }
    }


    protected override void PlayAbilityAnimation()
    {
        // controlled by lc
    }
}
