﻿using UnityEngine;


[CreateAssetMenu(fileName = "SmokeBomb ", menuName = "Settings/Ability/Mary/SmokeBomb")]
public class SmokeBombAbility : OnceResponceAbility
{
    [Space]
    [Header("Ability Settings")]
    public SmokeBomb.Data Data = default;
    public SmokeBomb smokeBombPrefab = default;

    private SmokeBomb smokeBomb;


    public override void UseAbility()
    {
        base.UseAbility();

        smokeBomb = Instantiate(smokeBombPrefab, ownerFighter.transform.position, Quaternion.identity);
        smokeBomb.Initialize(Data, ownerFighter);
    }


    public override void Deinitialize()
    {
        if (smokeBomb != null)
        {
            smokeBomb.Deinitialize();
        }

        base.Deinitialize();
    }
}
