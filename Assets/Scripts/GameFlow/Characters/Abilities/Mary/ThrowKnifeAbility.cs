﻿using UnityEngine;


[CreateAssetMenu(fileName = " ThrowKnifeAbility ", menuName = "Settings/Ability/Mary/ThrowKnifeAbility")]
public class ThrowKnifeAbility : DoubleUseAbility
{
    [Space]
    [Header("Blink Settings")]
    public float blinkOffset = default;
    [Space]
    [Header("Knife Settings")]
    public float cameraSmoothMultiplyFactor = default;
    public float cameraSmoothMutiplyDuration = default;

    public FighterAbilityProjectile projectilePrefab = default;
    public FighterAbilityProjectile.Data projectileData = default;

    private FighterAbilityProjectile hitZone;
    private bool isKnifeHittedTarget;
    private Fighter hittedFighter;

    private SchedulerTask cameraSmoothTask;


    public override bool IsSecondUseAvailable => isKnifeHittedTarget &&
        hittedFighter != null &&
        hittedFighter.HealthController.IsAlive;


    public override void Deinitialize()
    {
        if (cameraSmoothTask != null)
        {
            ResetCamerSmoothTime();
        }

        base.Deinitialize();
    }

    public override void UseAbility()
    {
        base.UseAbility();

        hitZone = Instantiate(projectilePrefab,
                              ownerFighter.CombatController.HitRoot.position,
                              Quaternion.identity);

        hitZone.Initialize(projectileData, ownerFighter, () => hitZone.OnHitFighter -= HitZone_OnHitFighter);
        hitZone.OnHitFighter += HitZone_OnHitFighter;


        Physics2D.IgnoreCollision(ownerFighter.MainCollider, hitZone.HitCollider);

        isKnifeHittedTarget = false;
        hittedFighter = null;
    }

    public override void UseAbilitySecond()
    {
        base.UseAbilitySecond();

        if (IsSecondUseAvailable)
        {
            float vectorOffset = hittedFighter.MovementController.VelocityMultiplier * blinkOffset;
            
            Vector3 blinkPosition = hittedFighter.transform.position.SetX(hittedFighter.transform.position.x + vectorOffset);
            ownerFighter.MovementController.MainRigidbody.position = blinkPosition;

            ownerFighter.MovementController.MoveSide(hittedFighter.MovementController.LookingSide);
            ownerFighter.MovementController.Stop();

            if (ownerFighter is PlayerFighter playerFighter &&
                !Mathf.Approximately(cameraSmoothMultiplyFactor, default))
            {
                playerFighter.PlayerCameraController.MultiplySmoothTime(cameraSmoothMultiplyFactor);
            }

            cameraSmoothTask = TaskInvoker.Instance.CallMethodWithDelay(this, ResetCamerSmoothTime, cameraSmoothMutiplyDuration);
        }
    }
    

    private void HitZone_OnHitFighter(Fighter obj)
    {
        hittedFighter = obj;
        isKnifeHittedTarget = true;
    }


    private void ResetCamerSmoothTime()
    {
        if (ownerFighter is PlayerFighter playerFighter &&
            !Mathf.Approximately(cameraSmoothMultiplyFactor, default))
        {
            playerFighter.PlayerCameraController.MultiplySmoothTime(1.0f / cameraSmoothMultiplyFactor);
        }

        cameraSmoothTask = null;
    }
}
