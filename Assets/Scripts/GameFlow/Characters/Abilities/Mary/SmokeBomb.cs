﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;


public class SmokeBomb : MonoBehaviour
{
    [Serializable]
    public struct Data
    {
        public float Duration;
        public float OwnerGraphicDisableDuration;

        [Range(0.0f, 1.0f)] public float alphaPlayer;
        [Range(0.0f, 1.0f)] public float alphaFighter;
    }


    [SerializeField]
    private CollidableObject collidableObject = default;

    [SerializeField]
    private FxHandler[] handlersPrefbs = default;

    private FxHandler currentFxHandler;

    private Data currentData;
    private Fighter owner;
    private readonly List<Fighter> fightersInside = new List<Fighter>();


    public void Initialize(Data data, Fighter ownerValue)
    {
        currentData = data;
        owner = ownerValue;

        collidableObject.OnCustomTriggerEnter2D += HandleEnterCollision;
        collidableObject.OnCustomTriggerExit2D += HandleExitCollision;

        owner.CombatController.OnAttack += CombatController_OnAttack;

        TaskInvoker.Instance.CallMethodWithDelay(this, Deinitialize, currentData.Duration);

        currentFxHandler = handlersPrefbs.RandomObject();
        currentFxHandler?.Play(transform.position, transform);
    }


    public void Deinitialize()
    {
        TaskInvoker.Instance.UnscheduleAllMethodForTarget(this);
        DOTween.Complete(owner, true);

        collidableObject.OnCustomTriggerEnter2D -= HandleEnterCollision;
        collidableObject.OnCustomTriggerExit2D -= HandleExitCollision;
        owner.CombatController.OnAttack -= CombatController_OnAttack;

        foreach (var fighter in fightersInside)
        {
            DisableSilence(fighter);
            SetFightersCollisionIgnored(owner, fighter, false);
        }

        GoOutsideSmoke();

        currentFxHandler.Stop();
        fightersInside.Clear();
        Destroy(gameObject);
    }

    
    private void HandleEnterCollision(Collider2D collider, CollidableObject.Type type)
    {
        Fighter collidedFighter = collider.GetComponent<Fighter>();

        if (collidedFighter != null)
        {
            if (!collidedFighter.Equals(owner))
            {
                EnableSilence(collidedFighter);
                fightersInside.Add(collidedFighter);
                SetFightersCollisionIgnored(owner, collidedFighter, true);
            }
            else
            {
                GoInsideSmoke();
            }
        }
    }


    private void HandleExitCollision(Collider2D collider, CollidableObject.Type type)
    {
        Fighter collidedFighter = collider.GetComponent<Fighter>();

        if (collidedFighter != null)
        {
            if (!owner.Equals(collidedFighter))
            {
                DisableSilence(collidedFighter);
                fightersInside.Remove(collidedFighter);
                SetFightersCollisionIgnored(owner, collidedFighter, false);
            }
            else
            {
                GoOutsideSmoke();
            }
        }
    }


    private void CombatController_OnAttack(FighterCombatData data)
    {
        TaskInvoker.Instance.UnscheduleMethod(this, GoInsideSmoke);

        GoOutsideSmoke();
        TaskInvoker.Instance.CallMethodWithDelay(this, GoInsideSmoke, currentData.OwnerGraphicDisableDuration);
    }


    private void EnableSilence(Fighter fighter)
    {
        fighter.ControlController.ApplyControl(ControlType.Silence, float.MaxValue, this);
        fighter.AbilityController.AbilityBlokers++;
    }


    private void DisableSilence(Fighter fighter)
    {
        fighter.ControlController.FinishControl(ControlType.Silence, this);
        fighter.AbilityController.AbilityBlokers--;
    }


    private void GoInsideSmoke()
    {
        float alpha = (owner as PlayerFighter == null) ? currentData.alphaFighter : currentData.alphaPlayer;
        owner.AnimationController.SetGraphicsColor(alpha);
        owner.CombatController.DodgeCounter++;
    }


    private void GoOutsideSmoke()
    {
        owner.AnimationController.SetGraphicsColor(1.0f);
        owner.CombatController.DodgeCounter--;
    }


    private void SetFightersCollisionIgnored(Fighter first, Fighter second, bool enable)
    {
        Physics2D.IgnoreCollision(first.MainCollider, second.MainCollider, enable);
        Physics2D.IgnoreCollision(first.KinematicCollider, second.KinematicCollider, enable);

        Physics2D.IgnoreCollision(first.MainCollider, second.KinematicCollider, enable);
        Physics2D.IgnoreCollision(first.KinematicCollider, second.MainCollider, enable);
    }
}
