﻿using Spine.Unity;
using UnityEngine;


[CreateAssetMenu(fileName = "BurstHitPasiveAbility", menuName = "Settings/Ability/Mary/BurstHitPasiveAbility")]
public class BurstHitPasiveAbility : PassiveAbility
{
    [Space]
    [Header("Ability Settings")]
    public Sprite disableSprite = default;

    public float additionalDamage = default;
    public float stunPerHit = default;
    public float coolDown = default;

#pragma warning disable 0414
    //only for reflection
    [SerializeField] private SkeletonDataAsset skeletonDataAsset = default;
#pragma warning restore 0414

    [SpineAnimation(dataField = "skeletonDataAsset")]
    public string abilityAnimation = default;

    private float currentCoolDown = default;
    private bool isCharged;


    public override void Initialize(Fighter fighter)
    {
        base.Initialize(fighter);

        currentCoolDown = coolDown;

        Charge();
        Scheduler.OnUpdate += Scheduler_OnUpdate;

        ownerFighter.CombatController.OnAttack += CombatController_OnAttack;
        ownerFighter.CombatController.OnBeforeHitFighter += CombatController_OnBeforeHitFighter;
        ownerFighter.CombatController.OnHitFighter += CombatController_OnHitFighter;
    }


    public override void Deinitialize()
    {
        Scheduler.OnUpdate -= Scheduler_OnUpdate;
        ownerFighter.CombatController.OnHitFighter -= CombatController_OnHitFighter;
        ownerFighter.CombatController.OnBeforeHitFighter -= CombatController_OnBeforeHitFighter;
        ownerFighter.CombatController.OnAttack -= CombatController_OnAttack;

        base.Deinitialize();
    }


    private void Scheduler_OnUpdate(float deltaTime)
    {
        if (!isCharged)
        {
            currentCoolDown -= Time.deltaTime;

            if (currentCoolDown <= 0.0f)
            {
                Charge();
                currentCoolDown = coolDown;
            }
        }
    }


    private void CombatController_OnAttack(FighterCombatData data)
    {
        currentCoolDown = coolDown;
    }


    private void CombatController_OnBeforeHitFighter(Fighter obj)
    {
        if (ownerFighter.MovementController.IsLoockingInBack(obj) && isCharged)
        {
            ownerFighter.CombatController.AddAdditionalDamage(additionalDamage);
        }
        else
        {
            SetOnCooldown();
        }
    }


    private void CombatController_OnHitFighter(Fighter fighter, float arg2, int arg3)
    {
        if (isCharged)
        {
            ownerFighter.CombatController.RemoveAdditionalDamage(additionalDamage);
            fighter.ControlController.ApplyControl(ControlType.Stun, stunPerHit, this);

            SetOnCooldown();

            ownerFighter.AnimationController.PlayAbilityAnimation(abilityAnimation);
        }
    }


    private void Charge()
    {
        isCharged = true;
        TriggerShouldChangeSpriteEvent(UISprite);
    }


    private void SetOnCooldown()
    {
        currentCoolDown = coolDown;
        isCharged = false;
        TriggerShouldChangeSpriteEvent(disableSprite);
    }
}
