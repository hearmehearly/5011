﻿using UnityEngine;


[CreateAssetMenu(fileName = "TechnologicalWeapon", menuName = "Settings/Ability/Ralph/TechnologicalWeapon")]
public class TechnologicalWeaponAbility : PassiveAbility
{
    [Space]
    [Header("Ability Settings")]

    [Header("Melee Settings")]
    public float SecondToLoosePoint = default;
    public int MaxOverheatingPoints = default;

    public float HeatDamageMultiplier = default;

    public float HeatDuration = default;

    [Header("Heated Data")]
    public ControlValue[] heatedControls = default;
    public float HeatedDuration = default;

    [Header("Range Settings")]
    public Sprite UISpriteRange = default;
    public int MaxChargesCount = default;
    public float ChragesReloadDuration = default;


    private float damageMultipiedDelta;

    private int currentOverheatingPoints;
    private int currentChargePoints;
    private float currentLosePointsTimer;

    private bool isHeat;
    private Coroutine resetHeatRoutine;

    private Coroutine heatedRoutine;

    private AttackTypeSwitchAbility attackSwitchAbility;
    private float currentChragesReloadTimer;



    public int CurrentOverheatingPoints
    {
        get => currentOverheatingPoints;

        set
        {
            if (value < 0 || value > MaxOverheatingPoints || value == currentOverheatingPoints)
            {
                return;
            }

            int prevValue = currentOverheatingPoints;
            currentOverheatingPoints = value;

            bool isAdded = currentOverheatingPoints > prevValue;

            if (isAdded && !isHeat && currentOverheatingPoints == MaxOverheatingPoints)
            {
                if (resetHeatRoutine == null && heatedRoutine == null)
                {
                    isHeat = true;

                    float currentDamageMultiplier = ownerFighter.CombatController.DamageMultiplier;
                    damageMultipiedDelta = currentDamageMultiplier * HeatDamageMultiplier - currentDamageMultiplier;

                    ownerFighter.CombatController.DamageMultiplier += damageMultipiedDelta;

                    ownerFighter.GetComponent<SpriteRenderer>().color = Color.yellow;

                    resetHeatRoutine = Scheduler.PlayMethodWithDelay(ResetHeat, HeatDuration);
                }
            }

            TriggerDisplayInfoEvent(currentOverheatingPoints.ToString());
        }
    }


    public int CurrentChargePoints
    {
        get => currentChargePoints;

        private set
        {
            if (value < 0 || value > MaxChargesCount || value == currentChargePoints)
            {
                return;
            }

            currentChargePoints = value;
            
            if (currentChargePoints == 0)
            {
                ownerFighter.ControlController.ApplyControl(ControlType.Disarm, float.MaxValue, this);
            }
            else
            {
                ownerFighter.ControlController.FinishControl(ControlType.Disarm, this);
            }

            TriggerDisplayInfoEvent(currentChargePoints.ToString());
        }
    }

    
    public override void Initialize(Fighter fighter)
    {
        base.Initialize(fighter);

        foreach (var ability in ownerFighter.AbilityController.currentAbilities)
        {
            if (ability is ActiveAbility activeAbility)
            {
                activeAbility.OnAbilityUsed += OnAbilityUsed;
            }
        }

        attackSwitchAbility = ownerFighter.AbilityController.currentAbilities.Find(element => element.AbilityName == Name.Adaptation) as AttackTypeSwitchAbility;

        ownerFighter.HealthController.OnApplyHit += OnApplyHit;
        ownerFighter.CombatController.OnAttack += OnAttack;
        ownerFighter.CombatController.OnHitFighter += OnHitFighter;
        Scheduler.OnUpdate += OnUpdate;

        TriggerDisplayInfoEvent(currentOverheatingPoints.ToString());
    }


    public override void Deinitialize()
    {
        base.Deinitialize();

        Scheduler.StopPlayingCoroutine(ref resetHeatRoutine);
        Scheduler.StopPlayingCoroutine(ref heatedRoutine);

        foreach (var ability in ownerFighter.AbilityController.currentAbilities)
        {
            if (ability is ActiveAbility activeAbility)
            {
                activeAbility.OnAbilityUsed -= OnAbilityUsed;
            }
        }

        ownerFighter.HealthController.OnApplyHit -= OnApplyHit;
        ownerFighter.CombatController.OnAttack -= OnAttack;
        Scheduler.OnUpdate -= OnUpdate;
    }


    public void IncrementCurrentChargePoints()
    {
        CurrentChargePoints++;
    }


    public void DecrementCurrentChargePoints()
    {
        CurrentChargePoints--;
    }


    private void OnAbilityUsed(Ability ability)
    {
        TryMarkHeater();

        if (ability is AttackTypeSwitchAbility attackTypeSwitchAbility)
        {
            if (ownerFighter.CombatController.IsRange)
            {
                CurrentOverheatingPoints = 0;

                if (isHeat)
                {
                    ResetHeat();
                }

                CurrentChargePoints = CurrentChargePoints;

                TriggerDisplayInfoEvent(currentChargePoints.ToString());
                TriggerShouldChangeSpriteEvent(UISpriteRange);
            }
            else if (ownerFighter.CombatController.IsMelee)
            {
                ownerFighter.ControlController.FinishControl(ControlType.Disarm, this);

                CurrentOverheatingPoints = CurrentOverheatingPoints;

                TriggerDisplayInfoEvent(currentOverheatingPoints.ToString());
                TriggerShouldChangeSpriteEvent(UISprite);
            }
        }
        else if (ownerFighter.CombatController.IsMelee)
        {
            CurrentOverheatingPoints++;
        }
    }


    private void OnApplyHit(float damage)
    {
        currentChragesReloadTimer = default;
    }


    private void OnAttack(FighterCombatData data)
    {
        TryMarkHeater();

        if (ownerFighter.CombatController.IsRange)
        {
            DecrementCurrentChargePoints();
            currentChragesReloadTimer = default;
        }
    }


    private void OnHitFighter(Fighter damagedFighter, float damage, int combo)
    {
        if (ownerFighter.CombatController.IsMelee)
        {
            CurrentOverheatingPoints++;
        }
    }


    // attack or ability used. Bad naming
    private void TryMarkHeater()
    {
        if (isHeat)
        {
            ResetHeat();
            MarkHeated();
        }
    }


    private void ResetHeat()
    {
        ownerFighter.GetComponent<SpriteRenderer>().color = Color.white;
        ownerFighter.CombatController.DamageMultiplier -= damageMultipiedDelta;

        Scheduler.StopPlayingCoroutine(ref resetHeatRoutine);
        isHeat = false;
    }


    private void MarkHeated()
    {
        if (heatedRoutine == null)
        {
            ownerFighter.GetComponent<SpriteRenderer>().color = Color.red;

            foreach (var control in heatedControls)
            {
                ownerFighter.ControlController.ApplyControl(control, this);
            }

            heatedRoutine = Scheduler.PlayMethodWithDelay(() =>
            {
                heatedRoutine = null;

                ownerFighter.GetComponent<SpriteRenderer>().color = Color.white;
            }, HeatedDuration);
        }
    }


    private void OnUpdate(float deltaTime)
    {
        if (ownerFighter.CombatController.IsMelee)
        {
            if (currentLosePointsTimer > SecondToLoosePoint)
            {
                CurrentOverheatingPoints--;
                currentLosePointsTimer = default;
            }
            else if (CurrentOverheatingPoints > 0)
            {
                currentLosePointsTimer += deltaTime;
            }
        }
        else if (ownerFighter.CombatController.IsRange)
        {
            if (currentChragesReloadTimer > ChragesReloadDuration)
            {
                CurrentChargePoints = MaxChargesCount;
                currentChragesReloadTimer = default;
            }
            else if (CurrentChargePoints < MaxChargesCount)
            {
                currentChragesReloadTimer += deltaTime;
            }
        }
    }
}
