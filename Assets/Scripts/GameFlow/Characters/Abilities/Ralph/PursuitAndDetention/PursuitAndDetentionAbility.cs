﻿using UnityEngine;


[CreateAssetMenu(fileName = " PursuitAndDetention ", menuName = "Settings/Ability/Ralph/PursuitAndDetention")]
public class PursuitAndDetentionAbility : OnceResponceAbility
{
    [Space]
    [Header("Melee Settings")]
    public Vector2 chargeForce = default;
    public AbilityHitZone.Data MeleeCombatTypeData = default;
    public int OverheatingPointsPerUse = default;

    public AbilityHitZone hitZonePrefab = default;

    [Space]
    [Header("Range Settings")]

    public FighterAbilityProjectile projectilePrefab = default;
    public FighterAbilityProjectile.Data projectileData = default;

    private TechnologicalWeaponAbility technologicalAbility;



    public override bool AllowUse => ownerFighter.CombatController.IsRange ?
                                  technologicalAbility.CurrentChargePoints > 0 : true;



    public override void Initialize(Fighter fighter)
    {
        base.Initialize(fighter);

        technologicalAbility = ownerFighter.AbilityController.currentAbilities.Find(element => element.AbilityName == Name.TechnologicalWeapon) as TechnologicalWeaponAbility;
    }


    public override void Deinitialize()
    {
        base.Deinitialize();

        ownerFighter.MovementController.OnGroundedOnLegs -= MovementController_OnGrounded;
    }


    public override void UseAbility()
    {
        if (ownerFighter.CombatController.IsMelee)
        {
            MeleeTypeAbilityUse();
        }
        else if (ownerFighter.CombatController.IsRange && technologicalAbility.CurrentChargePoints > 0)
        {
            RangeTypeAbilityUse();
        }

        base.UseAbility();
    }


    private void MeleeTypeAbilityUse()
    {
        ownerFighter.MovementController.ChargeBlockers++;
        ownerFighter.MovementController.MovementBlokers++;
        ownerFighter.MovementController.JumpBlokers++;

        ownerFighter.MovementController.AddRelativeToSideForce(chargeForce);
        ownerFighter.MovementController.OnGroundedOnLegs += MovementController_OnGrounded;
    }


    private void MovementController_OnGrounded()
    {
        ownerFighter.MovementController.ChargeBlockers--;
        ownerFighter.MovementController.MovementBlokers--;
        ownerFighter.MovementController.JumpBlokers--;

        ownerFighter.MovementController.Stop();
        CreateAfterChargeHitArea();

        ownerFighter.MovementController.OnGroundedOnLegs -= MovementController_OnGrounded;
    }


    private void CreateAfterChargeHitArea()
    {
        AbilityHitZone hitZone = Instantiate(hitZonePrefab,
                                             ownerFighter.transform.position,
                                             Quaternion.identity,
                                             ownerFighter.transform);

        hitZone.Initialize(ownerFighter, MeleeCombatTypeData);
        hitZone.AddOnFighterEnterAreaCallback(IncrementOverheatingPoints);

        for (int i = 0; i < OverheatingPointsPerUse; i++)
        {
            //technologicalAbility.CurrentOverheatingPoints++;
        }
        
        Physics2D.IgnoreCollision(ownerFighter.MainCollider, hitZone.HitCollider);
    }


    private void RangeTypeAbilityUse()
    {
        FighterAbilityProjectile hitZone = Instantiate(projectilePrefab,
                                                       ownerFighter.CombatController.HitRoot.position,
                                                       Quaternion.identity);

        hitZone.Initialize(projectileData, ownerFighter);

        Physics2D.IgnoreCollision(ownerFighter.MainCollider, hitZone.HitCollider);

        technologicalAbility.DecrementCurrentChargePoints();
    }
    

    private void IncrementOverheatingPoints(Fighter fighter)
    {
        technologicalAbility.CurrentOverheatingPoints++;
    }
}