﻿using UnityEngine;


[CreateAssetMenu(fileName = " ChoppingOffLimbs ", menuName = "Settings/Ability/Ralph/ChoppingOffLimbs")]
public class ChoppingOffLimbsAbility : OnceResponceAbility
{
    [Space]
    [Header("Melee Settings")]
    public AbilityHitZone.Data MeleeCombatTypeData = default;
    public int OverheatingPointsPerUse = default;

    public AbilityHitZone hitZonePrefab = default;

    [Space]
    [Header("Range Settings")]

    public IndestructibleProjectile indestructibleProjectilePrefab = default;
    public IndestructibleProjectile.Data indestructibleProjectileData = default;

    public float DamageMultiplierPerOverheatingPoint = default;

    private TechnologicalWeaponAbility technologicalAbility;
    private AbilityHitZone hitZone;

    public float CurrentDamageMultiplier { get; set; } = 1.0f;


    public override bool AllowUse => ownerFighter.CombatController.IsRange ?
                                     technologicalAbility.CurrentChargePoints > 0 : true;



    public override void Initialize(Fighter fighter)
    {
        base.Initialize(fighter);

        technologicalAbility = ownerFighter.AbilityController.currentAbilities.Find(element => element.AbilityName == Name.TechnologicalWeapon) as TechnologicalWeaponAbility;
    }


    public override void Deinitialize()
    {
        if (hitZone != null)
        {
            hitZone.OnShouldHitFighter -= HitZone_OnShouldHitFighter;
        }

        base.Deinitialize();
    }


    public override void UseAbility()
    {
        base.UseAbility();

        if (ownerFighter.CombatController.IsMelee)
        {
            MeleeTypeAbilityUse();
        }
        else if (ownerFighter.CombatController.IsRange)
        {
            RangeTypeAbilityUse();
        }
    }


    private void MeleeTypeAbilityUse()
    {
        hitZone = Instantiate(hitZonePrefab,
                              ownerFighter.CombatController.HitRoot.position,
                              Quaternion.identity,
                              ownerFighter.CombatController.HitRoot);

        hitZone.OnShouldHitFighter += HitZone_OnShouldHitFighter;
        hitZone.Initialize(ownerFighter, MeleeCombatTypeData, () =>
        {
            hitZone.OnShouldHitFighter -= HitZone_OnShouldHitFighter;
        });

        Physics2D.IgnoreCollision(ownerFighter.MainCollider, hitZone.HitCollider);
    }

    private void HitZone_OnShouldHitFighter(Fighter obj)
    {
        hitZone.OnShouldHitFighter -= HitZone_OnShouldHitFighter;

        AddPointPerMeleeHit();
    }

    private void RangeTypeAbilityUse()
    {
        int currentCharges = technologicalAbility.CurrentChargePoints;

        for (int i = 0; i < currentCharges; i++)
        {
            IndestructibleProjectile hitZone = Instantiate(indestructibleProjectilePrefab,
                                                           ownerFighter.CombatController.HitRoot.position,
                                                           Quaternion.identity);

            hitZone.Initialize(indestructibleProjectileData, ownerFighter);

            Physics2D.IgnoreCollision(ownerFighter.MainCollider, hitZone.HitCollider);

            technologicalAbility.DecrementCurrentChargePoints();
        }
    }


    private void AddPointPerMeleeHit()
    {
        for (int i = 0; i < OverheatingPointsPerUse; i++)
        {
            technologicalAbility.CurrentOverheatingPoints++;
        }
    }
}
