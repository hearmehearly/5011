﻿using Fighter5011.Ui;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "AttackTypeSwitch ", menuName = "Settings/Ability/AttackTypeSwitch")]
public class AttackTypeSwitchAbility : OnceResponceAbility
{
    [SerializeField]
    private List<Name> abilitiesToResetOnUse = default;
    [Space]
    [SerializeField]
    private DataDamageValue[] meleeDamageValues = default;
    [Space]
    [Space]
    [SerializeField]
    private DataDamageValue[] rangeDamageValues = default;
    [Space]
    [Header("Range Switch")]

    [SerializeField]
    private int chargesToAddOnRangeSwitch = default;
    [SerializeField]
    private float speedMultiplierOnRangeSwitch = default;
    [SerializeField]
    private float speedMultipliedDurationOnRangeSwitch = default;

    [Header("Melee Switch")]
    [SerializeField]
    private PercentDamageValue damageValueOnMeleeSwitch = default;
    [SerializeField]
    private float damagePercentBuffDuration = default;

    private bool isUnderAdditionalSpeed;
    private TechnologicalWeaponAbility technologicalWeaponAbility;
    private float lastAdditionalSpeed;


    public override void Initialize(Fighter fighter)
    {
        base.Initialize(fighter);

        technologicalWeaponAbility = ownerFighter.AbilityController.currentAbilities.Find(element => element.AbilityName == Name.TechnologicalWeapon) as TechnologicalWeaponAbility;

        SetupData();
    }


    public override void Deinitialize()
    {
        TaskInvoker.Instance.UnpauseAllMethodForTarget(this);

        if (isUnderAdditionalSpeed)
        {
            ResetAdditionalSpeed();
        }

        base.Deinitialize();
    }

    public override void UseAbility()
    {
        if (ownerFighter.CombatController.IsMelee)
        {
            ownerFighter.CombatController.CombatType = FighterCombatData.Type.Range;

            for (int i = 0; i < chargesToAddOnRangeSwitch; i++)
            {
                technologicalWeaponAbility.IncrementCurrentChargePoints();
            }

            SetAdditionalSpeed();
            TaskInvoker.Instance.CallMethodWithDelay(this, ResetAdditionalSpeed, speedMultipliedDurationOnRangeSwitch);
        }
        else if (ownerFighter.CombatController.IsRange)
        {
            ownerFighter.CombatController.CombatType = FighterCombatData.Type.Melee;

            EnableStremlenie();
            TaskInvoker.Instance.CallMethodWithDelay(this, DisableStremlenie, damagePercentBuffDuration);
        }


        SetupData();

        if (ownerFighter is PlayerFighter playerFighter)
        {
            UILevel uiLevel = UiScreenManager.Instance.LoadedScreen<UILevel>(Fighter5011.ScreenType.Ingame);
            uiLevel.InitializeAttackButton(ownerFighter.CombatController.CombatType);
        }

        foreach (var ability in ownerFighter.AbilityController.currentAbilities)
        {
            if (ability is ActiveAbility activeAbility && abilitiesToResetOnUse.Contains(activeAbility.AbilityName))
            {
                activeAbility.FastResetCoolDown();
            }
        }

        base.UseAbility();
    }


    void SetAdditionalSpeed()
    {
        lastAdditionalSpeed = ownerFighter.MovementController.SpeedFactor * speedMultiplierOnRangeSwitch;
        ownerFighter.MovementController.SpeedFactor += lastAdditionalSpeed;

        isUnderAdditionalSpeed = true;
    }


    void ResetAdditionalSpeed()
    {
        ownerFighter.MovementController.SpeedFactor -= lastAdditionalSpeed;

        isUnderAdditionalSpeed = false;
    }


    private void EnableStremlenie()
    {
        ownerFighter.AnimationController.SetSkeletonColor(Color.green);
        ownerFighter.CombatController.OnHitFighter += OnHitUnderStremlenie;
    }

    private void OnHitUnderStremlenie(Fighter fighter, float damage, int arg2)
    {
        //very bad solution
        fighter.HealthController.AddHealth(damage);
        float percentDamage = fighter.HealthController.CurrentHealth * damageValueOnMeleeSwitch.PercentFromHealth;

        fighter.ApplyHit(null,
                        percentDamage, 
                        damageValueOnMeleeSwitch.DamageType,
                        damageValueOnMeleeSwitch.HitImpuls * ownerFighter.transform.lossyScale.normalized,
                        damageValueOnMeleeSwitch.StunDurationPerHit,
                        damageValueOnMeleeSwitch.ShouldKnockOver,
                        ownerFighter);

        DisableStremlenie();
    }


    private void DisableStremlenie()
    {
        ownerFighter.AnimationController.SetSkeletonColor(Color.white);
        ownerFighter.CombatController.OnHitFighter -= OnHitUnderStremlenie;
    }


    private void SetupData()
    {
        if (ownerFighter.CombatController.IsMelee)
        {
            ownerFighter.CombatController.SetNewDamageValues(meleeDamageValues);
        }
        else if (ownerFighter.CombatController.IsRange)
        {
            ownerFighter.CombatController.SetNewDamageValues(rangeDamageValues);
        }
        else
        {
            Debug.LogWarning("Unexpected combat type for fighter " + ownerFighter);
        }
    }
}
