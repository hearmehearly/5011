﻿using System;
using UnityEngine;
using Fighter5011.Ui;


public partial class PlayerFighter : Fighter
{
    [SerializeField]
    private PlayerCameraController cameraController = default;


    private UILevel savedUiLevel;

    private Action onWinCallback;
    private static Vector2 offset;

    public static Vector2 Offset => offset;

    public override Side LookingSide => movementController.LookingSide;


    public PlayerCameraController PlayerCameraController => cameraController;

    
    public override void Initialize(Name name)
    {
        base.Initialize(name);

        cameraController.Initialize(this);

        savedUiLevel = UiScreenManager.Instance.LoadedScreen<UILevel>(Fighter5011.ScreenType.Ingame);

        savedUiLevel.InitializeAttackButton(CombatController.CombatType);
        savedUiLevel.InitializeAbilityButtons(AbilityController.currentAbilities);

        savedUiLevel.SetHealthInfo(healthController.CurrentHealth, healthController.MaxHealth);
    }


    public void SetupCamera(Camera camera) =>
        cameraController.SetupCamera(camera);
    

    public override void Deinitialize()
    {
        base.Deinitialize();

        cameraController.Deinitialize();
    }


    public override void StartLevel()
    {
        base.StartLevel();

        SubscribeOnMovementEvents();
        SubscribeOnAttackingEvents();

        Scheduler.OnFixedUpdate += UpdateArenaOffset;

        healthController.OnHealthChanged += OnHealthChanged;
    }


    public override void FinishLevel()
    {
        base.FinishLevel();

        UnsubscribeFromMovementEvents();
        UnsubscribeFromAttackingEvents();

        Scheduler.OnFixedUpdate -= UpdateArenaOffset;
        
        UILevel uiLevel = UiScreenManager.Instance.LoadedScreen<UILevel>(Fighter5011.ScreenType.Ingame);
        uiLevel.AttackingController.Deinitialize();
        uiLevel.AbilitiesController.Deinitialize();

        healthController.OnHealthChanged -= OnHealthChanged;
    }


    public void WinLevel()
    {
        FinishLevel();
        onWinCallback?.Invoke();
        onWinCallback = null;
    }


    public void SetOnWinCallback(Action callback)
    {
        onWinCallback = callback;
    }


    protected override void OnFighterAttack(FighterCombatData data)
    {
        base.OnFighterAttack(data);
    }


    public void SubscribeOnMovementEvents()
    {
        UILevel uiLevel = UiScreenManager.Instance.LoadedScreen<UILevel>(Fighter5011.ScreenType.Ingame);
        uiLevel.MovementController.OnShouldMoveLeft += MoveLeft;
        uiLevel.MovementController.OnShouldMoveRight += MoveRight;
        uiLevel.MovementController.OnShouldStop += Stop;

        uiLevel.MovementController.OnShouldJump += Jump;
        uiLevel.MovementController.OnShouldCharge += Charge;
        uiLevel.MovementController.OnShouldSeatDown += SeatDown;
        uiLevel.MovementController.OnShouldStandUp += StandUp;
    }


    public void UnsubscribeFromMovementEvents()
    {
        UILevel uiLevel = UiScreenManager.Instance.LoadedScreen<UILevel>(Fighter5011.ScreenType.Ingame);
        uiLevel.MovementController.OnShouldMoveLeft -= MoveLeft;
        uiLevel.MovementController.OnShouldMoveRight -= MoveRight;
        uiLevel.MovementController.OnShouldStop -= Stop;

        uiLevel.MovementController.OnShouldJump -= Jump;
        uiLevel.MovementController.OnShouldCharge -= Charge;
        uiLevel.MovementController.OnShouldSeatDown -= SeatDown;
        uiLevel.MovementController.OnShouldStandUp -= StandUp;
    }


    private void SubscribeOnAttackingEvents()
    {
        UILevel uiLevel = UiScreenManager.Instance.LoadedScreen<UILevel>(Fighter5011.ScreenType.Ingame);
        uiLevel.AttackingController.OnShouldAttack += Attack;
        uiLevel.AbilitiesController.OnPressAbility += OnPressAbility;
        uiLevel.AbilitiesController.OnPressUpAbility += OnPressUpAbility;
    }


    private void UnsubscribeFromAttackingEvents()
    {
        UILevel uiLevel = UiScreenManager.Instance.LoadedScreen<UILevel>(Fighter5011.ScreenType.Ingame);
        uiLevel.AttackingController.OnShouldAttack -= Attack;
        uiLevel.AbilitiesController.OnPressAbility -= OnPressAbility;
        uiLevel.AbilitiesController.OnPressUpAbility -= OnPressUpAbility;
    }
        

    private void UpdateArenaOffset(float fixedDeltaTime)
    {
        offset.x = cameraController.CameraLocalPosition.x / Arena.Border;
        offset.y = cameraController.CameraLocalPosition.y / Arena.TopMaxBorder;
    }


    private void OnHealthChanged()
    {
        savedUiLevel.SetHealthInfo(healthController.CurrentHealth, healthController.MaxHealth);
    }


    //debug
    private void Update()
    {
        // abilities MAIN FIGHTER
        if (Input.GetKeyDown(KeyCode.Q))
        {
            AbilityController.OnPressAbility(AbilityController.currentAbilities[0]);
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            AbilityController.OnPressAbility(AbilityController.currentAbilities[1]);
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            AbilityController.OnPressAbility(AbilityController.currentAbilities[2]);
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            AbilityController.OnPressAbility(AbilityController.currentAbilities[3]);
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            CombatController.Attack();
        }

        // movement
        if (Input.GetKeyDown(KeyCode.A))
        {
            MovementController.MoveLeft();
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            MovementController.MoveRight();
        }
        else if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
        {
            MovementController.Stop();
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            MovementController.Jump();
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            MovementController.SeatDown();
        }
        else if (Input.GetKeyUp(KeyCode.S))
        {
            MovementController.StandUp();
        }
        else if (Input.GetKeyDown(KeyCode.G) || Input.GetKeyDown(KeyCode.H))
        {
            MovementController.Charge();
        }
        else if (Input.GetKeyDown(KeyCode.K))
        {
            var fx = PhysicsManager.Instance.OverlapFightersInRadius(transform.position, 100, this);
            foreach(var i in fx)
            i.MovementController.KnockOver(transform.position);

        }
        else if (Input.GetKeyDown(KeyCode.L))
        {
            MainCollider.sharedMaterial.bounciness -= 0.3f;
            MainCollider.enabled = false;
            MainCollider.enabled = true;
        }
    }
}
