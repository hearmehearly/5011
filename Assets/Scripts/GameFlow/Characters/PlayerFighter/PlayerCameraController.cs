﻿using DG.Tweening;
using System.Collections;
using UnityEngine;


public class PlayerCameraController : MonoBehaviour
{
    private const float cameraDefaultSize = 5.0f;

    [SerializeField] private Transform target = default;
    [SerializeField] private FighterCameraData data = default;

    private Vector3 velocity = default;
    
    private Coroutine cameraScaleRoutine;
    private Camera gameCamera;
    private Fighter fighter;

    private float defaultSize;
    private float lastCameraSize;
    private Vector3 targetPosition;
    private FighterCameraData instanceData;


    public Vector3 CameraLocalPosition => gameCamera.transform.localPosition;


    public void SetupCamera(Camera camera)
    {
        gameCamera = camera;
        gameCamera.orthographicSize = cameraDefaultSize;
        defaultSize = gameCamera.orthographicSize;
        lastCameraSize = defaultSize;

        gameCamera.transform.position = fighter.transform.position;
    }


    public void Initialize(Fighter _fighter)
    {
        fighter = _fighter;
        instanceData = Instantiate(data);

        fighter.MovementController.OnJump += MovementController_OnJump;

        DOTween.Complete(this);
        Scheduler.StopPlayingCoroutine(ref cameraScaleRoutine);
    }


    public void Deinitialize()
    {
        fighter.MovementController.OnJump -= MovementController_OnJump;
    }


    private void FixedUpdate()
    {
        if (gameCamera != null)
        {
            targetPosition = target.TransformPoint(default);

            float velocityOffset = Mathf.Abs(fighter.MovementController.Velocity.x) * instanceData.targetPositionXCoefficient;

            velocityOffset = velocityOffset > instanceData.maxOffset ? instanceData.maxOffset : velocityOffset;

            targetPosition.x = fighter.MovementController.Velocity.x < 0 ? targetPosition.x - velocityOffset : targetPosition.x + velocityOffset;
            
            // Smoothly move the camera towards that target position
            float duration = (Mathf.Abs(fighter.MovementController.Velocity.y) > 1.0f &&
                              Mathf.Abs(fighter.MovementController.Velocity.x) < 1.0f)
                ? instanceData.smoothTimeInJump : instanceData.smoothTime;
            gameCamera.transform.position = Vector3.SmoothDamp(gameCamera.transform.position, targetPosition, ref velocity, duration);
        }
    }


    private void MovementController_OnJump()
    {
        ScaleCameraTween(gameCamera.orthographicSize + instanceData.additionalSizeOnJump, instanceData.scaleDeltaIncreateDuration);

        if (cameraScaleRoutine == null)
        {
            cameraScaleRoutine = StartCoroutine(CameraScale());
        }
    }


    private IEnumerator CameraScale()
    {
        while (fighter.MovementController.Velocity.y > 0)
        {
            yield return new WaitForFixedUpdate();
        }
        
        ScaleCameraTween(defaultSize, instanceData.scaleDeltaDecreaseDuration); 
        cameraScaleRoutine = null;
    }


    private void ScaleCameraTween(float targetSize, float duration)
    {
        float localLastCameraSize = lastCameraSize;

        DOTween.Kill(gameCamera, true);

        DOTween
            .To(() => localLastCameraSize, (x) =>
            {
                gameCamera.orthographicSize = x;
                lastCameraSize = x;
            }, targetSize, duration)
            .SetTarget(gameCamera);
    }


    public void MultiplySmoothTime(float factor)
    {
        instanceData.smoothTime *= factor;
        instanceData.smoothTimeInJump *= factor;
    }

    
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(targetPosition, 0.5f);
    }
}
