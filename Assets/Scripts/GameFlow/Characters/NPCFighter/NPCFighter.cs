﻿using UnityEngine;


public class NPCFighter : Fighter
{
    [SerializeField] private NPCData data = null;
    
    [SerializeField] private NPCSettings AISettings = default;

    Transform playerFoundTransform;
    //float attackTimer = 0.0f;
    // costil



    public override Side LookingSide => movementController.LookingSide;


    public NPCFighterAI FighterAI { get; private set; }
    public NPCFighterSolutions FighterSolutions { get; private set; }


    public NPCFighterAIAbilities NPCFighterAIAbilities { get; private set; }

    public NPCFighterTriggers FighterTriggers { get; private set; }

    public Fighter Fighter
    {
        get => default;
        set
        {
        }
    }


    //// for test
    //private void Update()
    //{
    //    attackTimer += Time.deltaTime;

    //    if (attackTimer > data.AttackCooldown)
    //    {
    //        Attack();
    //        attackTimer = 0.0f;
    //    }
    //}


    //void FixedUpdate()
    //{
    //    if (!data.IsPassive)
    //    {
    //        if (playerFoundTransform == null)
    //        {
    //            RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, transform.right * transform.localScale.x);

    //            foreach (var hit in hits)
    //            {
    //                CollidableObject collidableObject = hit.collider.GetComponent<CollidableObject>();

    //                if (collidableObject != null && collidableObject.CurrentType == CollidableObject.Type.Player)
    //                {
    //                    playerFoundTransform = collidableObject.transform;
    //                }
    //            }
    //        }
    //        else
    //        {
    //            float distance = Vector3.Distance(transform.position, playerFoundTransform.position);

    //            if (distance > data.DistanceToStop)
    //            {
    //                movementController.MoveTo(playerFoundTransform.position);
    //            }
    //        }
    //    }
    //}


    public override void Deinitialize()
    {
        FighterTriggers?.Deinitialize();
        FighterAI?.Deinitialize();
        NPCFighterAIAbilities?.Deinitialize();

        base.Deinitialize();
    }


    public void InitializeAI()
    {
        FighterSolutions = new NPCFighterSolutions(this);

        FighterTriggers = new NPCFighterTriggers();
        FighterTriggers.Initialize(this);

        FighterAI = new NPCFighterAI(this, FighterSolutions, FighterTriggers, AISettings);

        NPCFighterAIAbilities = new NPCFighterAIAbilities(this);

       // NPCFighterAIAbilities = new NPCFighterAI();
    }


    public void SetAIEnabled(bool enable)
    {
        if (enable)
        {
            FighterAI.Initialize();
            NPCFighterAIAbilities.Initialize();
        }
        else
        {
            FighterAI.Deinitialize();
            NPCFighterAIAbilities.Deinitialize();
        }
    }
    


    public override void Die()
    {
        FinishLevel();

        base.Die();
    }
    

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red.SetA(0.5f);

        foreach (var space in AISettings.doubleJumpSpaces)
        {
            foreach (var rect in space.positions)
            {
                Gizmos.DrawCube(rect.center, rect.size);
            }
        }
        
        Gizmos.color = Color.blue.SetA(0.5f);

        foreach (var space in AISettings.avoidJumpSpaces)
        {
            foreach (var rect in space.positions)
            {
                Gizmos.DrawCube(rect.center, rect.size);
            }
        }
    }
}
