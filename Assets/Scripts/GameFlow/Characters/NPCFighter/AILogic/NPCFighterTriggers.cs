﻿using UnityEngine;


public class NPCFighterTriggers
{
    public class Values
    {
        public float healthValue;
        public float distanceToPlayer;
        public bool loockingAtPlayer;
        public bool wallDetected;
        public float distanceToWall;
        public float distanceToPlayerY;
        public bool enoughRangeForAttackPlayer;
        
        public float distanceToAnotherFighter;
        public float distanceToAnotherFighterY;
        public bool enoughRangeForAttackAnotherFighter;

        public float distanceToBackAnotherFighter = default;

        public Values()
        {
            distanceToPlayer = float.MaxValue;
            loockingAtPlayer = false;
            wallDetected = false;
            distanceToPlayer = float.MaxValue;
            distanceToPlayerY = float.MaxValue;
        }
    }

    private Fighter currentFighter;



    public Values Triggers { get; private set; }
    public NPCState State { get; private set; }



    public void Initialize(Fighter fighter)
    {
        Triggers = new Values();
        currentFighter = fighter;
        Collect();

        isLockedStateChange = false;
    }


    public void Deinitialize()
    {
        TaskInvoker.Instance.UnscheduleAllMethodForTarget(this);
    }


    public void Collect()
    {
        TaskInvoker.Instance.UnscheduleMethod(this, Collect);
        
        Triggers = new Values();
        Triggers.healthValue = currentFighter.HealthController.CurrentHealth / currentFighter.HealthController.MaxHealth;

        var nearFighters = PhysicsManager.Instance.RaycastFightersInFaceArea(5f, currentFighter);

#warning improove logic
        var player = nearFighters.Find(element => (element as Fighter != null));
        var anotherFighter = nearFighters.Find(element => (element as Fighter != null));

        Triggers.loockingAtPlayer = player != null;

        if (player != null)
        {
            Triggers.distanceToPlayer = Vector3.Distance(player.transform.position, currentFighter.transform.position);
            Triggers.enoughRangeForAttackPlayer = Triggers.distanceToPlayer < currentFighter.CombatController.DistanceForHit;
            Triggers.distanceToPlayerY = (player.transform.position.y - currentFighter.transform.position.y);
        }

        if (anotherFighter != null)
        {
            Triggers.distanceToAnotherFighter = Vector3.Distance(anotherFighter.transform.position, currentFighter.transform.position);
            Triggers.enoughRangeForAttackAnotherFighter = Triggers.distanceToPlayer < currentFighter.CombatController.DistanceForHit;
            Triggers.distanceToAnotherFighterY = (anotherFighter.transform.position.y - currentFighter.transform.position.y);
        }

        RaycastHit2D[] faceHits = Physics2D.RaycastAll(currentFighter.transform.position, currentFighter.transform.right * currentFighter.MovementController.VelocityMultiplier);
        Triggers.wallDetected = false;

        foreach (var hit in faceHits)
        {
            var collidableObject = hit.transform.GetComponent<CollidableObject>();
            if (collidableObject != null && collidableObject.CurrentType == CollidableObject.Type.GroundWall)
            {
                Triggers.wallDetected = true;
                Triggers.distanceToWall = Vector2.Distance(hit.point, currentFighter.transform.position);
                break;
            }
        }

        RaycastHit2D[] backHits = Physics2D.RaycastAll(currentFighter.transform.position, currentFighter.MovementController.SideVector);

        foreach (var hit in backHits)
        {
            var collidableObject = hit.transform.GetComponent<CollidableObject>();
            if (collidableObject != null && collidableObject.CurrentType == CollidableObject.Type.GroundWall)
            {
                Triggers.distanceToBackAnotherFighter = Vector2.Distance(hit.point, currentFighter.transform.position);
                break;
            }
        }
        
        //Debug.Log("healthValue " + Triggers.healthValue);
        //Debug.Log("distanceToPlayer " + Triggers.distanceToPlayer);

        TaskInvoker.Instance.CallMethodWithDelay(this, Collect, 0.2f);
    }


    private bool isLockedStateChange;

    public void RefreshState(NPCSettings settings)
    {
        if (isLockedStateChange)
        {
            return;
        }

        if (Triggers.healthValue > settings.minHealthRateToAgressiveState)
        {
            State = NPCState.Agressive;
        }
        else
        {
            State = NPCState.Avoid;
        }

        if (currentFighter.CombatController.IsRange &&
            (Triggers.distanceToPlayer < settings.minDistanceForRangersToAvoid || 
            Triggers.distanceToAnotherFighter < settings.minDistanceForRangersToAvoid))
        {
            State = NPCState.Avoid;
        }
    }


    public void TryChangeState(NPCSettings settings)
    {
        if (isLockedStateChange)
        {
            return;
        }

        isLockedStateChange = true;
        TaskInvoker.Instance.CallMethodWithDelay(this, () => isLockedStateChange = false, settings.agressiveStateDurationOnSwitch);
        State = NPCState.Agressive;
    }
}
