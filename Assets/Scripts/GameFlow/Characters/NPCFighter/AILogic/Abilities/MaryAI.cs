﻿using System.Collections.Generic;


public class MaryAI : FighterAbilityAI
{
    protected override Ability.Name[] Abilities => new Ability.Name[] {
        Ability.Name.ThrowKnife,
        Ability.Name.SeriesOfMurders,
        Ability.Name.SmokeBomb,
        Ability.Name.BurstHitPasive
};


    public MaryAI(Fighter _fighter) : base(_fighter)
    {
    }

    public override Ability.Name GetOptimalAbility()
    {
        List<Ability.Name> abilitiesPrioritet = new List<Ability.Name>(4);
        abilitiesPrioritet.Add(Ability.Name.SeriesOfMurders);
        abilitiesPrioritet.Add(Ability.Name.SmokeBomb);
        abilitiesPrioritet.Add(Ability.Name.ThrowKnife);

        Ability.Name result = SelectPriorityAbility(abilitiesPrioritet);
        return result;
    }
}
