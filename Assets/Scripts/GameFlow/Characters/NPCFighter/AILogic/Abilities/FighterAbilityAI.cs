﻿using System.Collections.Generic;
using System.Linq;


public abstract class FighterAbilityAI
{
    protected abstract Ability.Name[] Abilities { get; }

    protected readonly Fighter fighter;

    public FighterAbilityAI(Fighter _fighter)
    {
        fighter = _fighter;
    }


    public abstract Ability.Name GetOptimalAbility();

    protected Ability.Name SelectPriorityAbility(List<Ability.Name> abilitiesPrioritet)
    {

        abilitiesPrioritet = abilitiesPrioritet
                                    .Where(e => fighter.AbilityController.GetAbility(e) is ActiveAbility activeAbility && activeAbility.isCoolDownExpired)
                                    .ToList();

        return abilitiesPrioritet.FirstOrDefault();
    }
}
