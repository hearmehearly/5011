﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WitchAI : FighterAbilityAI
{
    protected override Ability.Name[] Abilities => new Ability.Name[] {
        Ability.Name.BatsSwarm,
        Ability.Name.BloodCurse,
        Ability.Name.AwakeningPuppets,
        Ability.Name.DarkAura
};

    public WitchAI(Fighter _fighter) : base(_fighter)
    {
    }


    public override Ability.Name GetOptimalAbility()
    {
        List<Ability.Name> abilitiesPrioritet = new List<Ability.Name>();

        var fightersAround = PhysicsManager.Instance.OverlapFightersInRadius(fighter.transform.position, 3f, fighter);
        bool isAnyFighterNear = !fightersAround.IsNullOrEmpty();

        if (isAnyFighterNear)
        {
            abilitiesPrioritet.Add(Ability.Name.DarkAura);
        }
        
        var fightersAroundDistance = PhysicsManager.Instance.RaycastFightersInFaceArea(2f, fighter);
        bool isAnyFighterNearDistance = !fightersAround.IsNullOrEmpty();
        
        abilitiesPrioritet.Add(Ability.Name.BatsSwarm);

        Ability.Name result = SelectPriorityAbility(abilitiesPrioritet);

        if (result == Ability.Name.None)
        {
            // TODO: add logic for bool curse
        }

        return result;
    }
}
