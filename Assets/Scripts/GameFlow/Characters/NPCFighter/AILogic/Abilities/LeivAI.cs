﻿using System.Collections.Generic;
using System.Linq;


public class LeivAI : FighterAbilityAI
{
    protected override Ability.Name[] Abilities => throw new System.NotImplementedException();

    public LeivAI(Fighter _fighter) : base(_fighter)
    {
    }

    public override Ability.Name GetOptimalAbility()
    {
        List<Ability.Name> abilitiesPrioritet = new List<Ability.Name>();

        var fightersAround = PhysicsManager.Instance.OverlapFightersInRadius(fighter.transform.position, 5f, fighter);
        bool isAnyFighterNear = !fightersAround.IsNullOrEmpty();

        if (isAnyFighterNear)
        {
            if (fighter.HealthController.CurrentHealthFactor < 0.5f) // improove as last damage time < x
            {
                abilitiesPrioritet.Add(Ability.Name.StoneArmor);
            }
            else
            {
                abilitiesPrioritet.Add(Ability.Name.DeathRing);
            }
        }

        var fightersAroundNearly = PhysicsManager.Instance.OverlapFightersInRadius(fighter.transform.position, 2.0f, fighter);
        bool isAnyFighterNearly = !fightersAround.IsNullOrEmpty();

        if (isAnyFighterNearly)
        {
            abilitiesPrioritet.Add(Ability.Name.ExecutionerAxe);
        }
        
        return SelectPriorityAbility(abilitiesPrioritet);
    }
}
