﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NPCFighterAI
{
    private float[] actionInfo;

    private Fighter currentFighter;
    private NPCFighterSolutions solutions;
    private NPCFighterTriggers triggers;
    private readonly NPCSettings settings;

    private TaskInvoker taskInvoker;
    private SchedulerTask attackTask;

    private readonly Dictionary<int, float> actions = new Dictionary<int, float>(); // id , delay



    public NPCFighterAI(Fighter fighter, NPCFighterSolutions solutionsValue, NPCFighterTriggers triggersValue, NPCSettings _settings)
    {
        currentFighter = fighter;
        solutions = solutionsValue;
        triggers = triggersValue;
        settings = _settings;
        actionInfo = settings.ActionInfoRepeatTime;
        taskInvoker = TaskInvoker.Instance;
    }


    public void Initialize()
    {
        attackTask = null;

        Scheduler.OnUpdate += Scheduler_OnUpdate;
    }


    public void Deinitialize()
    {
        Scheduler.OnUpdate -= Scheduler_OnUpdate;

        taskInvoker.UnscheduleAllMethodForTarget(this);
    }


    private void Scheduler_OnUpdate(float deltaTime)
    {
        foreach (var key in actions.Keys.ToList())
        {
            actions[key] -= deltaTime;
        }

        UpdateLogic();
    }


    public void UpdateLogic()
    {
        triggers.RefreshState(settings);

        if (AllowAction(6))
        {
            float value = Random.value;

            if (value > settings.chanceToSwitchState)
            {
                triggers.TryChangeState(settings);
            }
        }

        //Debug.Log($"state {triggers.State}");

        if (triggers.State == NPCState.Agressive)
        {
            if (triggers.Triggers.loockingAtPlayer) // move to player
            {
                if (triggers.Triggers.enoughRangeForAttackPlayer)
                {
                    currentFighter.MovementController.Stop();
                }
                else if (currentFighter.MovementController.LookingSide == Side.Left)
                {
                    currentFighter.MovementController.MoveLeft();
                }
                else if (currentFighter.MovementController.LookingSide == Side.Right)
                {
                    currentFighter.MovementController.MoveRight();
                }
            }
            else
            {
                if (AllowAction(0))
                {
                    bool chance = Random.value > 0.5f;

                    if (chance)
                        MoveInAnotherSide();
                }

                if (AllowAction(5) &&
                    triggers.Triggers.distanceToBackAnotherFighter < settings.backFreeDistanceToFlipMove)
                {
                    MoveInAnotherSide();
                }
            }

            if (triggers.Triggers.enoughRangeForAttackPlayer &&
                currentFighter.CombatController.IsAttackAvailable &&
                taskInvoker != null)
            {
                attackTask = taskInvoker.CallMethodWithDelay(this, () =>
                {
                    currentFighter.CombatController.Attack();
                    attackTask = null;
                }, settings.AdditionalDelaysForHits);
            }

            if (triggers.Triggers.distanceToPlayerY > settings.distanceToStartJump)
            {
                TryJumpOnUp();
            }
            
            TryJumpOnAvoid();
        }

        else if (triggers.State == NPCState.Avoid)
        {
            if (triggers.Triggers.loockingAtPlayer)
            {
                if (triggers.Triggers.distanceToPlayer < settings.playerBorderDistance)
                {
                    if (AllowAction(1))
                    {
                        bool jumpChance = Random.value > 0.5f ||
                             triggers.Triggers.distanceToBackAnotherFighter < settings.backFreeDistanceToFlipMove;

                        if (jumpChance)
                        {
                            currentFighter.MovementController.Jump();

                            TaskInvoker.Instance.CallMethodWithDelay(this, () =>
                            currentFighter.CombatController.Attack(), 0.1f);
                        }
                        else
                        {
                            MoveInAnotherSide();
                        }
                    }
                }
            }

            if (triggers.Triggers.wallDetected
                && triggers.Triggers.distanceToWall < settings.playerWallDistance)
            {
                if (AllowAction(2))
                {
                    MoveInAnotherSide();
                    triggers.Triggers.wallDetected = false;
                }
            }

            TryJumpOnUp();
            TryJumpOnAvoid();

            // if in borders = go back
        }

        triggers.Collect();
    }


    private void TryJumpOnUp()
    {
        if (settings.IsInFirstLevevlJumpSpace(currentFighter.transform.position) &&
            settings.IsInDoubleJumpSpace(currentFighter.transform.position) &&
            AllowAction(3))
        {
            solutions.Jump();
            TaskInvoker.Instance.CallMethodWithDelay(this, () => solutions.Jump(), 0.28f);
        }

        if (settings.IsInDoubleJumpSpace(currentFighter.transform.position) &&
            AllowAction(4))
        {
            solutions.Jump();
            TaskInvoker.Instance.CallMethodWithDelay(this, () => solutions.Jump(), 0.2f);
        }
    }


    private void TryJumpOnAvoid()
    {
        if (settings.IsAvoidJumpSpace(currentFighter.transform.position))
        {
            solutions.Jump();
        }
    }


    private void MoveInAnotherSide()
    {
        if (currentFighter.MovementController.LookingSide == Side.Left)
        {
            currentFighter.MovementController.MoveRight();
        }
        else if (currentFighter.MovementController.LookingSide == Side.Right)
        {
            currentFighter.MovementController.MoveLeft();
        }
    }


    // not single resposibility ))))
    private bool AllowAction(int id)
    {
        if (!actions.ContainsKey(id))
        {
            if (id > actionInfo.Length)
            {
                throw new KeyNotFoundException();
            }
            actions.Add(id, actionInfo[id]);
        }

        if (actions[id] <= 0)
        {
            actions[id] = actionInfo[id];
            return true;
        }
        return false;
    }
}
