﻿using System.Collections;
using UnityEngine;

public class NPCFighterAIAbilities
{ 
    // remote ?
    private const float AbilitiesRepeatDelay = 3.0f;
    private const float ChargeRepeatDelay = 2.0f;

    private  readonly Fighter currentFighter;

    private TaskInvoker taskInvoker;
    private FighterAbilityAI abilityAI;

    private Coroutine repeatAbilitiesRoutine;
    private Coroutine repeatChragesRoutine;


    public NPCFighterAIAbilities(Fighter fighter)
    {
        currentFighter = fighter;

        abilityAI = fighter.GetAbilityAI();
        taskInvoker = TaskInvoker.Instance;
    }


    public void Initialize()
    {
        if (abilityAI != null)
        {
        //    taskInvoker.CallMethodWithDelay(this, UseOptimalAbility, AbilitiesRepeatDelay);

            repeatAbilitiesRoutine = Scheduler.PlayCoroutine(UseAbilities());
            repeatChragesRoutine = Scheduler.PlayCoroutine(UseCharges());

           // taskInvoker.CallMethodWithDelay(this, TryCharge, ChargeRepeatDelay);
        }
    }


    public void Deinitialize()
    {
        taskInvoker.UnscheduleAllMethodForTarget(this);

        Scheduler.StopPlayingCoroutine(ref repeatAbilitiesRoutine);
        Scheduler.StopPlayingCoroutine(ref repeatChragesRoutine);
    }


    public void UseOptimalAbility()
    {
      Ability.Name abilityNameToUse = abilityAI.GetOptimalAbility();

        if (abilityNameToUse != Ability.Name.None)
        {
            currentFighter.AbilityController.OnPressAbility(abilityNameToUse);

            Ability ability = currentFighter.AbilityController.GetAbility(abilityNameToUse);

            if (ability is OnceResponceAbility) // crnch for leiv
            {
                taskInvoker.CallMethodWithDelay(this, () =>
                {
                    currentFighter.AbilityController.OnPressUpAbility(ability);
                }, 0.5f);
            }

            if (abilityNameToUse == Ability.Name.AwakeningPuppets)
            {
                UseOptimalAbility();
            }
        }
    }


    public void TryCharge()
    {
       (float, Fighter) data = PhysicsManager.Instance.DistanceAndEnemyToFaceFighter(currentFighter);

        if (data.Item2 != null && data.Item1 < 3.0f && data.Item1 > 2.0f)
        {
            currentFighter.MovementController.Charge();
        }
    }


    private IEnumerator UseAbilities()
    {
        while (true)
        {
            yield return new WaitForSeconds(AbilitiesRepeatDelay); // cache
            UseOptimalAbility();
        }
    }


    private IEnumerator UseCharges()
    {
        while (true)
        {
            yield return new WaitForSeconds(ChargeRepeatDelay); // cache
            TryCharge();
        }
    }
}
