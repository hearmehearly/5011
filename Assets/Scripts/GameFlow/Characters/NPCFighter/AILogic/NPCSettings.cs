﻿using UnityEngine;
using System;
using System.Linq;


[CreateAssetMenu]
public class NPCSettings : ScriptableObject
{
    [Serializable]
    private class ActionInfo
    {
        public string name = default;
        public float minDelayForRepeat = default;
    }

    [Serializable]
    public class JumpSpace
    {
        public int level = default;
        public Rect[] positions = default;
    }

    [Header("Default settings")]
    public float cameraVisualDistance = 5.0f;
    public float playerBorderDistance = 2.0f;
    public float playerWallDistance = 28.0f;
    public float distanceToStartJump = 2.0f;
    public float minHealthRateToAgressiveState = 0.5f;
    public float minDistanceForRangersToAvoid = 2.4f;

    [Range(0.0f, 1.0f)] public float chanceToSwitchState = default;
    public float agressiveStateDurationOnSwitch = 2.0f;

    [SerializeField] private float minAdditionalDelaysForHits = default;
    [SerializeField] private float maxAdditionalDelaysForHits = default;

    public float backFreeDistanceToFlipMove = 10.0f;

    [Header("Coefficients")]
    public float attackDistanceCoefficient = 1.0f;

    [Header("Area settings")]
    public JumpSpace[] doubleJumpSpaces = default;
    public JumpSpace[] avoidJumpSpaces = default;


    [Header("Settings")]
    [SerializeField]
    private ActionInfo[] actionInfo = default;

    public float[] ActionInfoRepeatTime => actionInfo.Select(e => e.minDelayForRepeat).ToArray();


    public float AdditionalDelaysForHits => UnityEngine.Random.Range(minAdditionalDelaysForHits, maxAdditionalDelaysForHits);

    public bool IsInFirstLevevlJumpSpace(Vector3 point) => CheckSpace(point, doubleJumpSpaces[0]);

    public bool IsInDoubleJumpSpace(Vector3 point) => CheckSpace(point, doubleJumpSpaces);


    public bool IsAvoidJumpSpace(Vector3 point) => CheckSpace(point, avoidJumpSpaces);


    private bool CheckSpace(Vector3 point, JumpSpace[] jumpSpaces)
    {
        bool result = default;

        foreach (var space in jumpSpaces)
        {
            if (Array.Exists(space.positions, e => e.Contains(point)))
            {
                result = true;
                break;
            }
        }

        return result;
    }


    private bool CheckSpace(Vector3 point, JumpSpace space)
    {
        bool result = default;

        if (Array.Exists(space.positions, e => e.Contains(point)))
        {
            result = true;
        }

        return result;
    }
}
