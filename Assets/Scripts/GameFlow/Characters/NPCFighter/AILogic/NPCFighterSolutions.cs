﻿public class NPCFighterSolutions
{
    private Fighter fighter;

    public NPCFighterSolutions(Fighter fighterValue)
    {
        fighter = fighterValue;
    }

    public void MoveRight()
    {
        fighter.MovementController.MoveRight();
    }


    public void MoveLeft()
    {
        fighter.MovementController.MoveLeft();
    }


    public void Stop()
    {
        fighter.MovementController.Stop();
    }

    public void Jump()
    {
        fighter.MovementController.Jump();
    }


    public void JumpRight()
    {
        fighter.MovementController.MoveRight();
        fighter.MovementController.Jump();
    }


    public void JumpLeft()
    {
        fighter.MovementController.MoveLeft();
        fighter.MovementController.Jump();
    }


    public void ChargeRight()
    {
        fighter.MovementController.MoveRight();
        fighter.MovementController.Stop();
        fighter.MovementController.Charge();
    }


    public void ChargeLeft()
    {
        fighter.MovementController.MoveLeft();
        fighter.MovementController.Stop();
        fighter.MovementController.Charge();
    }


    public void Attack()
    {
        fighter.CombatController.Attack();
    }


    public void SeatDown()
    {
        fighter.MovementController.SeatDown();
    }


    public void StandUp()
    {
        fighter.MovementController.StandUp();
    }
}
