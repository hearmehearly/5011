﻿using Spine.Unity;
using System;
using System.Collections.Generic;
using UnityEngine;


public abstract class Fighter : MonoBehaviour
{
    public enum Name
    {
        None            = 0,
        Leiv            = 1,
        Ralph           = 2,
        Witch           = 3,
        Mary            = 4,
        TrainingTarget  = 8
    }

    public event Action OnDie;
    public event Action OnDeinitialize;

    [SerializeField] protected SkeletonAnimation skeletonAnimation = default;
    [SerializeField] protected Rigidbody2D mainRigidbody = default;
    [SerializeField] protected Rigidbody2D kinematicRigidbody = default;

    [SerializeField] protected CapsuleCollider2D mainCollider = default;
    [UnityEngine.Serialization.FormerlySerializedAs("kmrg2d")]
    [SerializeField] protected CapsuleCollider2D kinematicCollider = default;

    [SerializeField] protected SpriteRenderer[] renderers = default;

    [SerializeField] private SpriteRenderer healthSlider = default;
    [SerializeField] protected CollidableObject collidableObject = default;

    [Header("Combat controller")]
    [SerializeField] private FighterMeleeHitZone hitZone = default;
    [SerializeField] private FighterMeleeHitZone jumpHitZone = default;
    [SerializeField] private FighterMeleeHitZone seatDownHitZone = default;
    [SerializeField] private Transform hitRoot = default;

    [Header("Controll controller")]
    [SerializeField] private ControllIconData controllIconData = default;

    [Header("Debuff controller")]
    [SerializeField] private DebuffIconData debuffIconData = default;

    [Header("Animation controller")]
    [SerializeField] private BoneFollower attackBoneFollower = default;
    [SerializeField] private Transform rangeTransform = default;

    [SerializeField] private BoneFollower[] trailBones = default;


    protected FighterMovementController movementController;
    protected FighterHealthController healthController;
    protected FighterAnimationController animationController;
    protected FighterCombatController combatController;
    protected FighterControlController controlController;
    protected FighterDebuffController debuffController; 
    protected FighterAbilityController abilityController;
    protected FighterRuneController runeController;
    protected FighterEffectController effectController;

    private readonly List<FighterController> controllers = new List<FighterController>();

    private FighterState state = FighterState.None;

    private Action<Fighter> onDieCallback;
    


    protected FighterState State
    {
        get { return state; }
        set
        {
            if (value != state)
            {
                state = value;

                switch (state)
                {
                    case FighterState.None:
                        break;
                    case FighterState.Stand:
                        break;
                    case FighterState.Running:
                        break;
                    case FighterState.Stunned:
                        break;
                    case FighterState.Hitted:
                        break;
                }
            }
        }
    }

    public Name CurrentName { get; private set; }
    
    public CapsuleCollider2D MainCollider => mainCollider;

    public CapsuleCollider2D KinematicCollider => kinematicCollider;

    public CollidableObject CollidableObject => collidableObject;

    public SkeletonAnimation SkeletonAnimation => skeletonAnimation;

    public SpriteRenderer[] Renderers => renderers;

    public abstract Side LookingSide { get; }

    public Vector3 LookingVector => movementController.LookingSide == Side.Left ? Vector3.left : Vector3.right;

    public FighterAbilitiesData AbilitiesData => abilityController.Data;

    public int KillsCounter { get; set; }

    // Controllers
    public FighterMovementController MovementController => movementController;
    
    public FighterCombatController CombatController => combatController;

    public FighterHealthController HealthController => healthController;

    public FighterControlController ControlController => controlController;

    public FighterDebuffController DebuffController => debuffController;

    public FighterAbilityController AbilityController => abilityController;

    public FighterAnimationController AnimationController => animationController;

    public FighterRuneController RuneController => runeController;

    public BoneFollower[] TrailBones => trailBones;

    public virtual void CreateControllers()
    {
        movementController = movementController ?? new FighterMovementController(mainRigidbody, collidableObject);
        healthController = healthController ?? new FighterHealthController(healthSlider);
        combatController = combatController ?? new FighterCombatController(hitZone, jumpHitZone, seatDownHitZone, hitRoot, rangeTransform);
        animationController = animationController ?? new FighterAnimationController(attackBoneFollower);
        abilityController = abilityController ?? new FighterAbilityController();
        controlController = controlController ?? new FighterControlController(controllIconData.Data);
        debuffController = debuffController ?? new FighterDebuffController(debuffIconData.Data);
        runeController = runeController ?? new FighterRuneController();
        effectController = effectController ?? new FighterEffectController();

        controllers.AddExclusive(movementController);
        controllers.AddExclusive(healthController);
        controllers.AddExclusive(combatController);
        controllers.AddExclusive(animationController);
        controllers.AddExclusive(abilityController);
        controllers.AddExclusive(controlController);
        controllers.AddExclusive(debuffController);
        controllers.AddExclusive(runeController);
        controllers.AddExclusive(effectController);
    }


    public virtual void Initialize(Name name)
    {
        CurrentName = name;
        State = FighterState.Stand;
        
        foreach (var controller in controllers)
        {
            controller.Initialize(this);
        }

        gameObject.SetActive(true);

        Physics2D.IgnoreCollision(mainCollider, kinematicCollider);
        SetRigidbodiesSimulated(true);
    }


    public virtual void Deinitialize()
    {
        foreach (var controller in controllers)
        {
            controller.Deinitialize();
        }

        gameObject.SetActive(false); //hack

        OnDeinitialize?.Invoke();
    }


    public virtual void StartLevel()
    {
        healthController.OnShouldDie += Die;
        controlController.OnControlStarted += OnControlStarted;
        controlController.OnControlFinished += OnControlFinished;

        combatController.OnAttack += OnFighterAttack;
        combatController.AttackBlokers = 0;
        combatController.AllowApplyHits();
    }


    public virtual void FinishLevel()
    {
        healthController.OnShouldDie -= Die;
        controlController.OnControlStarted -= OnControlStarted;
        controlController.OnControlFinished -= OnControlFinished;

        combatController.OnAttack -= OnFighterAttack;
        combatController.AttackBlokers++;
        combatController.ForbidApplyHits();
    }


    public void AddOnDieCallback(Action<Fighter> callback)
    {
        onDieCallback += callback;
    }


    public virtual void Die()
    {
        //animationController.

        SetRigidbodiesSimulated(false);
        
        onDieCallback?.Invoke(this);
        onDieCallback = null;

        OnDie?.Invoke();

        FinishLevel();

        animationController.AllowChangeAnimation = true;
        animationController.PlayDeathAnimation(() =>
        {
            Deinitialize();
        });

        animationController.AllowChangeAnimation = false;
    }


    public void ApplyHit(float damage, DamageType damageType, Vector2 impuls, float stunDuration, bool shouldOverKnock, Fighter damageHandler)
    {
        ApplyHit(null, damage, damageType, impuls, stunDuration, shouldOverKnock, damageHandler);
    }


    public void ApplyHit(FighterCombatData data, float damage, DamageType damageType, Vector2 impuls, float stunDuration, bool shouldOverKnock, Fighter damageHandler)
    {
        State = FighterState.Hitted;

        if (data != null)
        {
            ApplyControl(ControlType.Stun, stunDuration, damageHandler);
        }

        healthController.ApplyDamage(damage, damageType, damageHandler);

        if (shouldOverKnock)
        {
            MovementController.KnockOver(damageHandler.transform.position);
        }

        if (damageHandler.MovementController.IsLoockingInBack(this))
        {
            Side sideToFlip = MovementController.LookingSide == Side.Left ? Side.Right : Side.Left;
            MovementController.Flip(sideToFlip);
        }
        
        movementController.AddForce(impuls);

        animationController.PlayApplyHitAnimation();
    }

    
    public virtual void ApplyHit(float damage, DamageType damageType, Fighter damageHandler)
    {
        healthController.ApplyDamage(damage, damageType, damageHandler);
        animationController.PlayApplyHitAnimation();
    }


    public virtual void ApplyHit(DataDamageValue damageValue, Fighter damageHandler)
    {
        ApplyHit(null,
                damageValue.GetDamage(),
                damageValue.damageType,
                damageValue.HitImpuls * damageHandler.movementController.VelocityVectorMultiplier,
                damageValue.StunDurationPerHit,
                damageValue.ShouldKnockOver,
                damageHandler);
    }


    public virtual void ApplyHit(DamageValue damageValue, Fighter damageHandler)
    {
        ApplyHit(null,
                damageValue.GetDamage(),
                damageValue.damageType,
                damageValue.HitImpuls * damageHandler.movementController.VelocityVectorMultiplier,
                default,
                damageValue.ShouldKnockOver,
                damageHandler);
    }


    public virtual void ApplyControl(ControlType controlType, float duration, object handler)
    {
        controlController.ApplyControl(controlType, duration, handler);
    }


    public void ResetControl()
    {
        controlController.ApplyDispel();
    }


    protected void MoveLeft()
    {
        if (movementController.AllowMovement)
        {
            movementController.MoveLeft();
        }
    }


    protected void MoveRight()
    {
        if (movementController.AllowMovement)
        {
            movementController.MoveRight();
        }
    }


    protected void Stop()
    {
        movementController.Stop();
    }


    protected void Jump(bool isFromJoystick)
    {
        movementController.Jump(isFromJoystick);
    }

    protected void SeatDown()
    {
        movementController.SeatDown();
    }


    protected void StandUp()
    {
        movementController.StandUp();
    }


    protected void Charge()
    {
        movementController.Charge();
    }


    protected void Attack()
    {
        combatController.Attack();
    }


    protected void OnPressAbility(Ability ability)
    {
        abilityController.OnPressAbility(ability);
    }


    protected void OnPressUpAbility(Ability ability)
    {
        abilityController.OnPressUpAbility(ability);
    }


    protected virtual void OnFighterAttack(FighterCombatData data)
    {
        PlayHitAnimation();

        if (movementController.IsSetDown && combatController.IsMelee)
        {
            movementController.ForceStop();
            movementController.StandUp();
        }
    }


    protected virtual void OnControlStarted(ControlType controlType, object handler)
    {
        switch (controlType)
        {
            case ControlType.None:
                break;
            case ControlType.InputDisable:
            case ControlType.Stun:
                combatController.AttackBlokers++;
                abilityController.AbilityBlokers++;
                movementController.MovementBlokers++;
                movementController.ChargeBlockers++;
                movementController.JumpBlokers++;
                break;
            case ControlType.Slowdown:
                movementController.SetSpeedFactor(0.5f);
                break;
            case ControlType.Paralysis:
                movementController.MovementBlokers++;
                movementController.ChargeBlockers++;
                movementController.JumpBlokers++;
                break;
            case ControlType.Silence:
                abilityController.AbilityBlokers++;
                break;
            case ControlType.Disarm:
                combatController.AttackBlokers++;
                break;
            case ControlType.Immunity:
                break;
            case ControlType.MobilityCuts:
                movementController.ChargeBlockers++;
                movementController.SetJumpSpeedFactor(0.5f);
                break;
            default:
                throw new NotImplementedException();
        }
    }


    protected virtual void OnControlFinished(ControlType controlType, object handler)
    {
        switch (controlType)
        {
            case ControlType.None:
                break;
            case ControlType.InputDisable:
            case ControlType.Stun:
                combatController.AttackBlokers--;
                abilityController.AbilityBlokers--;
                movementController.MovementBlokers--;
                movementController.ChargeBlockers--;
                movementController.JumpBlokers--;
                break;
            case ControlType.Slowdown:
                movementController.SetSpeedFactor(1.0f);
                break;
            case ControlType.Paralysis:
                movementController.MovementBlokers--;
                movementController.ChargeBlockers--;
                movementController.JumpBlokers--;
                break;
            case ControlType.Silence:
                abilityController.AbilityBlokers--;
                break;
            case ControlType.Disarm:
                combatController.AttackBlokers--;
                break;
            case ControlType.Immunity:
                break;
            case ControlType.MobilityCuts:
                movementController.ChargeBlockers--;
                movementController.SetJumpSpeedFactor(1.0f);
                break;
            default:
                throw new NotImplementedException();
        }
    }
    

    public FighterAbilityAI GetAbilityAI()
    {
        FighterAbilityAI result = default;

        switch (CurrentName)
        {
            case Name.Leiv:
                result = new LeivAI(this);
                break;
            case Name.Ralph:
                break;
            case Name.Witch:
                result = new WitchAI(this);
                break;
            case Name.Mary:
                result = new MaryAI(this);
                break;
            default:
                Debug.Log($"No Ability AI implemeneted for fighter with name {CurrentName}");
                break;
        }

        return result;
    }


    protected void PlayHitAnimation()
    {
        if (!movementController.IsGrounded)
        {
            animationController.PlayHitJumpAnimation();
        }
        else if (movementController.IsSetDown)
        {
            animationController.PlayHitSeatAnimation();
        }
        else
        {
            animationController.PlayHitAnimation(combatController.ComboCounter);
        }
    }


    private void SetRigidbodiesSimulated(bool enabled)
    {
        mainRigidbody.simulated = enabled;
        kinematicRigidbody.simulated = enabled;
    }
    

    private void OnValidate()
    {
        renderers = GetComponentsInChildren<SpriteRenderer>(true);
    }
}
