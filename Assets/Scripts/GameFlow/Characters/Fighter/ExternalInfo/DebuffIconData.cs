﻿using UnityEngine;


public class DebuffIconData : MonoBehaviour
{
    [SerializeField] private FighterDebuffController.IconData[] data = default;

    public FighterDebuffController.IconData[] Data => data;
}
