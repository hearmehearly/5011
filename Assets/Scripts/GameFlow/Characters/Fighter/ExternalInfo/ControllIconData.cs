﻿using UnityEngine;


public class ControllIconData : MonoBehaviour
{
    [SerializeField] private FighterControlController.IconData[] data = default;

    public FighterControlController.IconData[] Data => data;
}
