﻿using System;
using UnityEngine;


public class FighterBullet : FighterHitZone
{
    public event Action<Fighter, int, Vector3> OnShouldHitFighter;


    [SerializeField]
    private Rigidbody2D mainRigidbody2D = null;

    private int initializedCombo;


    public void Initialize(int combo, float maxDistance)
    {
        initializedCombo = combo;

        PhysicsManager.Instance.AddDistanceRigidbody(mainRigidbody2D, maxDistance, Deinitialize);

        // collidableObject.OnCustomTriggerEnter2D += CollidableObject_OnCustomTriggerEnter2D;

    }


    public void Deinitialize()
    {
        PhysicsManager.Instance.RemoveDistanceRigidbody(mainRigidbody2D);
        DisableZone();
        Destroy(gameObject);
    }


    public void SetVelocity(Vector2 velocity)
    {
        mainRigidbody2D.velocity = velocity;
    }


    protected override void ProcessNPCCollision(Fighter fighter)
    {
        base.ProcessNPCCollision(fighter);

        Deinitialize();
    }


    public override void TriggerShouldHitFighterEvent(Fighter fighter)
    {
        OnShouldHitFighter?.Invoke(fighter, initializedCombo, mainRigidbody2D.velocity.normalized);
    }
}
