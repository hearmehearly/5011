﻿using System;


public class FighterMeleeHitZone : FighterHitZone
{
    public event Action<Fighter> OnShouldHitFighter;



    public override void TriggerShouldHitFighterEvent(Fighter fighter)
    {
        OnShouldHitFighter?.Invoke(fighter);
    }
}
