﻿using System;
using UnityEngine;


public class IndestructibleProjectile : FighterHitZone
{
    [Serializable]
    public struct Data
    {
        public float MaxDistance;
        public DebuffValue BurnValue;
        public DamageValue ImmediateDamage;

        public Vector2 Velocity;
    }


    [SerializeField]
    private Rigidbody2D mainRigidbody2D = default;

    private Data currentData;
    private Fighter owner;


    public void Initialize(Data data, Fighter ownerValue)
    {
        currentData = data;
        owner = ownerValue;

        PhysicsManager.Instance.AddDistanceRigidbody(mainRigidbody2D, currentData.MaxDistance, Deinitialize);

        mainRigidbody2D.velocity = new Vector2(owner.transform.lossyScale.x * currentData.Velocity.x, currentData.Velocity.y);
        EnableZone();
    }


    public void Deinitialize()
    {
        PhysicsManager.Instance.RemoveDistanceRigidbody(mainRigidbody2D);

        DisableZone();
        Destroy(gameObject);
    }



    protected override void ProcessNPCCollision(Fighter fighter)
    {
        base.ProcessNPCCollision(fighter);

        fighter.ApplyHit(currentData.ImmediateDamage, owner);
        fighter.DebuffController.ApplyDebuff(currentData.BurnValue, owner);
    }


    public override void TriggerShouldHitFighterEvent(Fighter fighter)
    {
      //  throw new NotImplementedException();
    }
}
