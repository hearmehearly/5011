﻿using System;
using UnityEngine;


public class FighterAbilityProjectile : FighterHitZone
{
    [Serializable]
    public struct Data
    {
        [Tooltip("дебафы")]
        public DebuffValue[] Debuffs;
        [Tooltip("мгновенны урон")]
        public DamageValue ImmediateDamage;

        [Tooltip("скорость")]
        public Vector2 Velocity;
        [Tooltip("дистанция")]
        public float MaxDistance;
    }

    public event Action<Fighter> OnHitFighter;

    [SerializeField]
    private Rigidbody2D mainRigidbody2D = default;

    private Data currentData;
    private Fighter owner;
    private Action deinitializeCallback;


    public void Initialize(Data data, Fighter ownerValue, Action deinitializeCallbackValue = null)
    {
        currentData = data;
        owner = ownerValue;
        deinitializeCallback = deinitializeCallbackValue;

        PhysicsManager.Instance.AddDistanceRigidbody(mainRigidbody2D, currentData.MaxDistance, Deinitialize);

        mainRigidbody2D.velocity = new Vector2(owner.transform.lossyScale.x * currentData.Velocity.x, currentData.Velocity.y);
        EnableZone();
    }


    public void Deinitialize()
    {
        PhysicsManager.Instance.RemoveDistanceRigidbody(mainRigidbody2D);

        DisableZone();

        deinitializeCallback?.Invoke();
        Destroy(gameObject);
    }



    protected override void ProcessNPCCollision(Fighter fighter)
    {
        base.ProcessNPCCollision(fighter);

        fighter.ApplyHit(currentData.ImmediateDamage, owner);

        foreach (var debuff in currentData.Debuffs)
        {
            fighter.DebuffController.ApplyDebuff(debuff, owner);
        }

        Deinitialize();
    }


    public override void TriggerShouldHitFighterEvent(Fighter fighter)
    {
        OnHitFighter?.Invoke(fighter);
    }
}
