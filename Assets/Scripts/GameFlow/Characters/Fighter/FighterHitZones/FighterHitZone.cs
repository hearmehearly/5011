﻿using System;
using UnityEngine;


public abstract class FighterHitZone : MonoBehaviour
{
    private readonly CollidableObject.Type[] allowedAttackTypes = { CollidableObject.Type.NPCFighter, CollidableObject.Type.Player };

    [SerializeField]
    private BoxCollider2D hitCollider = default;
    [SerializeField]
    private CollidableObject collidableObject = default;

    [SerializeField]
    private bool shouldDrawGizmos = true;
    [Tooltip("Какого цвета будет рисоваться бокс. Только для отладка")]
    [SerializeField]
    private Color gizmosColor = Color.black;



    public bool WasHitOnLastEnable { get; private set; }


    public BoxCollider2D HitCollider => hitCollider;



    public void EnableZone()
    {
        if (hitCollider == null)
        {
            return;
        }

        // disable collider and disable trigger for next enabling to catch trigger collision if hit collider enabled when fighter had velocity Vector2.zero

        collidableObject.OnCustomTriggerEnter2D += CollidableObject_OnCustomTriggerEnter2D;

        hitCollider.enabled = false;
        hitCollider.isTrigger = false;

        hitCollider.enabled = true;
        hitCollider.isTrigger = true;

        WasHitOnLastEnable = false;
    }


    public void DisableZone()
    {
        if (hitCollider == null)
        {
            return;
        }

        hitCollider.enabled = false;
        collidableObject.OnCustomTriggerEnter2D -= CollidableObject_OnCustomTriggerEnter2D;
    }


    private void CollidableObject_OnCustomTriggerEnter2D(Collider2D collider, CollidableObject.Type type)
    {
        if (Array.IndexOf(allowedAttackTypes, type) != -1)
        {
            Fighter fighter = collider.GetComponent<Fighter>();
            
            if (!fighter.CombatController.IsDodging)
            {
                WasHitOnLastEnable = true;

                TriggerShouldHitFighterEvent(fighter);
                ProcessNPCCollision(fighter); // event firstly cuz of can be destroyed
            }
        }
    }


    public abstract void TriggerShouldHitFighterEvent(Fighter fighter);


    protected virtual void ProcessNPCCollision(Fighter fighter)
    {
    }


    private void OnDrawGizmos()
    {
        if ((TrainingRoomManager.Instance && Camera.current == TrainingRoomManager.Instance.GameCamera)
            || (GameManager.Instance && Camera.current == GameManager.Instance.GameCamera)
            && shouldDrawGizmos)
        {
            Gizmos.color = gizmosColor;
            Gizmos.DrawCube(hitCollider.bounds.center, hitCollider.bounds.size);
        }
    }
}
