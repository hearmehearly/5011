﻿public enum FighterState
{
    None             = 0,
    Stand            = 1,
    Running          = 2,
    Stunned          = 3,
    Hitted           = 4
}
