﻿using System;
using UnityEngine;
using DG.Tweening;


public class FighterHealthController : FighterController
{
    public event Action OnHealthChanged;
    public event Action OnShouldDie;
    public event Action<float> OnApplyHit;
    
    private SpriteRenderer healthSlider;

    private FighterHealthData data;
    float? initialHealthSliderValue;


    public float AppliedDamageMultiplier { get; set; } = 1.0f;

    public float CurrentHealth { get; private set; }

    public float MaxHealth { get; private set; }

    public float CurrentHealthFactor => MaxHealth == 0 ? default : CurrentHealth / MaxHealth;


    public bool IsAlive => CurrentHealth > 0.0f;
    private bool IsDeadAnimation { get; set; }


    public FighterHealthController(SpriteRenderer _healthSlider)
    {
        healthSlider = _healthSlider;
    }


    public override void Initialize(Fighter fighter)
    {
        base.Initialize(fighter);

        currentFighter = fighter;
        data = Fighters.GetHealthData(currentFighter.CurrentName);

        CurrentHealth = data.Health;
        MaxHealth = data.Health;

        initialHealthSliderValue = initialHealthSliderValue ?? (healthSlider ? healthSlider.size.x : 0.0f);
        
        healthSlider.size = new Vector2(initialHealthSliderValue.Value, healthSlider.size.y);
        RunSliderTween();

        IsDeadAnimation = false;
    }


    public override void Deinitialize()
    {
        DOTween.Kill(this);
        DOTween.Kill(healthSlider);

        if (healthSlider != null)
        {
            healthSlider.size = Vector2.zero.SetY(healthSlider.size.y);
        }
    }


    public void ApplyDamage(float damage, DamageType damageType, Fighter damageHandler)
    {
        damage *= AppliedDamageMultiplier;

        CurrentHealth -= damage;

        RunSliderTween();
       
        OnApplyHit?.Invoke(damage);

        if (!IsAlive)
        {
            if (damageHandler != null && !IsDeadAnimation)
            {
                damageHandler.CombatController.TriggerGottaKillEvent(currentFighter);
            }

            IsDeadAnimation = true;
            OnShouldDie?.Invoke();
        }
    }


    public void AddHealth(float value)
    {
        CurrentHealth += value;
        CurrentHealth = Mathf.Min(CurrentHealth, MaxHealth);

        RunSliderTween();
    }


    public void IncreaseHealth(float percentValue)
    {
        float currentHealthPercent = CurrentHealth / MaxHealth;
        MaxHealth += MaxHealth * percentValue;
        CurrentHealth = MaxHealth * currentHealthPercent;
    }


    public void DecreaseHealth(float percentValue)
    {
        float currentHealthPercent = CurrentHealth / MaxHealth;
        MaxHealth -= MaxHealth * percentValue;
        CurrentHealth = MaxHealth * currentHealthPercent;
    }


    private void RunSliderTween()
    {
        if (healthSlider != null)
        {
            healthSlider.DOComplete();

            float endValue = IsAlive ? CurrentHealth * initialHealthSliderValue.Value / MaxHealth : default;

            DOTween.To(() => healthSlider.size.x,
                       x =>
                       {
                           if (healthSlider != null)
                           {
                               healthSlider.size = new Vector2(x, healthSlider.size.y);
                           }
                           else
                           {
                               DOTween.Kill(this);
                           }
                       },
                       endValue,
                       0.5f) // TODO: in data
                       .SetEase(Ease.Linear)
                       .SetTarget(healthSlider)
                       .SetId(this);
        }

        OnHealthChanged?.Invoke();        
    }
}
