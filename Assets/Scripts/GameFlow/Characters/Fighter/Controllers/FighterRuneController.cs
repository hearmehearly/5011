﻿using System.Collections.Generic;


public class FighterRuneController : FighterController
{
    private readonly List<Rune> currentRunes = new List<Rune>();


    public override void Initialize(Fighter fighter)
    {
        base.Initialize(fighter);
    }

    public override void Deinitialize()
    {
        for (int i = currentRunes.Count - 1; i >= 0; i--)
        {
            currentRunes[i].OnResetted -= Rune_OnShouldReset;
            currentRunes[i].Reset();
        }

        currentRunes.Clear();
    }


    public void Apply(Rune rune)
    {
        rune.Apply();
        rune.OnResetted += Rune_OnShouldReset;

        currentRunes.Add(rune);
    }

    private void Rune_OnShouldReset(Rune rune)
    {
        rune.OnResetted += Rune_OnShouldReset;
        currentRunes.Remove(rune);
    }

    public void Reset(Rune rune)
    {
        rune.OnResetted -= Rune_OnShouldReset;
        currentRunes.Remove(rune);
    }
}
