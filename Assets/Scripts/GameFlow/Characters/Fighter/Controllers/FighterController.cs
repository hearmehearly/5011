﻿public abstract class FighterController
{
    protected Fighter currentFighter;

    public virtual void Initialize(Fighter fighter)
    {
        currentFighter = fighter;
    }

    public abstract void Deinitialize();
}
