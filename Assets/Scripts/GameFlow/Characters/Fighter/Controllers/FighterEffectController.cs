﻿using UnityEngine;
using DG.Tweening;
using Spine.Unity;
using Spine;
using System;


public class FighterEffectController : FighterController
{
    private FxHandler showFx;
    private FxHandler hideFx;

    private bool shouldPlayOnDeinit;

    public override void Initialize(Fighter fighter)
    {
        base.Initialize(fighter);

        showFx = Fighters.GetVisualData(fighter.CurrentName).showFx;
        hideFx = Fighters.GetVisualData(fighter.CurrentName).hideFx;

        TaskInvoker.Instance.CallMethodWithDelay(this, PlayShowFx, 0.2f);

        shouldPlayOnDeinit = true;

        fighter.AddOnDieCallback(OnDie);
    }

    private void OnDie(Fighter f)
    {
        PlayHideFx();
        shouldPlayOnDeinit = false;
    }

    public override void Deinitialize()
    {
        TaskInvoker.Instance.UnscheduleAllMethodForTarget(this);

        if (shouldPlayOnDeinit)
        {
            hideFx.PlayImmediatelyOnce(currentFighter.transform.position);
        }
    }

    private void PlayShowFx() =>
        showFx.PlayOnce(currentFighter.transform.position);

    private void PlayHideFx() =>
        hideFx.PlayOnce(currentFighter.transform.position);
}
