﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class FighterAbilityController : FighterController
{
    public List<Ability> currentAbilities;

    public FighterAbilitiesData Data { get; private set; }

    public int AbilityBlokers { get; set; }

    public bool AllowUsingAbilities => AbilityBlokers == 0;




    public override void Initialize(Fighter fighter)
    {
        base.Initialize(fighter);

        Data = Fighters.GetAbilityData(fighter.CurrentName);
        currentAbilities = new List<Ability>();

        currentFighter = fighter;
        currentFighter.ControlController.OnControlStarted += TryStopAbility;

        foreach (var i in Data.Abilities)
        {
            Ability clonedAbility = i.Clone() as Ability;
            currentAbilities.Add(clonedAbility);
        }

        // firstly add all. then all initialize as they may be combined
        foreach (var ability in currentAbilities)
        {
            ability.Initialize(currentFighter);

            if (ability is IAbilityRootHitable abilityRootHitable)
            {
                abilityRootHitable.InitializeHitRoot(currentFighter.CombatController.HitRoot);
            }

            if (ability is ActiveAbility activeAbility)
            {
                activeAbility.FastResetCoolDown();
            }
        }

        AbilityBlokers = 0;
    }


    public override void Deinitialize()
    {
        foreach (var i in currentAbilities)
        {
            i.Deinitialize();
        }

        currentAbilities.Clear();
        currentFighter.ControlController.OnControlStarted -= TryStopAbility;
    }


    public void OnPressAbility(Ability.Name name)
    {
        Ability abilityToUse = GetAbility(name);
        OnPressAbility(abilityToUse);
    }


    public void OnPressAbility(Ability ability)
    {
        if (AllowUsingAbilities)
        {
            Ability abilityToUse = currentAbilities.Find(element => element.Equals(ability));

            if (abilityToUse != null)
            {
                if (abilityToUse is OnceResponceAbility onceResponceAbility)
                {
                    OnPressAbilityProceed(onceResponceAbility);
                }
                else if (abilityToUse is DoubleUseAbility doubleUseAbility)
                {
                    OnPressAbilityProceed(doubleUseAbility);
                }
            }
            else
            {
                Debug.LogError("No ability " + ability + " exists for fighter " + currentFighter.name);
            }
        }
    }


    public void OnPressUpAbility(Ability ability)
    {
        if (currentAbilities.Count == 0)
        {
            return;
        }

      //  if (AllowUsingAbilities)
        {
            Ability abilityToUse = currentAbilities.Find(element => element == ability);

            if (abilityToUse != null)
            {
                if (abilityToUse is OnceResponceAbility onceResponceAbility)
                {
                    if (onceResponceAbility.ResponceType == ActiveAbility.Responce.ClickUp)
                    {
                        onceResponceAbility.StopHoldTimer();

                        if (onceResponceAbility.IsPreparedForUse && onceResponceAbility.AllowUse)
                        {
                            onceResponceAbility.SetCoolDown();
                            onceResponceAbility.UseAbility();

                            onceResponceAbility.IsPreparedForUse = false;
                        }
                    }
                }
            }
            else
            {
                Debug.LogError("No ability " + ability.name + " exists for fighter " + currentFighter.name);
            }
        }
    }


    public void FastResetCooldown(Ability abilityToStop)
    {
        Ability ability = currentAbilities.Find(element => element.Equals(abilityToStop));

        if (ability != null && ability is ActiveAbility activeAbility)
        {
            activeAbility.FastResetCoolDown();
        }
    }


    public bool TryIncreaseCoolDown(Ability ability, float duration)
    {
        if (ability != null && ability is ActiveAbility activeAbility)
        {
            return activeAbility.TryIncreaseCoolDown(duration);
        }

        throw new NotSupportedException();
    }


    public void IncreaseCooldown(ActiveAbility ability, float percent)
    {
        if (ability != null)
        {
            ability.IncreaseCooldown(percent);
        }
    }


    public void IncreaseCooldownPercent(ActiveAbility ability, float percent)
    {
        if (ability != null)
        {
            ability.IncreaseCooldownPercent(percent);
        }
    }


    public void DecreaseCooldownPercent(ActiveAbility ability, float percent)
    {
        if (ability != null)
        {
            ability.DecreaseCooldownPercent(percent);
        }
    }


    public float CurrentCooldownTimer(ActiveAbility activeAbility)
    {
        if (activeAbility != null)
        {
            return activeAbility.coolDownTimer;
        }

        throw new NotSupportedException();
    }


    public Ability GetAbility(Ability.Name name)
    {
        Ability ability = currentAbilities.Find(element => element.AbilityName == name);

        if (ability == null)
        {
            Debug.Log($"No ability {name} in fighter {currentFighter}");
        }

        return ability;
    }


    private void TryStopAbility(ControlType controlType, object handler)
    {
        foreach (var ability in currentAbilities)
        {
            if (ability is ActiveAbility activeAbility && activeAbility.ResponceType == ActiveAbility.Responce.ClickUp && activeAbility.IsPreparedForUse)
            {
                if (ability.ControlToStop.Contains(controlType))
                {
                    activeAbility.IsPreparedForUse = false;
                }
            }
        }
    }


    private void OnPressAbilityProceed(OnceResponceAbility onceResponceAbility)
    {
        if (onceResponceAbility.AllowUse)
        {
            onceResponceAbility.OnPress();

            if (onceResponceAbility.ResponceType == ActiveAbility.Responce.Press)
            {
                if (onceResponceAbility.isCoolDownExpired) // use always is cd ok
                {
                    onceResponceAbility.SetCoolDown();
                    onceResponceAbility.UseAbility();
                }
            }
            else if (onceResponceAbility.ResponceType == ActiveAbility.Responce.ClickUp)
            {
                if (onceResponceAbility.isCoolDownExpired)
                {
                    onceResponceAbility.IsPreparedForUse = true;

                    onceResponceAbility.ResetHoldTimer();
                    onceResponceAbility.StartHoldTimer();
                }
            }
        }
    }


    private void OnPressAbilityProceed(DoubleUseAbility doubleUseAbility)
    {
        if (doubleUseAbility.ResponceType == ActiveAbility.Responce.Press &&
            doubleUseAbility.AllowUse)
        {
            if (doubleUseAbility.isCoolDownExpired) // use always is cd ok
            {
                doubleUseAbility.SetCoolDown();
                doubleUseAbility.UseAbility();
            }
            else if (doubleUseAbility.IsSecondUseAvailable)
            {
                doubleUseAbility.UseAbilitySecond();
            }
        }
        else
        {
            throw new NotImplementedException();
        }
    }
}
