﻿using DG.Tweening;
using System;
using UnityEngine;


public enum Side
{
    None        = 0,
    Right       = 1,
    Left        = 2
}


public class FighterMovementController : FighterController
{
    public event Action OnGroundedOnLegs;
    public event Action OnGroundCollision;
    public event Action OnJump;
    public event Action OnDoubleJump;
    public event Action OnRun;
    public event Action OnFinishRun;
    
    protected CollidableObject collidableFighter;

    protected FighterMovementData data;
    
    private Collider2D mainCollider;
    protected Rigidbody2D mainRigidbody;
    private Vector2 movingDirection;

    private int jumpCounter;
    private float jumpSpeedFactor = 1.0f;
    private ChargeAbility chargeAbility;

    public int ChargeBlockers { get; set; }
    public int MovementBlokers { get; set; }
    public int JumpBlokers { get; set; }
    public int DoubleJumpBlokers { get; set; }


    private Vector2? initialMainColliderSize;
    private Vector2? initialKinematicColliderSize;


    public float SpeedFactor { get; set; } = 1.0f; // + or -. plz never set as well cuz of control options

    public Side LookingSide { get; private set; } = Side.Right;


    public Vector3 SideVector => LookingSide == Side.Right ? Vector3.right : Vector3.left;


    public Vector3 BackVector => LookingSide == Side.Left ? Vector3.right : Vector3.left;


    public float VelocityMultiplier => LookingSide == Side.Right ? 1.0f : -1.0f;

    public Vector3 VelocityVectorMultiplier => currentFighter.transform.lossyScale.normalized;

    public Vector2 Velocity
    {
        get => mainRigidbody == null ? Vector2.zero : mainRigidbody.velocity;
        set => mainRigidbody.velocity = value;
    }


    public Rigidbody2D MainRigidbody => mainRigidbody;


    public bool AllowCharge => ChargeBlockers == 0;


    public bool AllowMovement => MovementBlokers == 0;


    public bool AllowJump => JumpBlokers == 0;


    public bool AllowDoubleJump => DoubleJumpBlokers == 0;


    public bool IsGrounded { get; private set; } = true;


    public bool IsSetDown { get; private set; }

    public bool IsKnockingOver { get; private set; }


    public FighterMovementController(Rigidbody2D rigidbody, CollidableObject _collidableFighter)
    {
        mainRigidbody = rigidbody;
        collidableFighter = _collidableFighter;
    }


    public override void Initialize(Fighter fighter)
    {
        base.Initialize(fighter);

        data = Fighters.GetMovementData(currentFighter.CurrentName);

        if (data.Charge != null)
        {
            chargeAbility = data.Charge.Clone() as ChargeAbility;
            chargeAbility.Initialize(currentFighter);
        }

        SpeedFactor = 1.0f;
        mainCollider = currentFighter.MainCollider;

        initialMainColliderSize = initialMainColliderSize ?? currentFighter.MainCollider.size;
        initialKinematicColliderSize = initialKinematicColliderSize ?? currentFighter.KinematicCollider.size;

        currentFighter.MainCollider.size = initialMainColliderSize.Value;
        currentFighter.MainCollider.offset = Vector2.zero;

        currentFighter.KinematicCollider.size = initialKinematicColliderSize.Value;
        currentFighter.KinematicCollider.offset = Vector2.zero;

        collidableFighter.OnCustomCollisionEnter2D += ProccesCollistion;
        currentFighter.CombatController.OnPreAttack += OnPreAttack;
        Scheduler.OnUpdate += OnUpdate;
        
        currentFighter.MainCollider.sharedMaterial = new PhysicsMaterial2D
        {
            bounciness = currentFighter.MainCollider.sharedMaterial.bounciness,
            friction = currentFighter.MainCollider.sharedMaterial.friction
        };

        ResetKnockOver();

        ForceStop();

        MovementBlokers = 0;
        DoubleJumpBlokers = 0;
        JumpBlokers = 0;
        ChargeBlockers = 0;

        SetJumpSpeedFactor(1.0f);
    }


    public override void Deinitialize()
    {
        if (chargeAbility != null)
        {
            chargeAbility.Deinitialize();
        }
        
        OnGroundedOnLegs = null;
        OnGroundCollision = null;
        collidableFighter.OnCustomCollisionEnter2D -= ProccesCollistion;
        currentFighter.CombatController.OnPreAttack -= OnPreAttack;
        Scheduler.OnUpdate -= OnUpdate;

        TaskInvoker.Instance.UnscheduleAllMethodForTarget(this);
        DOTween.Kill(mainRigidbody);
    }


    public void AddRelativeToSideForce(Vector2 force)
    {
        Vector2 forceToAdd = new Vector2(currentFighter.transform.localScale.x * force.x, currentFighter.transform.localScale.y * force.y);
        mainRigidbody.AddForce(forceToAdd, ForceMode2D.Impulse);
    }


    public void AddForce(Vector2 force)
    {
        mainRigidbody.AddForce(force, ForceMode2D.Impulse);
    }


    public void MoveFaceSide()
    {
        if (LookingSide == Side.Left)
        {
            MoveLeft();
        }
        else if (LookingSide == Side.Right)
        {
            MoveRight();
        }
    }


    public void MoveRight()
    {
        DOTween.Kill(mainRigidbody);

        movingDirection = Vector2.right;
        LookingSide = Side.Right;

        currentFighter.transform.localScale = currentFighter.transform.localScale.SetX(1.0f);
    }


    public void MoveLeft()
    {
        DOTween.Kill(mainRigidbody);

        movingDirection = Vector2.left;
        LookingSide = Side.Left;

        currentFighter.transform.localScale = currentFighter.transform.localScale.SetX(-1.0f);
    }


    public void MoveSide(Side side)
    {
        if (side == Side.Left)
        {
            MoveLeft();
        }
        else
        {
            MoveRight();
        }
    }


    public void Flip(Side side)
    {
        MoveSide(side);
        Stop();
    }


    public void ForceStop()
    {
        movingDirection = Vector2.zero;
        mainRigidbody.velocity = Vector2.zero;
        
        OnFinishRun?.Invoke();
    }


    public void Stop()
    {
        movingDirection = Vector2.zero;

        DOTween.Kill(mainRigidbody);

        DOTween
            .To(() => mainRigidbody.velocity.x, x => mainRigidbody.velocity = mainRigidbody.velocity.SetX(x), 0.0f, data.DurationToTotalStop)
            .SetTarget(mainRigidbody);

        OnFinishRun?.Invoke();
    }


    private void Move(Vector2 direction)
    {
        if (AllowMovement)
        {
            //Vector3 currentPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            //Vector3 positionToMove = currentPosition + Vector3.left * data.MoveSpeed * Time.deltaTime;

            currentFighter.transform.localScale = currentFighter.transform.localScale.SetX(direction.x);

            float moveOffset = data.MoveSpeed * Time.deltaTime;
            Vector2 force = moveOffset * direction * SpeedFactor;

            bool isMaxVelocityAchived = (mainRigidbody.velocity.x < -data.MaxMoveSpeed * SpeedFactor && force.x < 0.0f) ||
                                        (mainRigidbody.velocity.x > data.MaxMoveSpeed * SpeedFactor && force.x > 0.0f);

            if (isMaxVelocityAchived)
            {
                return;
            }

            mainRigidbody.AddForce(force, ForceMode2D.Impulse);
        }
    }

    public void Jump()
    {
        Jump(IsGrounded);
    }


    public void Jump(bool isFromJoystick)
    {
        if (AllowJump)
        {
            bool canJumpFirstly = jumpCounter == 0 && 
                                  IsGrounded && 
                                  Velocity.y >= 0.0f &&
                                  isFromJoystick;

            bool canJumpSecondly = jumpCounter > 0 &&
                                   jumpCounter < data.JumpForces.Length &&
                                   !IsGrounded &&
                                   AllowDoubleJump &&
                                   !isFromJoystick;
            // && mainRigidbody.velocity.y > 0.0f; disabled for test's. Maybe will be returned

            if (canJumpFirstly || canJumpSecondly)
            {
                if (jumpCounter > 0)
                {
                    mainRigidbody.velocity = new Vector2(mainRigidbody.velocity.x, 0.0f);
                }

                // a = (vf^2 - v0^2) / 2d
                float jumpForce = Mathf.Sqrt(2 * data.JumpForces[jumpCounter] * Mathf.Abs(Physics2D.gravity.y));
                jumpForce *= jumpSpeedFactor;

                mainRigidbody.AddForce(new Vector2(0.0f, jumpForce), ForceMode2D.Impulse);

                jumpCounter++;
                IsGrounded = false;

                currentFighter.AnimationController.PlayJumpAnimation();
                OnJump?.Invoke();

                if (jumpCounter == 2)
                {
                  //  ApplyDoubleJumpCooldown();

                    OnDoubleJump?.Invoke();
                }
            }
        }
    }


    private void ApplyDoubleJumpCooldown()
    {
        DoubleJumpBlokers++;
        currentFighter.ControlController.ShowIcon(ControlType.ChargesBlock);

        TaskInvoker.Instance.CallMethodWithDelay(this, () =>
        {
            DoubleJumpBlokers--;
            currentFighter.ControlController.HideIcon(ControlType.ChargesBlock);
        }, data.DoubleJumpCooldown);
    }


    private void ProccesCollistion(Collision2D collision, CollidableObject.Type type)
    {
        if (type == CollidableObject.Type.Ground)
        {
            ResetJumpCount();
            OnGroundCollision?.Invoke();
        }
        else if (!IsGrounded && type == CollidableObject.Type.FighterPhysics)
        {
            Stop();
        }


        void ResetJumpCount()
        {
            ColliderDistance2D colliderDistance = collision.collider.Distance(mainCollider);

            bool isCorrectStanding = Vector2.Angle(colliderDistance.normal, Vector2.up) < 90.0f;

            // maybe should check if velocity on Y axis <= 0.0f;

            if (isCorrectStanding)
            {
                if (!IsGrounded)
                {
                    currentFighter.AnimationController.PlayJumpLandingAnimation();
                }

                IsGrounded = true;
                
                jumpCounter = 0;

                OnGroundedOnLegs?.Invoke();
            }
        }
    }


    private void OnPreAttack()
    {
        TryStop();
        TryFlip();

        void TryStop()
        {
            MovementBlokers++;

            Vector2 savedDirection = movingDirection;
            ForceStop();
            movingDirection = savedDirection;

            TaskInvoker.Instance.CallMethodWithDelay(this, () => MovementBlokers--, data.StopDurtionOnAttack);
        }

        void TryFlip()
        {
            if (currentFighter.CombatController.IsRange)
            {
                (float, Fighter) faceData = PhysicsManager.Instance.DistanceAndEnemyToFaceFighter(currentFighter);
                (float, Fighter) backData = PhysicsManager.Instance.DistanceAndEnemyToBackFighter(currentFighter);

                float projetileDistance = currentFighter.CombatController.DistanceForHit;
                bool isEnoughtDistanceToHitInBack = backData.Item2 != null && backData.Item1 < projetileDistance;
                bool isCloserToBackFighter = backData.Item2 != null &&
                                             ((faceData.Item2 != null &&
                                             Vector2.Distance(faceData.Item2.transform.position, currentFighter.transform.position) >
                                             Vector2.Distance(backData.Item2.transform.position, currentFighter.transform.position)) || faceData.Item2 == null);

                if (isEnoughtDistanceToHitInBack && isCloserToBackFighter)
                {
                    Side flipSide = LookingSide == Side.Right ? Side.Left : Side.Right;
                    Flip(flipSide);
                }
            }
        }
    }
    

    private void OnUpdate(float deltaTime)
    {
        //Vector2 prevDiration = 
        if (movingDirection != Vector2.zero) // dont need actually
        {
            Move(movingDirection);

            currentFighter.AnimationController.ChangeState(FighterAnimationController.State.Run);

            if (!IsSetDown && IsGrounded && AllowMovement)
            {
                OnRun?.Invoke();
            }
        }
    }


    public void SetSpeedFactor(float factor)
    {
        SpeedFactor = factor;

        mainRigidbody.velocity = new Vector2(mainRigidbody.velocity.x * SpeedFactor, mainRigidbody.velocity.y);
    }


    public void SetJumpSpeedFactor(float factor)
    {
        jumpSpeedFactor = factor;

        mainRigidbody.velocity = new Vector2(mainRigidbody.velocity.x, mainRigidbody.velocity.y * jumpSpeedFactor);
    }


    public void Charge()
    {
        if (AllowCharge)
        {
            if (chargeAbility != null && chargeAbility.IsCoolDownExpired && chargeAbility.IsChargeAvailable)
            {
                chargeAbility.SetCoolDown();

                if (chargeAbility.IsFamiliarCoolDownWithDoubleJump)
                {
                    ApplyDoubleJumpCooldown();
                }

                currentFighter.ControlController.ShowIcon(ControlType.ChargesBlock);

                TaskInvoker.Instance.CallMethodWithDelay(this, () =>
                {
                    currentFighter.ControlController.HideIcon(ControlType.ChargesBlock);
                }, chargeAbility.CoolDown);
                
                chargeAbility.Charge();
            }
        }
    }


    public void SeatDown()
    {
        if (IsGrounded)
        {
            ForceStop();

            ResizeToSeatCollider(currentFighter.MainCollider);
            ResizeToSeatCollider(currentFighter.KinematicCollider);

            MovementBlokers++;
            JumpBlokers++;
            IsSetDown = true;

            currentFighter.AnimationController.PlaySeatDownAnimation();
        }
    }


    public void StandUp()
    {
        if (IsSetDown)
        {
            ResizeFromSeatCollider(currentFighter.MainCollider);
            ResizeFromSeatCollider(currentFighter.KinematicCollider);

            MovementBlokers--;
            JumpBlokers--;
            IsSetDown = false;

            currentFighter.AnimationController.PlayStandUpAnimation();
        }
    }


    private void ResizeToSeatCollider(CapsuleCollider2D collider2D)
    {
        float colliderOffsetY = collider2D.size.y * data.PercentToReduceColliderOnSeatDown;

        collider2D.size *= data.PercentToReduceColliderOnSeatDown;
        collider2D.offset = new Vector2(collider2D.offset.x, collider2D.offset.y - (colliderOffsetY / 2.0f));
    }


    void ResizeFromSeatCollider(CapsuleCollider2D collider2D)
    {
        collider2D.size *= (1.0f / data.PercentToReduceColliderOnSeatDown);
        float colliderOffsetY = collider2D.size.y * data.PercentToReduceColliderOnSeatDown;

        collider2D.offset = new Vector2(collider2D.offset.x, collider2D.offset.y + (colliderOffsetY / 2.0f));
    }


    public void KnockOver(Vector3 hitPosition)
    {
        Vector3 direction = (currentFighter.transform.position - hitPosition).normalized;
        mainRigidbody.velocity = data.OverKnockMagnitude * direction;

        if (!IsKnockingOver)
        {
            StandUp();

            SetColliderBounciness(data.OverKnockBounciness);
            FlipCollider(currentFighter.MainCollider, initialMainColliderSize.Value, CapsuleDirection2D.Horizontal);
            FlipCollider(currentFighter.KinematicCollider, initialKinematicColliderSize.Value, CapsuleDirection2D.Horizontal);

            currentFighter.ControlController.ApplyControl(ControlType.Stun, float.MaxValue, this);

            OnGroundCollision += ReduceBounce;

            IsKnockingOver = true;

            currentFighter.AnimationController.PlayKnockOverAnimation();
            currentFighter.AnimationController.AllowChangeAnimation = false;
        }
    }


    private void ReduceBounce()
    {
        float bounciness = currentFighter.MainCollider.sharedMaterial.bounciness - data.OverKnockReduceBounciness;
        SetColliderBounciness(bounciness);

        if (currentFighter.MainCollider.sharedMaterial.bounciness <= 0.0f)
        {
            ResetKnockOver();

            currentFighter.AnimationController.AllowChangeAnimation = true;
            currentFighter.AnimationController.PlayKnockStandAnimation();
        }
    }


    private void ResetKnockOver()
    {
        SetColliderBounciness(0.0f);

        FlipCollider(currentFighter.MainCollider, initialMainColliderSize.Value, CapsuleDirection2D.Vertical);
        FlipCollider(currentFighter.KinematicCollider, initialKinematicColliderSize.Value, CapsuleDirection2D.Vertical);

        OnGroundCollision -= ReduceBounce;
        currentFighter.ControlController.FinishControl(this);
        IsKnockingOver = false;
    }


    private void SetColliderBounciness(float value)
    {
        currentFighter.MainCollider.sharedMaterial.bounciness = value;
        currentFighter.MainCollider.enabled = false;
        currentFighter.MainCollider.enabled = true;
    }

    
    private void FlipCollider(CapsuleCollider2D collider2D, Vector2 initialSize, CapsuleDirection2D direction2D)
    {
        if (collider2D.direction != direction2D)
        {
            collider2D.size = (direction2D == CapsuleDirection2D.Horizontal) ? initialSize.SetY(initialSize.y * 0.5f) : initialSize;

            collider2D.direction = direction2D;
        }

        float angle = collider2D.direction == CapsuleDirection2D.Horizontal ? 90.0f : 0.0f;
        //currentFighter.MainSpriteRenderer.transform.localEulerAngles = currentFighter.MainSpriteRenderer.transform.localEulerAngles.SetZ(angle);
    }


    public void MoveTo(Vector3 position)
    {
        Vector2 currentPosition = new Vector2(currentFighter.transform.position.x, currentFighter.transform.position.y);

        Vector2 direction = currentPosition.x > position.x ? Vector2.left : Vector2.right;
        Move(direction);
    }


    public bool IsLoockingInBack(Fighter anotherFighter)
    {
        bool result = LookingSide == anotherFighter.MovementController.LookingSide;

        if (LookingSide == Side.Right)
        {
            result &= MainRigidbody.position.x < anotherFighter.MovementController.MainRigidbody.position.x;
        }
        else
        {
            result &= MainRigidbody.position.x > anotherFighter.MovementController.MainRigidbody.position.x ;
        }
              
        return result;
    }
}
