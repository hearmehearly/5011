﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = System.Object;


public enum ControlType
{
    None            = 0,
    Stun            = 1,
    Slowdown        = 2,
    Paralysis       = 3,
    Silence         = 4,
    Disarm          = 5,
    Immunity        = 6,
    MobilityCuts    = 7,

    ChargesBlock    = 8,
    InputDisable = 9
}


[Serializable]
public class ControlValue
{
    public ControlType Type = default;
    public float Duration = default;
}

public class FighterControlController : FighterController
{
    [Serializable]
    public class IconData
    {
        public ControlType type = default;
        public SpriteRenderer sprite = default;
    }


    public class Data
    {
        public ControlType type = default;
        public float maxDuration = default;
        public float currentTimer = default;
        public IconData iconData = default;
        public Object handler = default;

        public bool ShouldEndControl => currentTimer >= maxDuration;

        public float ControlTimeLeft
        {
            get
            {
                float timeLeft = maxDuration - currentTimer;
                return timeLeft < 0.0f ? 0.0f : timeLeft;
            }
        }

        public void ShowIcon()
        {
            iconData?.sprite?.gameObject.SetActive(true);
        }


        public void HideIcon()
        {
            iconData?.sprite?.gameObject.SetActive(false);
        }
    }

    public enum State
    {
        None = 0,
        Free = 1,
        Stunned = 2,
        Hitted = 3
    }


    public Action<ControlType, object> OnControlStarted;
    public Action<ControlType, object> OnControlFinished;
    
    private readonly IconData[] iconDatas;
    
    private readonly List<Data> currentControlData = new List<Data>();


    public FighterControlController(IconData[] _iconDatas)
    {
        iconDatas = _iconDatas;
    }


    private void OnUpdate(float deltaTime)
    {
        for (int i = currentControlData.Count - 1; i >= 0; i--)
        {
            Data controlData = currentControlData[i];
            controlData.currentTimer += Time.deltaTime;

            if (controlData.iconData != null)
            {
                float scaleX = currentFighter.MovementController.VelocityMultiplier;
                controlData.iconData.sprite.transform.localScale = controlData.iconData.sprite.transform.localScale.SetX(scaleX);
            }

            if (controlData.ShouldEndControl)
            {
                int differentHandlersCount = currentControlData.FindAll(element => element.type == controlData.type).Count;

                currentControlData.RemoveAt(i);

                if (differentHandlersCount <= 1)
                {
                    controlData.HideIcon();
                    ControlType finishedtype = controlData.type;


                    OnControlFinished?.Invoke(controlData.type, controlData.handler);
                }
            }
        }

        //currentControlData.RemoveAll(element => element.ShouldEndControl); // this should be before callback
    }


    public override void Initialize(Fighter fighter)
    {
        base.Initialize(fighter);

        currentFighter = fighter;

        foreach (var iconData in iconDatas)
        {
            iconData.sprite.gameObject.SetActive(false);
        }


#warning hotfix
        foreach (var controlData in currentControlData)
        {
            controlData.currentTimer = controlData.maxDuration;
        }

        currentControlData.RemoveAll(element => true);

        Scheduler.OnUpdate += OnUpdate;
    }


    public override void Deinitialize()
    {
        foreach (var controlData in currentControlData)
        {
            controlData.currentTimer = controlData.maxDuration;
        }

        currentControlData.RemoveAll(element => true);
        Scheduler.OnUpdate -= OnUpdate;
    }


    public void ShowIcon(ControlType type)
    {
        var data = Array.Find(iconDatas, element => element.type == type);
        data.sprite.gameObject.SetActive(true);
    }


    public void HideIcon(ControlType type)
    {
        var data = Array.Find(iconDatas, element => element.type == type);
        data.sprite.gameObject.SetActive(false);
    }


    public bool IsControlActive(ControlType controlType)
    {
        return currentControlData.Find(element => element.type == controlType) != null;
    }


    public void ApplyControl(ControlValue control, Object handler)
    {
        ApplyControl(control.Type, control.Duration, handler);
    }


    public void ApplyControl(ControlType type, float duration, Object handler)
    {
        if (type == ControlType.None)
        {
            return;
        }

        bool isImmunityActive = !IsControlActive(ControlType.Immunity);

        if (isImmunityActive || type == ControlType.Immunity || type == ControlType.InputDisable)
        {
            bool wasControlActive = IsControlActive(type);
            //Debug.Log("Start " + type);
            StartControl(type, duration, handler);

            if (!wasControlActive)
            {
                OnControlStarted?.Invoke(type, handler);
            }
        }
    }


    public void ApplyDispel()
    {
        foreach (var controlData in currentControlData)
        {
            controlData.currentTimer = controlData.maxDuration;
        }
    }


    private void StartControl(ControlType controlType, float duration, Object handler)
    {
        Data data = currentControlData.Find(element => element.type == controlType);

        bool isControlWithSameHandlerExist = currentControlData.Exists(element => element.handler == handler);

        if (data == null || !isControlWithSameHandlerExist)
        {
            Data dataToAdd = new Data
            {
                type = controlType,
                maxDuration = duration,
                currentTimer = 0.0f,
                iconData = Array.Find(iconDatas, element => element.type == controlType),
                handler = handler
            };

            dataToAdd.ShowIcon();
            currentControlData.Add(dataToAdd);
        }
        else
        {
            data.maxDuration += duration;
        }
    }


    public void FinishControl(Object handler)
    {
        foreach (var controlData in currentControlData)
        {
            if (controlData.handler == handler ||
                (controlData.handler != null && controlData.handler.Equals(handler)))
            {
                controlData.currentTimer = controlData.maxDuration;
            }
        }
    }


    public void FinishControl(ControlType controlType, Object handler)
    {
        foreach (var controlData in currentControlData)
        {
            if (controlData.handler != null &&
                
                (controlData.handler == handler || controlData.handler.Equals(handler)) &&
                controlData.type == controlType)
            {
                controlData.currentTimer = controlData.maxDuration;
            }
        }
    }
}
