﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;


[Serializable]
public class DebuffValue
{
    public DebuffType Type = default;
    public float Duration = default;
    public float TotalDamage = default;
}


public enum DebuffType
{
    None            = 0,
    Bleed           = 1,
    ChargesBlock    = 2
}


public class FighterDebuffController : FighterController
{
    const float TICK_DAMAGE_SECOND = 1.0f;
    const float SHOW_DAMAGE_ICON_DURATION = 0.2f;

    [Serializable]
    public class IconData
    {
        public DebuffType type = default;
        public GameObject root = default;
        public TextMesh text = default;
    }


    public class Data
    {
        public DebuffType type = default;
        public DamageType damageType = DamageType.Simple;
        public float maxDuration = default;
        public float currentTimer = default;

        public float maxDamage = default;
        public float damagePerSecond = default;

        public float tickTimer = default;

        public IconData iconData = default;
        public Object handler = default;
        public Fighter fighterThatDealsDamage = default;



        public bool ShouldEndDebuff => currentTimer >= maxDuration;

        public float ControlTimeLeft
        {
            get
            {
                float timeLeft = maxDuration - currentTimer;
                return timeLeft < 0.0f ? 0.0f : timeLeft;
            }
        }

        public void ShowIcon(float damage)
        {
            iconData.text.text = damage.ToString();
            iconData.text.gameObject.SetActive(true);

            TaskInvoker.Instance.CallMethodWithDelay(this, () =>
            {
                if (iconData.text != null)
                {
                    iconData.text.gameObject?.SetActive(false);
                }
            }, SHOW_DAMAGE_ICON_DURATION);
        }


        public void ShowIcon()
        {
            iconData?.root?.gameObject?.SetActive(true);
        }


        public void HideIcon()
        {
            if (iconData != null &&
                iconData.root != null &&
                iconData.root.gameObject != null)
            {
                iconData.root.gameObject.SetActive(false);
            }
        }


        public void Deinitialize()
        {
            HideIcon();
            TaskInvoker.Instance.UnscheduleAllMethodForTarget(this);
        }
    }


    public Action<DebuffType> OnDebuffStarted;
    public Action<DebuffType> OnDebuffFinished;
    
    private readonly IconData[] iconData;
    
    private readonly List<Data> currentDebuffData = new List<Data>();



    public FighterDebuffController(IconData[] _iconData)
    {
        iconData = _iconData;
    }


    public override void Initialize(Fighter fighter)
    {
        base.Initialize(fighter);

        foreach (var iconData in iconData)
        {
            Debug.Log("No debuff icons");
            iconData.text.gameObject.SetActive(false);
        }
    }


    public override void Deinitialize()
    {
        foreach (var data in currentDebuffData)
        {
            data.HideIcon();
            data.Deinitialize();
        }

        currentDebuffData.Clear();
    }


    public bool IsDebuffActive(DebuffType type)
    {
        return currentDebuffData.Find(element => element.type == type) != null;
    }


    public void ShowIcon(DebuffType debuffType)
    {
        Data data = currentDebuffData.Find(element => element.type == debuffType);
        data?.ShowIcon();
    }


    public void HideIcon(DebuffType debuffType)
    {
        Data data = currentDebuffData.Find(element => element.type == debuffType);
        data?.HideIcon();
    }


    public void ApplyDebuff(DebuffValue debuff, Fighter fighter, Object handler = null)
    {
            StartDebuff(debuff.Type, debuff.Duration, debuff.TotalDamage, fighter, handler);
            OnDebuffStarted?.Invoke(debuff.Type);
    }


    private void StartDebuff(DebuffType debuffType, float duration, float totalDamage, Fighter fighter, Object handler)
    {
        Data data = currentDebuffData.Find(element => element.type == debuffType);

        bool isControlWithSameHandlerExist = currentDebuffData.Exists(element => element.handler == handler);

        if (data == null || !isControlWithSameHandlerExist)
        {
            float damagePerSecond = totalDamage / duration;

            Data dataToAdd = new Data
            {
                type = debuffType,
                maxDuration = duration,
                currentTimer = 0.0f,
                damagePerSecond = damagePerSecond,
                iconData = Array.Find(iconData, element => element.type == debuffType),
                fighterThatDealsDamage = fighter,
                handler = handler
            };

            currentDebuffData.Add(dataToAdd);
        }
        else
        {
            data.maxDuration += duration;
        }
    }


    public void FinishDebuff(Object handler)
    {
        foreach (var controlData in currentDebuffData)
        {
            if (controlData.handler == handler)
            {
                controlData.currentTimer = controlData.maxDuration;
            }
        }
    }


    public void ResetAllDebuffs()
    {
        foreach (var controlData in currentDebuffData)
        {
            controlData.currentTimer = controlData.maxDuration;
        }
    }


    private void Update()
    {
        for (int i = currentDebuffData.Count - 1; i >= 0; i--)
        {
            Data debuffData = currentDebuffData[i];
            debuffData.currentTimer += Time.deltaTime;

            float scaleX = currentFighter.LookingSide == Side.Left ? -1.0f : 1.0f;
            //debuffData.iconData.sprite.transform.localScale = new Vector3(scaleX,
            //                                                                debuffData.iconData.sprite.transform.localScale.y,
            //                                                                debuffData.iconData.sprite.transform.localScale.z);

            if (debuffData.ShouldEndDebuff)
            {
                int differentHandlersCount = currentDebuffData.FindAll(element => element.type == debuffData.type).Count;

                DebuffType finishedtype = debuffData.type;

                currentDebuffData[i].Deinitialize();
                currentDebuffData.RemoveAt(i);
                if (differentHandlersCount <= 1)
                {
                    debuffData.HideIcon();
                    OnDebuffFinished?.Invoke(debuffData.type);
                }
            }
            else
            {
                debuffData.tickTimer += Time.deltaTime;

                if (debuffData.tickTimer >= TICK_DAMAGE_SECOND)
                {
                    currentFighter.HealthController.ApplyDamage(debuffData.damagePerSecond,debuffData.damageType, debuffData.fighterThatDealsDamage);
                    debuffData.tickTimer = 0.0f;


                    debuffData.ShowIcon(debuffData.damagePerSecond);
                }
            }
        }
    }
}
