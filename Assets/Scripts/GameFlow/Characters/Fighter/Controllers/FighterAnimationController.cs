﻿using UnityEngine;
using DG.Tweening;
using Spine.Unity;
using Spine;
using System;


public class FighterAnimationController : FighterController
{
    public enum State
    {
        None        = 0,
        Stand       = 1,
        Run         = 2,
        Hit         = 3,
        Jump        = 4,
        SeatDown    = 5,
        KnockOver   = 6,
        Dead        = 7
    }

    private const int AnimationIndex = 0;
    private const int HitAnimationIndex = 1;
    private const int AdditionalAnimationIndex = 2;


    private FighterVisualData data;
    private SkeletonAnimation skeletonAnimation;
    private readonly BoneFollower attackBoneFollower;
    

    public bool AllowChangeAnimation { get; set; }
 
    private State CurrentState { get; set; }


    public FighterAnimationController(BoneFollower _attackBoneFollower)
    {
        attackBoneFollower = _attackBoneFollower;
    }
    

    public override void Initialize(Fighter fighter)
    {
        base.Initialize(fighter);

        skeletonAnimation = currentFighter.SkeletonAnimation;

        data = Fighters.GetVisualData(fighter.CurrentName);

        attackBoneFollower.SetBone(data.attackRoot);

        currentFighter.SkeletonAnimation.skeletonDataAsset = data.skeletonDataAsset;
        currentFighter.SkeletonAnimation.Initialize(true);

        SetGraphicsColor(1.0f);
        
        currentFighter.MovementController.OnRun += MovementController_OnRun;
        currentFighter.MovementController.OnFinishRun += MovementController_OnFinishRun;

        CurrentState = default;
        AllowChangeAnimation = true;
    }


    public override void Deinitialize()
    {
        currentFighter.MovementController.OnRun -= MovementController_OnRun;
        currentFighter.MovementController.OnFinishRun -= MovementController_OnFinishRun;
    }

    public void ChangeState(State value) =>
        CurrentState = CurrentState;


    public void PlayStandIdleAnimation()
    {
        if (CurrentState != State.Stand && CurrentState != State.Jump)
        {
            SetAnimation(data.idleAnimation);
            CurrentState = State.Stand;
        }
    }


    public void PlayRunAnimation()
    {
        if (CurrentState != State.Run && CurrentState != State.Dead)
        {
            SetAnimation(data.runAnimation);
            CurrentState = State.Run;
        }
    }

    public void PlaySeatDownAnimation()
    {
        if (CurrentState != State.SeatDown && CurrentState != State.Dead)
        {
            SetAnimation(data.seatAnimation, false);
            CurrentState = State.SeatDown;
        }
    }


    public void PlayStandUpAnimation()
    {
        CurrentState = State.None;


        SetAnimation(data.standUpAnimation, false, AnimationIndex, (_) => PlayStandIdleAnimation());
    }


    public void PlayKnockOverAnimation()
    {
        CurrentState = State.KnockOver;
        SetAnimation(data.knockOverAnimation, false);
    }

    public void PlayKnockStandAnimation()
    {
        CurrentState = State.None;
        SetAnimation(data.knockStandAnimation, false);
    }
    
    
    public void PlayHitAnimation(int combo) =>
        SetAnimation(data.GetHitAnimation(combo), false, HitAnimationIndex, (t) => ClearAnimations(HitAnimationIndex));
    

    public void PlayHitJumpAnimation() =>
        SetAnimation(data.RandomHitInJumpAnimation, false, HitAnimationIndex, (t) => ClearAnimations(HitAnimationIndex));
    

    public void PlayHitSeatAnimation() =>
        SetAnimation(data.RandomHitInSeatDownAnimation, false, HitAnimationIndex, (t) => ClearAnimations(HitAnimationIndex));
    

    public void PlayDeathAnimation(Action callback)
    {
        CurrentState = State.Dead;

        ClearAnimations(AnimationIndex);
        ClearAnimations(HitAnimationIndex);

        SetAnimation(data.deathAnimation, false, AnimationIndex, (t) =>
        {
            callback();
        });
    }


    public void PlayAbilityAnimation(string spineAnimation) =>
        PlayAbilityAnimation(spineAnimation, null);


    public void PlayAbilityAnimation(string spineAnimation, Action callback, bool loop = false, int index = HitAnimationIndex)
    {
        ClearAnimations(index);
        //ClearAnimations(AnimationIndex);

        SetAnimation(spineAnimation, loop, index, (t) =>
        {
            ClearAnimations(index);

            CurrentState = default;
            callback?.Invoke();
            // PlayStandIdleAnimation();
        });
    }


    public void SetHitAnimation(string animationName, bool loop = true)
    {
        SetAnimation(animationName, loop, HitAnimationIndex);
    }

    public void PlayJumpAnimation()
    {
       // if (State != AnimationState.Jump)
        {
            SetAnimation(data.jumpAnimation, false, AnimationIndex, (_) => PlayJumpIdleAnimation());
            CurrentState = State.Jump;
        }
    }
    

    private void PlayJumpIdleAnimation()
    {
        // if (State != AnimationState.Jump)
        {
            SetAnimation(data.jumpLoopAnimation, false);
            CurrentState = State.Jump;
        }
    }


    public void PlayJumpLandingAnimation()
    {
        if (!currentFighter.MovementController.IsKnockingOver)
        {
            ClearAnimations(AnimationIndex);
            SetAnimation(data.jumpEndAnimation, false, AnimationIndex, (_) =>
            {
                ClearAnimations(AnimationIndex);
                CurrentState = default;
                PlayStandIdleAnimation();
            });
            CurrentState = State.Jump;
        }
    }


    public void PlayAppearAnimation()
    {
        currentFighter.transform.localScale = Vector3.zero;
        currentFighter.transform.DOScale(Vector3.one, 2f);
    }


    public void PlayApplyHitAnimation()
    {
        data.hitFx.PlayOnce(currentFighter.transform.position);

        if (!currentFighter.MovementController.IsKnockingOver)
        {
            ClearAnimations(HitAnimationIndex);
            SetAnimation(data.takeDamageAnimation, false, HitAnimationIndex, (t) =>
            {
                ClearAnimations(HitAnimationIndex);
                PlayStandIdleAnimation();
            });
        }
    }


    public void SetGraphicsEnabled(bool enabled)
    {
        skeletonAnimation.GetComponent<MeshRenderer>().enabled = enabled;
    }


    public void SetSkeletonColor(Color color)
    {
        foreach (var slot in skeletonAnimation.Skeleton.Slots)
        {
            slot.SetColor(color);
        }
    }


    public void SetGraphicsColor(float value)
    {
        foreach (var spriteRenderer in currentFighter.Renderers)
        {
            spriteRenderer.color = spriteRenderer.color.SetA(value);
        }

        foreach (var slot in skeletonAnimation.Skeleton.Slots)
        {
            Color color = slot.GetColor().SetA(value);
            slot.SetColor(color);
        }
    }



    private void SetAnimation(string animationName, bool loop = true, int index = AnimationIndex, Action<TrackEntry> callback = null)
    {
        if (!AllowChangeAnimation)
        {
            return;
        }
       // Debug.Log($"anim {animationName} for {currentFighter}");

        TrackEntry trackEntry = currentFighter.SkeletonAnimation.AnimationState.SetAnimation(index, animationName, loop);
        Spine.AnimationState.TrackEntryDelegate h = null;

        h = (t) =>
        {
            trackEntry.Complete -= h;
            callback?.Invoke(t);
        };

        trackEntry.Complete += h;
    }


    public void ClearAnimations(int trackIndex)
    {
       // currentFighter.SkeletonAnimation.AnimationState.ClearTrack(trackIndex);
        currentFighter.SkeletonAnimation.AnimationState.SetEmptyAnimation(1, 0.1f);
        //currentFighter.SkeletonAnimation.Update(default);
    }



    private void MovementController_OnFinishRun()
    {
        if (!currentFighter.MovementController.IsSetDown)
        {
           // ClearAnimations(AnimationIndex);
            PlayStandIdleAnimation();
        }
    }

    private void MovementController_OnRun()
    {
        PlayRunAnimation();
    }
}
