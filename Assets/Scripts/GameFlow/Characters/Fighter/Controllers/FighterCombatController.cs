﻿using UnityEngine;
using System;


public class FighterCombatController : FighterController
{
    public event Action<FighterCombatData> OnAttack;
    public event Action OnPreAttack;

    /// <summary>
    ///   <para>OnHitFighter .</para>
    /// </summary>
    /// <param name="damagedFighter"></param>
    /// <param name="damage"></param>
    /// <param name="currentCombo"></param>
    public event Action<Fighter, float, int> OnHitFighter;
    public event Action<Fighter> OnBeforeHitFighter;
    public event Action<int> OnComboCounterChanged;
    public event Action<Fighter, Fighter> OnGottaKill; // this and to kill

    private FighterMeleeHitZone hitZone;
    private FighterMeleeHitZone jumpHitZone;
    private FighterMeleeHitZone seatDownHitZone;

    private Transform rangeTransform;


    private FighterCombatData data;

    private Coroutine resetComboCoroutine;

    private bool isCooldownForHitExpired = true;
    private int hitsPerJumpCount;

    private readonly Guid prepareHitBoxGuid = Guid.NewGuid();
    private readonly Guid disableHitBoxGuid = Guid.NewGuid();


    public int AttackBlokers { get; set; }

    public int ComboCounter { get; set; }

    public int DodgeCounter { get; set; }

    public bool IsAttackAvailable => AllowAttack && isCooldownForHitExpired;

    public bool AllowAttack => AttackBlokers == 0;


    public bool IsDodging => DodgeCounter > 0;


    public FighterCombatData.Type CombatType
    {
        get => data.CombatType;
        set => data.CombatType = value;
    }


    public bool IsMelee => CombatType == FighterCombatData.Type.Melee;


    public bool IsRange => CombatType == FighterCombatData.Type.Range;


    public Transform HitRoot { get; private set; }

    public float DistanceForHit => IsMelee ? (HitRoot.localPosition.x + (hitZone.HitCollider.size.x * 0.5f)) : data.BulletMaxDistance;


    public float DamageMultiplier { get; set; } = 1.0f; // usually it works as plus and minus from here. that will be fine.


    public float AdditionalDamage { get; private set; }

    public bool IsJumpAttackAvailable => (IsMelee && hitsPerJumpCount < data.MaxHitCountPerJump) || IsRange;

    // No methods to restrict cuz maybe will be helpful

    public FighterCombatController(FighterMeleeHitZone _hitZone,
                                   FighterMeleeHitZone _jumpHitZone,
                                   FighterMeleeHitZone _seatDownHitZone,
                                   Transform _hitRoot,
                                   Transform _rangeTransform)
    {
        hitZone = _hitZone;
        jumpHitZone = _jumpHitZone;
        seatDownHitZone = _seatDownHitZone;
        HitRoot = _hitRoot;
        rangeTransform = _rangeTransform;
    }


    public override void Initialize(Fighter fighter)
    {
        base.Initialize(fighter);
        data = UnityEngine.Object.Instantiate(Fighters.GetCombatData(currentFighter.CurrentName));

        currentFighter.MovementController.OnGroundedOnLegs += ResetHitPerJumpCount;

        jumpHitZone.HitCollider.offset = data.JumpDamageValue.hitColliderOffset;
        jumpHitZone.HitCollider.size = data.JumpDamageValue.hitColliderSize;

        seatDownHitZone.HitCollider.offset = data.SeatDownDamageValue.hitColliderOffset;
        seatDownHitZone.HitCollider.size = data.SeatDownDamageValue.hitColliderSize;

        jumpHitZone.DisableZone();
        seatDownHitZone.DisableZone();
        hitZone.DisableZone();

        AttackBlokers = 0;
        DodgeCounter = 0;
    }


    public override void Deinitialize()
    {
        Scheduler.StopPlayingCoroutine(ref resetComboCoroutine);

        currentFighter.MovementController.OnGroundedOnLegs -= ResetHitPerJumpCount;

        TaskInvoker.Instance.UnscheduleAllMethodForTarget(this);
        TaskInvoker.Instance.UnscheduleAllMethodForTarget(prepareHitBoxGuid);
        TaskInvoker.Instance.UnscheduleAllMethodForTarget(disableHitBoxGuid);
    }


    public void AllowApplyHits()
    {
        hitZone.OnShouldHitFighter += HitFighter;
        jumpHitZone.OnShouldHitFighter += JumpHitFighter;
        seatDownHitZone.OnShouldHitFighter += SeatDownHitFighter;
    }


    public void ForbidApplyHits()
    {
        hitZone.OnShouldHitFighter -= HitFighter;
        jumpHitZone.OnShouldHitFighter -= JumpHitFighter;
        seatDownHitZone.OnShouldHitFighter -= SeatDownHitFighter;
    }


    public void TriggerGottaKillEvent(Fighter fighterToKill)
    {
        Debug.Log($"trigger kill: owner {currentFighter.CurrentName}. killed: {fighterToKill.CurrentName}");
        currentFighter.KillsCounter++;

        OnGottaKill?.Invoke(currentFighter, fighterToKill);
    }


    private void HitFighter(Fighter fighter)
    {
        if (IsMelee)
        {
            IncrementComboCounter();
        }

        CompleteHit(fighter, data, ComboCounter, currentFighter.MovementController.VelocityVectorMultiplier);

        if (IsMelee && ComboCounter >= data.MaxComboCount)
        {
            ResetComboCounter();
        }
    }


    private void JumpHitFighter(Fighter fighter)
    {
        HitFighterFromDamageData(fighter, data.JumpDamageValue, true, data.JumpHitMove);
    }


    private void SeatDownHitFighter(Fighter fighter)
    {
        HitFighterFromDamageData(fighter, data.SeatDownDamageValue, true, data.SeatDownHitMove);
    }


    private void RangeHitFighter(Fighter fighter, int projectileCombo, Vector3 impulsMultiplier)
    {
        if (fighter == null || this == null)
        {
            return;
        }

        CompleteHit(fighter, data, projectileCombo, impulsMultiplier);
    }


    private void CompleteHit(Fighter fighterToHit, FighterCombatData data, int combo, Vector3 impulsMultiplier, bool shouldOveriHitImpult = false, Vector3 overridedHitMoveImpuls = default)
    {
        OnBeforeHitFighter?.Invoke(fighterToHit);
        //Debug.Log("<color=red>HitFighter COMBO: " + projectileCombo + "</color>");

        float damage = (data.GetDamage(combo) + AdditionalDamage) * DamageMultiplier;
        Vector2 impuls = data.GetHitImpuls(combo) * impulsMultiplier;

        fighterToHit.ApplyHit(data, damage, DamageType.Simple, impuls, data.StunDuration(combo), data.IsKnockOver(combo), currentFighter);

        Vector3 moveImpuls = default;

        if (shouldOveriHitImpult)
        {
            moveImpuls = overridedHitMoveImpuls;
        }
        else
        {
            if (IsMelee)
            {
                moveImpuls = data.FindHitMove(ComboCounter);
            }
        }

        currentFighter.MovementController.AddRelativeToSideForce(moveImpuls);

        OnHitFighter?.Invoke(fighterToHit, damage, ComboCounter);

        AudioManager.Instance.Play(IngameData.asset.Value.Settings.audioSettings.RandomEmotionClip);
    }

#warning DUPLICATE LOWER
    private void HitFighterFromDamageData(Fighter fighterToHit, DataDamageValue damageData, bool shouldOveriHitImpult = false, Vector3 overridedHitMoveImpuls = default)
    {
        float damage = (damageData.GetDamage() + AdditionalDamage) * DamageMultiplier;
        Vector2 impuls = damageData.HitImpuls * currentFighter.MovementController.VelocityVectorMultiplier;
        float stunDuration = damageData.StunDurationPerHit;

        fighterToHit.ApplyHit(data, damage, DamageType.Simple, impuls, stunDuration, damageData.ShouldKnockOver, currentFighter);

        Vector3 moveImpuls = default;

        if (shouldOveriHitImpult)
        {
            moveImpuls = overridedHitMoveImpuls;
        }
        else
        {
            if (IsMelee)
            {
                moveImpuls = data.FindHitMove(ComboCounter);
            }
        }

        currentFighter.MovementController.AddRelativeToSideForce(moveImpuls);
        OnHitFighter?.Invoke(fighterToHit, damage, ComboCounter);

        AudioManager.Instance.Play(IngameData.asset.Value.Settings.audioSettings.RandomEmotionClip);
    }


#warning DUPLICATE UPPER
    public void Attack()
    {
        if (IsAttackAvailable)
        {
            bool isAnyAttackAvailable = currentFighter.MovementController.IsGrounded || IsJumpAttackAvailable;

            if (!isAnyAttackAvailable)
            {
                return;
            }

            OnPreAttack?.Invoke();

            if (currentFighter.MovementController.IsGrounded)
            {
                if (currentFighter.MovementController.IsSetDown)
                {
                    SeatDownAttack();
                }
                else
                {
                    GroundAttack();
                }
            }
            else if (IsJumpAttackAvailable)
            {
                JumpAttack();
            }

            if (IsRange)
            {
                Vector2 hitMove = data.FindHitMove(ComboCounter);
                currentFighter.MovementController.AddRelativeToSideForce(hitMove);
            }

            float controlDuration = data.FindInputDisableOnHitDuration(ComboCounter);
            currentFighter.ControlController.ApplyControl(ControlType.InputDisable, controlDuration, this);

            OnAttack?.Invoke(data);

            AudioManager.Instance.Play(IngameData.asset.Value.Settings.audioSettings.RandomHitClip);
        }
    }


    private void GroundAttack()
    {
        Scheduler.StopPlayingCoroutine(ref resetComboCoroutine);

        switch (CombatType)
        {
            case FighterCombatData.Type.Melee:
                MakeMeleeAttack();
                break;

            case FighterCombatData.Type.Range:
                MakeRangeAttack();
                break;

            default:
                break;
        }

        isCooldownForHitExpired = false;
        Scheduler.PlayMethodWithDelay(() => isCooldownForHitExpired = true, data.DelayBetweenHits);

        resetComboCoroutine = Scheduler.PlayMethodWithDelay(ResetComboCounter, data.MaxDelayBetweenCombo);
    }


    private void JumpAttack()
    {
        switch (CombatType)
        {
            case FighterCombatData.Type.Melee:

                TaskInvoker.Instance.CallMethodWithDelay(prepareHitBoxGuid, () => EnableHitZone(jumpHitZone), data.JumpHitDelay);
                currentFighter.HealthController.OnApplyHit += OnApplyHitWhileAttacking;
                
                jumpHitZone.OnShouldHitFighter += ProceedJumpHitZone;

                TaskInvoker.Instance.CallMethodWithDelay(disableHitBoxGuid, () =>
                {
                    jumpHitZone.DisableZone();
                    jumpHitZone.OnShouldHitFighter -= ProceedJumpHitZone;
                }, data.JumpHitDelay + data.JumpHitDuration);
                break;

            case FighterCombatData.Type.Range:
                MakeRangeAttack();

                isCooldownForHitExpired = false;
                Scheduler.PlayMethodWithDelay(() => isCooldownForHitExpired = true, data.DelayBetweenHits);

                break;

            default:
                break;
        }

        Vector2 savedVelocity = currentFighter.MovementController.MainRigidbody.velocity;
        currentFighter.MovementController.MainRigidbody.isKinematic = true;
        currentFighter.MovementController.MainRigidbody.velocity = Vector2.zero;

        TaskInvoker.Instance.CallMethodWithDelay(this, () =>
        {
            //  currentFighter.MovementController.MainRigidbody.velocity = savedVelocity;
            currentFighter.MovementController.MainRigidbody.isKinematic = false;
        }, data.LevitationDuration);

        hitsPerJumpCount++;
    }


    private void SeatDownAttack()
    {
        switch (CombatType)
        {
            case FighterCombatData.Type.Melee:
                TaskInvoker.Instance.CallMethodWithDelay(prepareHitBoxGuid, () => EnableHitZone(seatDownHitZone), data.SeatDownHitDelay);
                currentFighter.HealthController.OnApplyHit += OnApplyHitWhileAttacking;
                TaskInvoker.Instance.CallMethodWithDelay(disableHitBoxGuid, () => seatDownHitZone.DisableZone(), data.SeatDownHitDelay + data.SeatDownHitDuration);
                break;

            case FighterCombatData.Type.Range:
                MakeRangeAttack();

                isCooldownForHitExpired = false;
                Scheduler.PlayMethodWithDelay(() => isCooldownForHitExpired = true, data.DelayBetweenHits);
                break;

            default:
                break;
        }
    }


    private void MakeMeleeAttack()
    {
        hitZone.HitCollider.offset = data.GetHitColliderOffset(ComboCounter + 1);
        hitZone.HitCollider.size = data.GetHitColliderSize(ComboCounter + 1);

        hitZone.OnShouldHitFighter += ProcessMeleeHitEvent;
        currentFighter.HealthController.OnApplyHit += OnApplyHitWhileAttacking;

        float hitDelay = data.FindHitBoxAppearDelay(ComboCounter + 1);
        TaskInvoker.Instance.CallMethodWithDelay(prepareHitBoxGuid, () => EnableHitZone(hitZone), hitDelay);

        TaskInvoker.Instance.CallMethodWithDelay(disableHitBoxGuid, () =>
        {
            hitZone.OnShouldHitFighter -= ProcessMeleeHitEvent;
            hitZone.DisableZone();

            if (!hitZone.WasHitOnLastEnable)
            {
                ResetComboCounter();
            }
        }, hitDelay + data.HitDuration);
    }


    private void EnableHitZone(FighterMeleeHitZone zone)
    {
        DevContent.CreateVisualHitBox(currentFighter, zone);
        zone.EnableZone();
    }


    private void OnApplyHitWhileAttacking(float health)
    {
        TaskInvoker.Instance.UnscheduleAllMethodForTarget(prepareHitBoxGuid);
        TaskInvoker.Instance.UnscheduleAllMethodForTarget(disableHitBoxGuid); 

        currentFighter.HealthController.OnApplyHit -= OnApplyHitWhileAttacking;
        jumpHitZone.OnShouldHitFighter -= ProceedJumpHitZone;

        hitZone.OnShouldHitFighter -= ProcessMeleeHitEvent;

        hitZone.DisableZone();
        jumpHitZone.DisableZone();
        seatDownHitZone.DisableZone();
    }


    private void MakeRangeAttack()
    {
        float delay = data.FindHitBoxAppearDelay(ComboCounter);

        TaskInvoker.Instance.CallMethodWithDelay(prepareHitBoxGuid, () =>
        {
            IncrementComboCounter();

            FighterBullet bullet = UnityEngine.Object.Instantiate(data.FighterBullet, rangeTransform.position, Quaternion.identity);
            bullet.Initialize(ComboCounter, data.BulletMaxDistance);

            bullet.SetVelocity(Vector2.right * data.BulletSpeed * currentFighter.transform.localScale.x);

            bullet.OnShouldHitFighter += RangeHitFighter;
            bullet.EnableZone();

            Physics2D.IgnoreCollision(bullet.HitCollider, currentFighter.gameObject.GetComponent<Collider2D>());

            if (IsRange && ComboCounter >= data.MaxComboCount)
            {
                ResetComboCounter();
            }
        }, delay);
    }


    private void ProcessMeleeHitEvent(Fighter fighter)
    {
        hitZone.OnShouldHitFighter -= ProcessMeleeHitEvent;
        //IncrementComboCounter();
    }


    private void IncrementComboCounter()
    {
        bool isMeleeInJump = !currentFighter.MovementController.IsGrounded &&
            currentFighter.CombatController.CombatType == FighterCombatData.Type.Melee;

        if (isMeleeInJump)
        {
            return;
        }

        ComboCounter++;
        
        OnComboCounterChanged?.Invoke(ComboCounter);
    }


    private void ResetComboCounter()
    {
        ComboCounter = 0;
        OnComboCounterChanged?.Invoke(ComboCounter);
    }


    private void ResetHitPerJumpCount()
    {
        hitsPerJumpCount = 0;
    }


    public void AddAdditionalDamage(float value)
    {
        AdditionalDamage += value;
    }


    public void RemoveAdditionalDamage(float value)
    {
        AdditionalDamage = (AdditionalDamage - value < 0) ? 0 : (AdditionalDamage - value);
    }


    public void SetNewDamageValues(DataDamageValue[] damageValues)
    {
        data.DamageValues = damageValues;
    }


    private void ProceedJumpHitZone(Fighter obj)
    {
        jumpHitZone.OnShouldHitFighter -= ProceedJumpHitZone;
        jumpHitZone.DisableZone();
    }
}
