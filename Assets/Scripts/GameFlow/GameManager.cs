﻿using UnityEngine;


public class GameManager : MonoBehaviour
{
    [SerializeField] private Camera gameCamera = null;
    [SerializeField] private Arena arena = null;
    [SerializeField] private TutorialArena tutorialArena = null;

    private SceneArena sceneArena;
    private ScenePlayer scenePlayer;
    
    public static GameManager Instance
    {
        get;
        private set;
    }
    
    public GamePause GamePause { get; private set; }
    public Camera GameCamera => gameCamera;



    private void Awake()
    {
        Instance = this;
        Input.multiTouchEnabled = true;
    }


    private void Start()
    {
        sceneArena = GetComponent<SceneArena>();
        scenePlayer = GetComponent<ScenePlayer>();

        GamePause = new GamePause();

        arena.Initialize();
        tutorialArena.Initialize();

        StartGame();
    }



    private void StartGame()
    {
        DevContent.SetEnabled(false);

        AudioManager.Instance.Initialize();
        UnityAds.Initialize();
        TutorialManager.Initialize();

        //PlayArena();
        ShowScenePlayer();
    }


    private void PlayArena(UIPlayerMenu.Result result)
    {
        IArena arenaToPlay = TutorialManager.ShouldPlayTutorial ? (IArena)tutorialArena : arena;
        if (arenaToPlay != tutorialArena) tutorialArena.HideBack();

        sceneArena.Play(arenaToPlay, result, () => ShowScenePlayer());
    }


    private void ShowScenePlayer()
    {
        scenePlayer.Show((result) => PlayArena(result));
    }
}