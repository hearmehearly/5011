﻿using UnityEngine;


public static class CommonExtensions
{
    public static Vector2 SetX(this Vector2 vector, float value)
    {
        vector.x = value;
        return vector;
    }

    public static Vector2 SetY(this Vector2 vector, float value)
    {
        vector.y = value;
        return vector;
    }


    public static Vector3 SetX(this Vector3 vector, float value)
    {
        vector.x = value;
        return vector;
    }

    public static Vector3 SetY(this Vector3 vector, float value)
    {
        vector.y = value;
        return vector;
    }


    public static Vector3 SetZ(this Vector3 vector, float value)
    {
        vector.z = value;
        return vector;
    }


    public static Color SetA(this Color color, float value)
    {
        color.a = value;
        return color;
    }


    public static float Clamp0360(this float eulerAngles)
    {
        float result = eulerAngles - Mathf.CeilToInt(eulerAngles / 360f) * 360f;
        if (result < 0)
        {
            result += 360f;
        }

        return result;
    }


    public static bool IsAngleBetween(float angleToCheck, float angle1, float angle2)
    {
        float smallestAngle = Mathf.Abs(Mathf.DeltaAngle(angle1, angle2));

        float angle1Delta = Mathf.Abs(Mathf.DeltaAngle(angleToCheck, angle1));
        float angle2Delta = Mathf.Abs(Mathf.DeltaAngle(angleToCheck, angle2));

        // return TRUE if angleToCheck is between the SMALLEST range between angle1 and angle2).

        return ((angle1Delta + angle2Delta) <= smallestAngle);
    }
}
