﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class GamePause
{
    public static event Action<bool> OnGamePausedChange;

    private readonly List<MonoBehaviour> pauseSenders = new List<MonoBehaviour>();

    private bool isGamePaused;
    private float timeScaleBeforePause;

    public bool IsGamePaused
    {
        get => isGamePaused;

        private set
        {
            if (isGamePaused != value)
            {
                isGamePaused = value;

                timeScaleBeforePause = isGamePaused ? Time.timeScale : timeScaleBeforePause;
                Time.timeScale = isGamePaused ? 0.0f : timeScaleBeforePause;

                OnGamePausedChange?.Invoke(isGamePaused);
            }
        }
    }


    public void SetGamePaused(bool isPaused, MonoBehaviour pauseSender)
    {
        if (isPaused)
        {
            if (!pauseSenders.Contains(pauseSender))
            {
                pauseSenders.Add(pauseSender);
            }
        }
        else
        {
            pauseSenders.Remove(pauseSender);
        }

        IsGamePaused = (pauseSenders.Count != 0);
    }
}
