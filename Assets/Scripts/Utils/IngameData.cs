﻿using System;
using UnityEngine;


[CreateAssetMenu]
public class IngameData : ScriptableObject
{
    [Serializable]
    public class SettingsData
    {
        public UiAbilitiesSettings FighterVisualData = default;
        public RunesSettings RunesSettings = default;
        public DevContentSettings devContentSettings = default;
        public TutorialSettings tutorialSettings = default;
        public AudioSettings audioSettings = default;

        public ArenaFightersController.Settings FightersControllerSettings = default;
    }

    public static readonly ResourceAsset<IngameData> asset = new ResourceAsset<IngameData>("Game/IngameData");
    

    public SettingsData Settings = default;
}