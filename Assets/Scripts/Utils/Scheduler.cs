﻿using System.Collections;
using UnityEngine;
using System;


public class Scheduler : MonoBehaviour
{
    public static event Action OnGameStart;
    public static event Action OnGameQuit;
    public static event Action<float> OnUpdate;
    public static event Action<float> OnFixedUpdate;

    static Scheduler instance;
    

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void Initialize()
    {
        GameObject go = new GameObject("Monobehavior Sheduler");
        instance = go.AddComponent<Scheduler>();
        DontDestroyOnLoad(go);
    }

    public static Coroutine PlayCoroutine(IEnumerator routine)
    {
#warning sometimes bugs have to search?
        // if (routine == null)
        {
            return instance.StartCoroutine(routine);
        }
    }


    public static void StopPlayingCoroutine(ref Coroutine routine)
    {
        if (routine != null)
        {
            instance.StopCoroutine(routine);
            routine = null;
        }
    }


    public static Coroutine PlayMethodWithDelay(Action method, float delay) =>
        instance.StartCoroutine(instance.InvokeMethodWithDelay(delay, method));


    public static void RepeatInvoke(string method, float intervals, float delay = 0.0f)
    {
       instance.InvokeRepeating(method, delay,  intervals);
    }
    

    private void Awake()
    {
        OnGameStart?.Invoke();
    }


    private void Update()
    {
        OnUpdate?.Invoke(Time.deltaTime);
    }


    private void FixedUpdate()
    {
        OnFixedUpdate?.Invoke(Time.fixedDeltaTime);
    }


    private void OnApplicationQuit()
    {
        OnGameQuit?.Invoke();
    }


    private IEnumerator InvokeMethodWithDelay(float delay, Action method)
    {
        yield return new WaitForSeconds(delay);
        method?.Invoke();
    }
}
