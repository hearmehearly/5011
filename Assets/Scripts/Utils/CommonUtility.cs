﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

#if UNITY_EDITOR
    using UnityEditor;
#endif


namespace Fighter5011
{
    public static class CommonUtility
    {
        #region Variables

        public const float OneFrameDelay = 0.0f;

        public static int[] DEFAULT_TRIANGLE_INDEXES = { 0, 1, 2 };

        #endregion



        #region Public methods

        public static Rect CalculateGameZoneRect(Camera camera, float gameZoneSizeMultiplier = 1.0f)
        {
            Camera gameCamera = camera;
            float cameraHalfWidth = gameCamera.orthographicSize * gameCamera.aspect * gameZoneSizeMultiplier;
            float cameraHalfHeight = gameCamera.orthographicSize * gameZoneSizeMultiplier;

            Vector2 cameraLowAnchor = new Vector2(gameCamera.transform.position.x - cameraHalfWidth, gameCamera.transform.position.y - cameraHalfHeight);
            Vector2 cameraSize = new Vector2(cameraHalfWidth * 2.0f, cameraHalfHeight * 2.0f);

            return new Rect(cameraLowAnchor, cameraSize);
        }


        public static void Clear(this Array array)
        {
            if (array == null)
            {
                return;
            }

            Array.Clear(array, 0, array.Length);
        }
        

        public static bool SolveQuadraticEquation(float a, float b, float c, out float minRoot, out float maxRoot)
        {
            bool result = false;
            minRoot = -1f;
            maxRoot = -1f;

            if (Mathf.Approximately(a, 0f))
            {
                if (!Mathf.Approximately(b, 0f))
                {
                    result = true;

                    minRoot = -c / b;
                    maxRoot = minRoot;
                }
            }
            else
            {
                float discriminant = b * b - 4 * a * c;
                if (discriminant >= 0f)
                {
                    result = true;
                    discriminant = Mathf.Sqrt(discriminant);

                    float firstRoot = (-b - discriminant) * 0.5f / a;
                    float secondRoot = (-b + discriminant) * 0.5f / a;

                    if (firstRoot > secondRoot)
                    {
                        maxRoot = firstRoot;
                        minRoot = secondRoot;
                    }
                    else
                    {
                        maxRoot = secondRoot;
                        minRoot = firstRoot;
                    }
                }
            }

            return result;
        }


        public static void Clear(this StringBuilder sb)
        {
            if (sb.Length > 0)
            {
                sb.Remove(0, sb.Length);
            }
        }


        public static void SetObjectActive(GameObject go, bool active)
        {
            if (active != go.activeSelf)
            {
                go.SetActive(active);
            }
        }


        public static IEnumerator CallInEndOfFrame(Action callback)
        {
            yield return new WaitForEndOfFrame();

            callback?.Invoke();
        }


        public static Vector2 Rotate(this Vector2 point, Vector2 anchor, float angle)
        {
            Vector2 result;
            point -= anchor;

            float cos = Mathf.Cos(angle * Mathf.Deg2Rad);
            float sin = Mathf.Sin(angle * Mathf.Deg2Rad);
            result.x = (cos * point.x) - (sin * point.y);
            result.y = (sin * point.x) + (cos * point.y);

            return result + anchor;
        }


        public static float NormalizeAngle(float angle)
        {
            if (angle < 0f || angle >= 360f)
            {
                angle -= ((int)(angle / 360f)) * 360f;

                if (angle < 0f)
                {
                    angle += 360f;
                }
            }

            return angle;
        }


        public static bool IsAngleBetweenAngles(float targetAngle, float minAngle, float maxAngle)
        {
            targetAngle = NormalizeAngle(targetAngle);
            minAngle = NormalizeAngle(minAngle);
            maxAngle = NormalizeAngle(maxAngle);

            if (maxAngle < minAngle)
            {
                maxAngle += 360f;
            }
            if (targetAngle < minAngle)
            {
                targetAngle += 360f;
            }

            return (minAngle <= targetAngle && targetAngle <= maxAngle);
        }        


        public static bool IsLeftPoint(Vector2 targetPoint, Vector2 firstPoint, Vector2 secondPoint) // from given vector to vector with start point of out v and end with target point
        {
            Vector2 v = secondPoint - firstPoint;
            Vector2 vt = targetPoint - firstPoint;

            float va = Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
            float vta = Mathf.Atan2(vt.y, vt.x) * Mathf.Rad2Deg;

            float angle = vta - va;

            if (angle < 0f)
            {
                angle += 360f;
            }

            bool isConvex = (angle <= 180f);

            return isConvex;
        }


        


        public static T Find<T>(this T[] array, Func<T, bool> predicate)
        {
            T result = default(T);
            for (int i = 0, n = array.Length; i < n; i++)
            {
                if (predicate(array[i]))
                {
                    result = array[i];
                    break;
                }
            }

            return result;
        }


        public static bool Contains<T>(this T[] array, Func<T, bool> predicate)
        {
            bool result = false;
            for (int i = 0, n = array.Length; i < n; i++)
            {
                if (predicate(array[i]))
                {
                    result = true;
                    break;
                }
            }

            return result;
        }


        public static bool Contains<T>(this List<T> list, Func<T, bool> predicate)
        {
            bool result = false;
            for (int i = 0, n = list.Count; i < n; i++)
            {
                if (predicate(list[i]))
                {
                    result = true;
                    break;
                }
            }

            return result;
        }


        public static bool Same(this Vector3 thisVector, Vector2 vec2, float precision = 0.05f)
        {
            return Vector2.SqrMagnitude((Vector2)thisVector - vec2) <= precision * precision;
        }


        public static float RoundToValue(float sourceValue, float roundedValue)
        {
            return Mathf.Ceil(sourceValue / roundedValue) * roundedValue;
        }


        public static float ColliderOffset(Collider2D sourceCollider)
        {
            return Mathf.Max(sourceCollider.bounds.size.x, sourceCollider.bounds.size.x);
        }

        public static Vector3 CircumCirclePoint(Vector3 v0, Vector3 v1, Vector3 v2)
        {
            Vector3 result = Vector3.zero;

            float dx1 = v1.x - v0.x; float dy1 = v1.y - v0.y;
            if (Mathf.Approximately(dy1, 0f))
            {
                dy1 = float.Epsilon;
            }
            float angle1 = Mathf.Atan2(dy1, dx1) * Mathf.Rad2Deg;

            float dx2 = v2.x - v0.x; float dy2 = v2.y - v0.y;

            if (Mathf.Approximately(dy2, 0f))
            {
                dy2 = float.Epsilon;
            }

            float angle2 = Mathf.Atan2(dy2, dx2) * Mathf.Rad2Deg;

            float angleBis1 = (angle1 + angle2) * 0.5f;
            float kBis1 = Mathf.Tan(angleBis1 * Mathf.Deg2Rad);
            float yBis1 = v0.y - kBis1 * v0.x;


            float dx3 = v1.x - v2.x;
            float dy3 = v1.y - v2.y;

            if (Mathf.Approximately(dy3, 0f))
            {
                dy3 = float.Epsilon;
            }

            float angle3 = Mathf.Atan2(dy3, dx3) * Mathf.Rad2Deg;
            float angle3Bis = (angle1 + angle3) * 0.5f;
            float kBis3 = Mathf.Tan(angle3Bis * Mathf.Deg2Rad);
            float yBis3 = v1.y - kBis3 * v1.x;

            float x = (yBis3 - yBis1) / (kBis1 - kBis3);
            float y = kBis1 * x + yBis1;

            result = new Vector3(x, y, v0.z);

            return result;
        }


        public static string ToTotalMMSS(this TimeSpan timeSpan, string mask = "{0:D2}:{1:D2}")
        {
            string result = timeSpan.TotalSeconds > 0 ?
                string.Format(mask, (int)timeSpan.TotalMinutes, timeSpan.Seconds) :
                string.Format(mask, 0, 0);

            return result;
        }


        public static string ToTotalHHMMSS(this TimeSpan timeSpan, string mask = "{0:D2}:{1:D2}:{2:D2}")
        {
            string result = timeSpan.TotalSeconds > 0 ?
                string.Format(mask, (int)timeSpan.TotalHours, timeSpan.Minutes, timeSpan.Seconds) :
                string.Format(mask, 0, 0, 0);

            return result;
        }


        public static string ToTotalHHMM(this TimeSpan timeSpan, string mask = "{0:D2}:{1:D2}")
        {
            string result = timeSpan.TotalSeconds > 0 ?
                string.Format(mask, (int)timeSpan.TotalHours, timeSpan.Minutes) :
                string.Format(mask, 0, 0);

            return result;
        }


        public static void DrawCircle(Vector3 centerPostition, float radius, int segmentsCount, Color color, bool shouldDrawRadius = true, float duration = -1f)
        {
            Vector3[] result = new Vector3[segmentsCount];
            float radianPerSegment = Mathf.Deg2Rad * 360f / segmentsCount;

            for (int i = 0; i < segmentsCount; i++)
            {
                float rad = i * radianPerSegment;
                result[i] = new Vector3(Mathf.Sin(rad) * radius, Mathf.Cos(rad) * radius);
            }

            for (int i = 0; i < segmentsCount; i++)
            {
                if (duration <= 0f)
                {
                    Debug.DrawLine(centerPostition + result[i], centerPostition + result[(i + 1) % segmentsCount], color);
                }
                else
                {
                    Debug.DrawLine(centerPostition + result[i], centerPostition + result[(i + 1) % segmentsCount], color, duration);
                }
            }

            if (shouldDrawRadius)
            {
                if (duration <= 0f)
                {
                    Debug.DrawLine(centerPostition, centerPostition + result[0], color);
                }
                else
                {
                    Debug.DrawLine(centerPostition, centerPostition + result[0], color, duration);
                }
            }
        }


        public static void DrawArrow(Vector3 from, Vector3 to, Color color, float arrowHeadAngle = 10f, float arrowHeadLength = 5f)
        {
            float angle = Vector2.SignedAngle(Vector2.left, to - from) - 45f;
            Vector2 direction1 = Quaternion.Euler(0f, 0f, angle + arrowHeadAngle) * Vector2.one;
            Vector2 direction2 = Quaternion.Euler(0f, 0f, angle - arrowHeadAngle) * Vector2.one;

            Debug.DrawLine(from, to, color);
            Debug.DrawRay(to, direction1 * arrowHeadLength, color);
            Debug.DrawRay(to, direction2 * arrowHeadLength, color);
        }


        public static void DrawCross(Vector3 position, float size, float duration, Color color)
        {
            Debug.DrawLine(position + new Vector3(-size, -size), position + new Vector3(size, size), color, duration);
            Debug.DrawLine(position + new Vector3(-size, size), position + new Vector3(size, -size), color, duration);
        }


        public static string HierarchyPath(GameObject gameObject)
        {
            StringBuilder result = new StringBuilder();

            if (gameObject != null)
            {
                Transform parent = gameObject.transform.parent;
                result.Append(gameObject.name);

                while (parent != null)
                {
                    result.Append($"<{parent.name}");
                    parent = parent.transform.parent;
                }
            }
            else
            {
                result.Append("null");
            }

            return result.ToString();
        }


        public static void RemoveAllComponents<T>(this GameObject go) where T : MonoBehaviour
        {
            if (go != null)
            {
                T[] array = go.GetComponents<T>();

                for (int i = 0; i < array.Length; i++)
                {
                    GameObject.Destroy(array[i]);
                }
            }
        }


        public static void RemoveComponent<T>(this GameObject go) where T : MonoBehaviour
        {
            if (go != null)
            {
                T component = go.GetComponent<T>();

                if (component != null)
                {
                    GameObject.Destroy(component);
                }
            }
        }

        public static float Distance2D(Vector3 a, Vector3 b)
        {
            return Vector2.Distance(a, b);
        }


        public static Vector3 CalculateCentralPoint(MonoBehaviour[] monoBehaviours)
        {
            Vector3 result = Vector3.zero;

            if (monoBehaviours != null && monoBehaviours.Length > 0)
            {
                foreach (var child in monoBehaviours)
                {
                    result += child.transform.position;
                }
                result /= monoBehaviours.Length;
            }

            return result;
        }
        
        
        public static float CalculateDistanceFromPointToSegment(Vector2 point, Vector2 leftSegmentPoint, Vector2 rightSegmentPoint)
        {
            float dx = rightSegmentPoint.x - leftSegmentPoint.x;
            float dy = rightSegmentPoint.y - leftSegmentPoint.y;
            if (Mathf.Approximately(dx, 0.0f) && Mathf.Approximately(dy, 0.0f))
            {
                dx = point.x - leftSegmentPoint.x;
                dy = point.y - leftSegmentPoint.y;
                return Mathf.Sqrt(dx * dx + dy * dy);
            }

            float t = ((point.x - leftSegmentPoint.x) * dx + (point.y - leftSegmentPoint.y) * dy) / (dx * dx + dy * dy);

            if (t < 0)
            {
                dx = point.x - leftSegmentPoint.x;
                dy = point.y - leftSegmentPoint.y;
            }
            else if (t > 1)
            {
                dx = point.x - rightSegmentPoint.x;
                dy = point.y - rightSegmentPoint.y;
            }
            else
            {
                Vector2 closest = new Vector2(leftSegmentPoint.x + t * dx, leftSegmentPoint.y + t * dy);
                dx = point.x - closest.x;
                dy = point.y - closest.y;
            }

            return Mathf.Sqrt(dx * dx + dy * dy);
        }


        public static bool IsOutOfBounds(Transform checkTransform, Bounds checkBounds, bool shouldCheckZ)
        {
            Vector3 checkPosition = checkTransform.position;

            if (!shouldCheckZ)
            {
                checkPosition.z = checkBounds.center.z;
            }

            return !checkBounds.Contains(checkPosition);;
        }


        public static int ToPercents(this float value) => (int)(value * 100f);


        public static string ToStringPercents (this float value)
        {
            int percents = value.ToPercents();

            return percents.ToString();
        }

        #endregion



        #region Editor
        #if UNITY_EDITOR

        public static UnityEngine.Object[] FindAssets(Type findType)
        {
            List<UnityEngine.Object> result = new List<UnityEngine.Object>();

            string filter = $"t:{findType.Name}";
            string[] guids = AssetDatabase.FindAssets(filter);

            foreach (var guid in guids)
            {
                string path = AssetDatabase.GUIDToAssetPath(guid);
                UnityEngine.Object unityObject = AssetDatabase.LoadAssetAtPath(path, findType);

                result.Add(unityObject);
            }

            return result.ToArray();
        }

        #endif
        #endregion

    }
}
