﻿using UnityEngine;


public class SingletonMonobehaviour<T> : MonoBehaviour where T : MonoBehaviour
{

    public static T Instance
    {
        get;
        private set;
    }


    private void Awake()
    {
        Instance = GetComponent<T>();
        Input.multiTouchEnabled = true;
    }
}
