﻿using System;
using UnityEngine;


public class InputController : MonoBehaviour
{
    public static event Action<Vector3> OnMouseDown;

    Camera mainCamera;



    void Start()
    {
        mainCamera = Camera.main;
    }


    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector3 pressedPosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            OnMouseDown?.Invoke(pressedPosition);
        }
    }
}
