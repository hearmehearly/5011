﻿using UnityEngine;


[RequireComponent(typeof(SpriteRenderer))]
public class ParallaxAdapteTop : MonoBehaviour
{
    private void Awake()
    {
        transform.position = new Vector3(transform.position.x, Camera.main.orthographicSize, transform.position.z);
    }
}

