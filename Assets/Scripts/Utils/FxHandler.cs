﻿using System;
using UnityEngine;


[Serializable]
public class FxHandler
{
    [SerializeField] private float delay = default;
    [SerializeField] private float secondsToDestroy = 10.0f;
    [SerializeField] private ParticleSystem particleSystem = default;

    private ParticleSystem currentSystem;

    public void Play(Vector3 position, Transform parent = default)
    {
        TaskInvoker.Instance.UnscheduleAllMethodForTarget(this);
        TaskInvoker.Instance.CallMethodWithDelay(this, () => PlayImmediately(position, parent), delay);
    }

    public void PlayImmediately(Vector3 position, Transform parent = default)
    {
        if (currentSystem == null)
        {
            currentSystem = UnityEngine.Object.Instantiate(particleSystem, parent);
        }

        currentSystem.transform.position = position;
        currentSystem.Clear();
        currentSystem.Play(true);
    }


    public void PlayOnce(Vector3 position, Transform parent = default)
    {
        Play(position, parent);
        TaskInvoker.Instance.CallMethodWithDelay(this, Stop, secondsToDestroy);
    }
    

    public void PlayImmediatelyOnce(Vector3 position, Transform parent = default)
    {
        PlayImmediately(position, parent);
        TaskInvoker.Instance.CallMethodWithDelay(this, Stop, secondsToDestroy);
    }


    public void Stop()
    {
        TaskInvoker.Instance.UnscheduleAllMethodForTarget(this);

        if (currentSystem != null)
        {
            currentSystem.Stop();
            UnityEngine.Object.Destroy(currentSystem.gameObject);
            currentSystem = null;
        }
    }
         
}
