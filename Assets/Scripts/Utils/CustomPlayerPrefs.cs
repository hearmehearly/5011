﻿using System;
using UnityEngine;


public static class CustomPlayerPrefs
{
    private const int BOOL_TRUE_INT_VALUE = 1;
    private const int BOOL_FALSE_INT_VALUE = 0;


    public static void SetBool(string key, bool value, bool isSaveImmediately = false)
    {
        int targetValue = (value) ? (BOOL_TRUE_INT_VALUE) : (BOOL_FALSE_INT_VALUE);
        PlayerPrefs.SetInt(key, targetValue);
    }


    public static bool GetBool(string key, bool defaultValue)
    {
        int currentDefaultValue = defaultValue ? BOOL_TRUE_INT_VALUE : BOOL_FALSE_INT_VALUE;
        return PlayerPrefs.GetInt(key, currentDefaultValue) == BOOL_TRUE_INT_VALUE;
    }


    public static bool GetBool(string key)
    {
        return PlayerPrefs.GetInt(key) == BOOL_TRUE_INT_VALUE;
    }


    public static void SetObjectValue<T>(string key, T value, bool saveImmediately = false)
        where T : class
    {
        string objectValue = (value == null) ? (string.Empty) : (JsonConvert.SerializeObject(value));

        SetString(key, objectValue, saveImmediately);
    }


    public static T GetObjectValue<T>(string key) where T : class
    {
        string savedObjectValue = GetString(key);

        return (string.IsNullOrEmpty(savedObjectValue))
            ? (null)
            : (JsonConvert.DeserializeObject<T>(savedObjectValue));
    }


    public static DateTime GetDateTime(string key, DateTime defaultValue)
    {
        string savedString = PlayerPrefs.GetString(key);
        DateTime result = defaultValue;

        if (!string.IsNullOrEmpty(savedString))
        {
            long temp = Convert.ToInt64(savedString);
            result = DateTime.FromBinary(temp);
        }

        return result;
    }


    public static string GetString(string key)
    {
        return PlayerPrefs.GetString(key, "default_string");
    }


    public static void SetString(string key, string value, bool isSaveImmediately = false)
    {
        PlayerPrefs.SetString(key, value);

        if (isSaveImmediately)
        {
            PlayerPrefs.Save();
        }
    }


    public static void SetDateTime(string key, DateTime value, bool isSaveImmediately = false)
    {
        PlayerPrefs.SetString(key, value.ToBinary().ToString());

        if (isSaveImmediately)
        {
            PlayerPrefs.Save();
        }
    }
}
