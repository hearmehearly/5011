﻿using Fighter5011;
using Fighter5011.Ui;
using UnityEngine;
using System;


[CreateAssetMenu]
public class ContentStorage : ScriptableObject
{
    [Header("Fighters")]

    [SerializeField]
    private PlayerFighter playerFighterPrefab = default;
    [SerializeField]
    private NPCFighter NPCFighterPrefab = default;

    [SerializeField]
    private AnimatorScreen[] animatorViews = default;

    [SerializeField]
    private RuneObject runeObject = default;

    static readonly ResourceAsset<ContentStorage> asset = new ResourceAsset<ContentStorage>("Game/ContentStorage");



    public static PlayerFighter CreatePlayerFighter(Vector3 position = default, Transform parent = default)
    {
        PlayerFighter result = Instantiate(asset.Value.playerFighterPrefab, parent);
        result.transform.position = position;
        result.CreateControllers();

        return result;
    }


    public static NPCFighter CreateNPCFighter(Vector3 position = default, Transform parent = default)
    {
        NPCFighter result = Instantiate(asset.Value.NPCFighterPrefab, position, Quaternion.identity, parent);
        result.CreateControllers();

        return result;
    }

    public static AnimatorScreen GetAnimatorScreen(ScreenType screenType)
    {
        var foundView = Array.Find(asset.Value.animatorViews, e => e.ScreenType == screenType);

        if (foundView == null)
        {
            Debug.Log($"No screen found with type {screenType} in {asset}");
        }

        return foundView;
    }


    public static RuneObject CreateRuneObject(RuneType type, Vector3 position = default, Transform parent = default)
    {
        var result = Instantiate(asset.Value.runeObject, parent);
        result.transform.position = position;
        result.SetType(type);

        return result;
    }
}
