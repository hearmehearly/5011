﻿using System;
using UnityEngine;


[CreateAssetMenu]
public class SocialController : ScriptableObject
{
    static readonly ResourceAsset<SocialController> asset = new ResourceAsset<SocialController>("Game/SocialController");

    private const int hoursDelta = 24;
    private static readonly string[] SOCIAL_URL = new[]
    {
        "https://www.instagram.com/p/CWBoA2_orp3/",
        "https://www.instagram.com/p/CF5i20FlsGe/",
        "https://www.instagram.com/p/B1CbFoJoI37/"
    };

    const string LastAcceptDateKey = "SocialController_LastAcceptDateKey";

    [SerializeField]
    private float reward = 0.0f;


    public static string URL => SOCIAL_URL.RandomObject();


    private DateTime LastAcceptDate
    {
        get => CustomPlayerPrefs.GetDateTime(LastAcceptDateKey, DateTime.MinValue);
        set => CustomPlayerPrefs.SetDateTime(LastAcceptDateKey, value);
    }


    private bool IsOfferAvailable => Player.Level >= 3 &&
                                     DateTime.Now.Subtract(LastAcceptDate).TotalHours > hoursDelta;



    public static void TryProposeOffer(Action<UnitResult> callback = null)
    {
        if (asset.Value.IsOfferAvailable)
        {
            UISocialPopup.Prefab.Instance.Show(asset.Value.reward, callback);
            asset.Value.LastAcceptDate = DateTime.Now;
        }
        else
        {
            callback?.Invoke(null);
        }
    }


    public static void TakeReward()
    {
        Player.AddCoins(asset.Value.reward);

    }
}
