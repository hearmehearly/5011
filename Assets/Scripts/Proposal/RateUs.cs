﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RateUs
{
    public const string URL = "https://play.google.com/store/apps/details?id=com.hearmehearly.fighter";

    public const int FirstLevelToShow = 2;

    public const int LevelsDelta = 5;

    public static bool IsAppRated
    {
        get => CustomPlayerPrefs.GetBool("rate_us_key");
        private set => CustomPlayerPrefs.SetBool("rate_us_key", value);
    }

    public static bool CanShow(int levelIndex)
    {
        return levelIndex == FirstLevelToShow || ((levelIndex + FirstLevelToShow) % LevelsDelta == 0);
    }


    public const float Reward = 1000.0f;

    public static void ApplyReward()
    {
        if (IsAppRated)
        {
            return;
        }

        Player.AddCoins(Reward);
        IsAppRated = true;
    }
}
