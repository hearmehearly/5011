﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Fighter5011.Ui;


public class TrainingRoomManager : MonoBehaviour
{
    [SerializeField]
    private Fighter.Name debugFighterName = default;

    [SerializeField]
    private Camera gameCamera = default;

    [SerializeField]
    private Transform targetRoot = default;
    [SerializeField]
    private Transform weakTargetRoot = default;

    private PlayerFighter fighter;

    private NPCFighter target;

    private List<Fighter> spawnedFighters = new List<Fighter>();


    public static TrainingRoomManager Instance
    {
        get;
        private set;
    }


    public Camera GameCamera => gameCamera;



    private void Awake()
    {
        Instance = this;
        Input.multiTouchEnabled = true;

        DevContent.SetEnabled(true);
    }


    private void Start()
    {
        StartTraining();
    }


    private void Update()
    {
        if (UITrainingRoom.Prefab.Instance)
        {
            UITrainingRoom.Prefab.Instance.SetVelocity(fighter.MovementController.Velocity);
        }
    }


    public void SpawnWeakTarget()
    {
        target = ContentStorage.CreateNPCFighter(weakTargetRoot.transform.position, weakTargetRoot);
        target.Initialize(fighter.CurrentName);
        target.StartLevel();

        UITrainingRoom.Prefab.Instance.InitializeTarget(target);
    }


    private void StartTraining()
    {
        AudioManager.Instance.Initialize();

        UITrainingRoom.Prefab.Instance.Show((_) => FinishTrain());
        UITrainingRoom.OnShouldSwitchFighter += SwitchFighter;
        UITrainingRoom.OnShouldApplyControlFighter += ApplyControl;
        UITrainingRoom.OnShouldDispel += ResetControl;
        UITrainingRoom.OnShouldApplyDamage += OnShouldApplyDamage;
        
        DebugCreatePlayer();
        DebugCreateTrainingTarget();


        void DebugCreatePlayer()
        {
            if (fighter != null)
            {
                return;
            }

            UILevel uILevel = UiScreenManager.Instance.LoadedScreen<UILevel>(Fighter5011.ScreenType.Ingame);

            if (uILevel == null)
            {
                uILevel = UiScreenManager.Instance.ShowScreen(Fighter5011.ScreenType.Ingame, (_) => DebugCreatePlayer()) as UILevel;
            }

            fighter = ContentStorage.CreatePlayerFighter();
            fighter.Initialize(debugFighterName);
            fighter.SetupCamera(gameCamera);
            fighter.CombatController.OnHitFighter += SetLastDamageInfo;

            UITrainingRoom.Prefab.Instance.InitializePlayer(fighter);
            uILevel.InitializeAttackButton(fighter.CombatController.CombatType);
            uILevel.InitializeAbilityButtons(fighter.AbilityController.currentAbilities);

            fighter.StartLevel();

            fighter.AddOnDieCallback((_) =>
            {
                fighter.CombatController.OnHitFighter -= SetLastDamageInfo;
                uILevel.Hide();
            });

        }


        void DebugCreateTrainingTarget()
        {
            target = ContentStorage.CreateNPCFighter(targetRoot.transform.position, targetRoot);
            target.Initialize(Fighter.Name.TrainingTarget);
            target.StartLevel();

        }
    }


    private void FinishTrain()
    {
        UITrainingRoom.OnShouldSwitchFighter -= SwitchFighter;
        UITrainingRoom.OnShouldApplyControlFighter -= ApplyControl;
        UITrainingRoom.OnShouldDispel -= ResetControl;
        UITrainingRoom.OnShouldApplyDamage -= OnShouldApplyDamage;

        SceneManager.LoadScene("IntroScene");
    }


    private void SwitchFighter(Fighter.Name name)
    {
        fighter.FinishLevel();
        fighter.Deinitialize();
        fighter.Initialize(name);
        fighter.StartLevel();
        Debug.Log("<color=green>Initialized fighter :" + name + "</color>");
    }


    private void ApplyControl(ControlType controlType)
    {
        fighter.ApplyControl(controlType, 3.0f, this);
        Debug.Log("<color=green>Apply control :" + controlType + " per 3.0f sec</color>");
    }


    private void ResetControl()
    {
        fighter.ResetControl();
    }


    private void OnShouldApplyDamage(float damage)
    {
        fighter.ApplyHit(null, damage, DamageType.Pure, Vector2.zero, default, false, null);
    }


    private void SetLastDamageInfo(Fighter fighter, float damage, int combo)
    {
        UITrainingRoom.Prefab.Instance.SetLastDamage(damage);
        UITrainingRoom.Prefab.Instance.SetLastCombo(combo);
    }
}