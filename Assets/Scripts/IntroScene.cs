﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class IntroScene : MonoBehaviour
{
    [SerializeField]
    private Button trainingRoom = null;
    [SerializeField]
    private Button game = null;


    private void Awake()
    {
        trainingRoom.onClick.AddListener(EnterTrainingRoomScene);
        game.onClick.AddListener(EnterGameScene);
    }


    private void OnDestroy()
    {
        trainingRoom.onClick.RemoveListener(EnterTrainingRoomScene);
        game.onClick.RemoveListener(EnterGameScene);
    }

    void EnterTrainingRoomScene()
    {
        SceneManager.LoadScene("TrainingRoom");
    }


    void EnterGameScene()
    {
        SceneManager.LoadScene("MainScene");
    }
}
