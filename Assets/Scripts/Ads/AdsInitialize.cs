﻿using UnityEngine.Advertisements;


public class AdsInitialize : IAction
{
    private const string iosGameId = "3551847";
    private const string androidGameId = "3551846";
    private  const bool TestMode = true;

    public void PerfomAction()
    {
        Advertisement.Initialize(SelectGameId(), TestMode);
    }

    public string SelectGameId()
    {
#if UNITY_IOS
        return iosGameId;
#elif UNITY_ANDROID
        return androidGameId;
#else
        return default;
#endif
    }
}
