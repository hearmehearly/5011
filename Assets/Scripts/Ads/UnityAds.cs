﻿using System;
using UnityEngine;
using UnityEngine.Advertisements;


public enum AdType
{
    Interstitial,
    Video,
    Banner
}

public enum VideoResult
{
    NoInternet,
    NoAds,
    Viewed,
    Skipped
}


public class AdsShowingInfo
{
    public string placement = string.Empty;

    public AdsShowingInfo(string placement)
    {
        this.placement = placement;
    }
}


public class VideoShowingInfo : AdsShowingInfo
{
    public string factor = string.Empty;

    public VideoShowingInfo(string placement, string factor)
        : base(placement)
    {
        this.factor = factor;
    }
}


public static class UnityAds
{
    public static event Action<AdType, AdsShowingInfo> OnAdsShow;
    public static event Action<AdType> OnAdsHide;
    public static event Action<AdType> OnAdsClick;

    public static event Action<bool> OnBannerLoaded;
    public static event Action OnVideoViewed;

    private static Action<VideoResult> OnVideoResult;
    private static AdsShowingInfo interstitialShowingInfo;
    private static AdsShowingInfo videoShowingInfo;
    private static AdsShowingInfo bannerShowingInfo;

    private static bool isNeedShowBannerAfterLoad;

    private static AdsInitialize adsInitialize;
    private static UnityAdsListener adsListener;
    private static AdsLimiter adsLimiter;

    private const string RewardedPlacement = "rewardedVideo";

    public static bool IsBannerShowing { get; private set; }


    public static bool IsInterstitialAvailable => Advertisement.isInitialized &&
                                                  Advertisement.IsReady() &&
                                                  !Advertisement.isShowing &&
                                                  Advertisement.isSupported;


    public static bool IsVideoAvailable => true;


    public static bool IsBannerAvailable => true;



    public static void Initialize()
    {
        adsInitialize = new AdsInitialize();
        adsInitialize.PerfomAction();

        adsLimiter = new AdsLimiter();

        adsListener = new UnityAdsListener();
        adsListener.OnAdsStart += AdsListener_OnAdsStart;
        adsListener.OnAdsFinish += AdsListener_OnAdsFinish;
    }


    public static void ShowVideo(Action<VideoResult> action, AdsShowingInfo showingInfo = null)
    {
        OnVideoResult = action;

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            OnVideoResult(VideoResult.NoInternet);
            return;
        }

        if (!IsVideoAvailable)
        {
            OnVideoResult(VideoResult.NoAds);
            return;
        }

        videoShowingInfo = showingInfo;

        Advertisement.Show(RewardedPlacement);
    }


    public static void ShowInterstitial()
    {
        if (!IsInterstitialAvailable || !adsLimiter.CanProposeInterstitial)
        {
            return;
        }
        
        Advertisement.Show();
    }


    public static void ShowBanner(AdsShowingInfo showingInfo)
    {
        if (!IsBannerAvailable)
        {
            isNeedShowBannerAfterLoad = true;
            return;
        }

        bannerShowingInfo = showingInfo;
        Advertisement.Banner.Show();
        IsBannerShowing = true;
        OnAdsShow?.Invoke(AdType.Banner, bannerShowingInfo);
    }


    public static void HideBanner()
    {
        Advertisement.Banner.Hide();
        IsBannerShowing = false;
        OnAdsHide?.Invoke(AdType.Banner);
        isNeedShowBannerAfterLoad = false;
        bannerShowingInfo = null;
    }


    private static void OnBannerLoad(bool result)
    {
        OnBannerLoaded?.Invoke(result);

        if (!isNeedShowBannerAfterLoad)
        {
            return;
        }

        isNeedShowBannerAfterLoad = false;
    }
    

    private static void OnVideoFinish(bool isViewed)
    {
        OnAdsHide(AdType.Video);

        if (isViewed)
        {
            OnVideoViewed();
        }

        OnVideoResult?.Invoke(isViewed ? VideoResult.Viewed : VideoResult.Skipped);
        videoShowingInfo = null;
    }

    
    private static void AdsListener_OnAdsStart(string placement)
    {
        OnAdsShow?.Invoke(AdType.Interstitial, interstitialShowingInfo);
    }


    private static void AdsListener_OnAdsFinish(string placement, ShowResult result)
    {
        OnInterstitialFinish();
        OnVideoFinish(result == ShowResult.Finished);
    }


    private static void OnInterstitialFinish()
    {
        OnAdsHide?.Invoke(AdType.Interstitial);
        interstitialShowingInfo = null;
    }
}
