﻿using System;
using UnityEngine.Advertisements;


public class UnityAdsListener : IUnityAdsListener
{
    public event Action<string> OnAdsStart;
    public event Action<string, ShowResult> OnAdsFinish;


    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        OnAdsFinish?.Invoke(placementId, showResult);
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        OnAdsStart?.Invoke(placementId);
    }

    public void OnUnityAdsReady(string placementId)
    {
    }

    public void OnUnityAdsDidError(string message)
    {
    }
}
