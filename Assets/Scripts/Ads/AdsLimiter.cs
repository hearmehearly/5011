﻿using System;
using UnityEngine;


public class AdsLimiter
{
    private const string LastInterstitialWatchTimeKey = "LastInterstitialWatchTime";
    private const string LastVideoWatchTimeKey = "LastVideoWatchTime";

    private const float VideoCooldown = 30.0f;
    private const float InterstitialCooldown = 30.0f;


    public bool CanProposeInterstitial
    {
        get
        {
            bool result = true;

            bool isMinLevelPassed = true;

            result &= isMinLevelPassed;
            result &= IsEnoughTimePassed;

            return result;
        }
    }


    private bool IsEnoughTimePassed
    {
        get
        {
            bool result = true;

            // use Abs to prevent time cheating
            float timeSinceVideoWatched = Mathf.Abs((float)DateTime.UtcNow.Subtract(LastVideoWatchTime).TotalSeconds);
            float timeSinceInterstitialWatched = Mathf.Abs((float)DateTime.UtcNow.Subtract(LastInterstitialWatchTime).TotalSeconds);

            bool isVideoCooldownPassed = timeSinceVideoWatched > VideoCooldown;
            bool isInterstitialCooldownPassed = timeSinceInterstitialWatched > InterstitialCooldown;

            result &= isVideoCooldownPassed;
            result &= isInterstitialCooldownPassed;

            return result;
        }
    }


    private DateTime LastInterstitialWatchTime
    {
        get => CustomPlayerPrefs.GetDateTime(LastInterstitialWatchTimeKey, DateTime.MinValue);
        set => CustomPlayerPrefs.SetDateTime(LastInterstitialWatchTimeKey, value);
    }


    private DateTime LastVideoWatchTime
    {
        get => CustomPlayerPrefs.GetDateTime(LastVideoWatchTimeKey, DateTime.MinValue);
        set => CustomPlayerPrefs.SetDateTime(LastVideoWatchTimeKey, value);
    }

    public void UpdateLastVideoWatchTime() => LastVideoWatchTime = DateTime.UtcNow;

    public void UpdateLastInterstitialWatchTime() => LastInterstitialWatchTime = DateTime.UtcNow;

}