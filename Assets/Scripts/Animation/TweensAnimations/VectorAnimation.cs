﻿using System;
using DG.Tweening;
using DG.Tweening.Core;
using UnityEngine;


namespace Fighter5011
{
    [Serializable]
    public class VectorAnimation : CommonAnimation
    {
        public Vector3 beginValue = default;
        public Vector3 endValue = default;


        public void Play(DOSetter<Vector3> setter, object handler, Action callback = null, bool reversed = false)
        {
            var begin = reversed ? endValue : beginValue;
            var end = reversed ? beginValue : endValue;

            var tween = DOTween
                .To(() => begin, setter, end, duration)
                .SetDelay(delay)
                .SetEase(curve)
                .SetId(handler)
                .SetUpdate(shouldUseUnscaledDeltaTime)
                .OnComplete(() => callback?.Invoke());

            if (loop)
            {
                tween.SetLoops(-1, loopType);
            }
        }


        public void SetupData(VectorAnimation animation)
        {
            base.SetupData(animation);

            beginValue = animation.beginValue;
            endValue = animation.endValue;
        }
    }
}
