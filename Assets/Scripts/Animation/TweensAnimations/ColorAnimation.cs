﻿using System;
using DG.Tweening;
using DG.Tweening.Core;
using UnityEngine;


namespace Fighter5011
{
    [Serializable]
    public class ColorAnimation : CommonAnimation
    {
        #region Fields

        public Color beginValue = Color.white;
        public Color endValue = Color.white;

        #endregion



        #region Methods

        public void Play(DOSetter<Color> setter, object handler, Action callback = null)
        {
            var tween = DOTween
                 .To(() => beginValue, setter, endValue, duration)
                 .SetDelay(delay)
                 .SetEase(curve)
                 .SetUpdate(shouldUseUnscaledDeltaTime)
                 .SetId(handler)
                 .OnComplete(() => callback?.Invoke());

            if (loop)
            {
                tween.SetLoops(-1, loopType);
            }
        }


        public void SetupData(ColorAnimation animation)
        {
            base.SetupData(animation);

            beginValue = animation.beginValue;
            endValue = animation.endValue;
        }

        #endregion
    }
}
