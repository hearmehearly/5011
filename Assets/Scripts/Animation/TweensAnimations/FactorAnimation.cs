﻿using System;
using DG.Tweening;
using DG.Tweening.Core;


namespace Fighter5011
{
    [Serializable]
    public class FactorAnimation : CommonAnimation
    {
        #region Fields

        public float beginValue = default;

        public float endValue = default;

        #endregion



        #region Public methods

        public void Play(DOSetter<float> setter, object handler, Action callback = null, bool reversed = false)
        {
            var begin = reversed ? endValue : beginValue;
            var end = reversed ? beginValue : endValue;

            var tween = DOTween
                .To(() => begin, setter, end, duration)
                .SetDelay(delay)
                .SetEase(curve)
                .SetId(handler)
                .SetUpdate(shouldUseUnscaledDeltaTime)
                .OnComplete(() => callback?.Invoke());

            if (loop)
            {
                tween.SetLoops(-1, loopType);
            }
        }


        public void SetupData(FactorAnimation animation)
        {
            base.SetupData(animation);

            beginValue = animation.beginValue;
            endValue = animation.endValue;
        }

        #endregion
    }
}
