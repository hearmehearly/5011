﻿using System;
using DG.Tweening;
using UnityEngine;


namespace Fighter5011
{
    [Serializable]
    public class CommonAnimation
    {
        #region Fields
        
        public float duration = default;

        public AnimationCurve curve = default;
        
        public float delay = default;

        public bool loop = default;
        public LoopType loopType = default;

        public bool shouldUseUnscaledDeltaTime = default;

        #endregion



        #region Properties

        public virtual float Time => duration + delay;

        #endregion



        #region Public methods

        public void SetupData(CommonAnimation animation)
        {
            duration = animation.duration;
            curve = animation.curve;
            delay = animation.delay;
            shouldUseUnscaledDeltaTime = animation.shouldUseUnscaledDeltaTime;
        }

        #endregion
    }
}
