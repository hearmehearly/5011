﻿using System;
using System.Collections.Generic;


public static class Player
{
    [Serializable]
    public class Data
    {
        public int Level = default;
        public float Coins = default;
        public float Gems = default;

        public List<Fighter.Name> openedFighters = new List<Fighter.Name>();
    }


    const string DATA_KEY = "PlayerData";

    static Data data;
    
    public static event Action OnChangeCoins;
    public static event Action OnChangeGems;


    public static int Level
    {
        get => data.Level;
        
        set
        {
            data.Level = value;
            CustomPlayerPrefs.SetObjectValue(DATA_KEY, data);
        }
    }


    public static float Coins
    {
        get => data.Coins;
        
        private set
        {
            data.Coins = value;
            CustomPlayerPrefs.SetObjectValue(DATA_KEY, data);
            OnChangeCoins?.Invoke();
        }
    }


    public static float Gems
    {
        get => data.Gems;
        
        private set
        {
            data.Gems = value;
            CustomPlayerPrefs.SetObjectValue(DATA_KEY, data);
            OnChangeGems();
        }
    }


    static Player()
    {
        data = CustomPlayerPrefs.GetObjectValue<Data>(DATA_KEY);

        if (data == null)
        {
            UnityEngine.Debug.Log("NEW GAME STARTED");
        }

        data = data ?? new Data();
        OpenFighter(Fighter.Name.Leiv);

        SaveData();
    }


    public static void OpenFighter(Fighter.Name name)
    {
        data.openedFighters.AddExclusive(name);
        SaveData();
    }

    public static bool IsFighterOpen(Fighter.Name name) =>
        data.openedFighters.Contains(name);

    public static void UpLevel()
    {
        Level++;
    }


    public static void AddCoins(float count)
    {
        Coins += count;
    }


    public static void AddGems(float gems)
    {
        Gems += gems;
    }


    public static bool TryRemoveCoins(float count)
    {
        if (Coins < count)
        {
            return false;
        }

        Coins -= count;

        return true;
    }


    public static bool TryRemoveGems(float count)
    {
        if (Gems < count)
        {
            return false;
        }

        Gems -= count;
        return true;
    }


    private static void SaveData()
    {
        CustomPlayerPrefs.SetObjectValue(DATA_KEY, data);
    }
}
