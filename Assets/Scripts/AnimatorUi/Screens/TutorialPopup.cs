﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System;


namespace Fighter5011.Ui
{
    public class TutorialPopup : AnimatorScreen
    {
        [Serializable]
        private class Data
        {
            public TutorialType type = default;
            public GameObject root = default;

        }
        #region Fields

        [SerializeField] private Data[] data = default;
        [SerializeField] private Button okayButton = default;

        #endregion



        #region Overrided properties

        public override ScreenType ScreenType => ScreenType.TutorialPopup;

        #endregion



        #region Overrided methods

        public override void Show()
        {
            base.Show();

            okayButton.onClick.AddListener(OkayButton_OnClick);
        }

        public override void Hide()
        {
            okayButton.onClick.RemoveListener(OkayButton_OnClick);

            base.Hide();
        }

        public void SetTutorial(TutorialType type)
        {
            foreach (var d in data)
            {
                d.root.SetActive(type == d.type);
            }
        }

        #endregion



        #region Methods
    

        #endregion



        #region Events handlers

        private void OkayButton_OnClick() => Hide();

        #endregion
    }
}

