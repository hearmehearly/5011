﻿using System;
using UnityEngine;
using UnityEngine.UI;


namespace Fighter5011.Ui
{
    public class PauseScreen : AnimatorScreen
    {
        #region Fields

        [SerializeField] private Button menuButton = default;
        [SerializeField] private Button continueButton = default;

        #endregion


        #region Properties

        protected override string ShowKey => AnimationKeys.Screen.Show;

        protected override string HideKey => AnimationKeys.Screen.Hide;

        public override ScreenType ScreenType => ScreenType.PauseScreen;

        #endregion



        #region Methods

        public override void Initialize(Action<AnimatorView> onShowEndCallback = null,
                                        Action<AnimatorView> onHideEndCallback = null,
                                        Action<AnimatorView> onShowBegin = null,
                                        Action<AnimatorView> onHideBegin = null)
        {
            base.Initialize(onShowEndCallback, onHideEndCallback, onShowBegin, onHideBegin);

            menuButton.onClick.AddListener(MenuButton_OnClick);
            continueButton.onClick.AddListener(Hide);
        }


        public override void Deinitialize()
        {
            DeinitializeButtons();

            base.Deinitialize();
        }

        private void DeinitializeButtons()
        {
            menuButton.onClick.RemoveListener(MenuButton_OnClick);
            continueButton.onClick.RemoveListener(Hide);
        }


        private void MenuButton_OnClick()
        {
            DeinitializeButtons();

       }

        #endregion
    }
}
