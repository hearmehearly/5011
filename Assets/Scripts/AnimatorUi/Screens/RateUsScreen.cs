﻿using UnityEngine;
using UnityEngine.UI;

namespace Fighter5011.Ui
{
    public class RateUsScreen : AnimatorScreen
    {
        #region Fields

        [SerializeField] private Button rateButton = default;
        [SerializeField] private Button closeButton = default;

        #endregion



        #region Overrided properties

        public override ScreenType ScreenType => ScreenType.RateUsScreen;

        protected override string ShowKey => AnimationKeys.Screen.Show;

        protected override string HideKey => AnimationKeys.Screen.Hide;

        #endregion



        #region Overrided methods

        public override void Show()
        {
            base.Show();

            rateButton.onClick.AddListener(RateButton_OnClick);
            closeButton.onClick.AddListener(CloseButton_OnClick);
        }


        public override void Hide()
        {
            rateButton.onClick.RemoveAllListeners();
            closeButton.onClick.RemoveAllListeners();

            base.Hide();
        }

        #endregion



        #region Events handlers

        private void RateButton_OnClick()
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                UiScreenManager.Instance.ShowPopup(OkPopupType.NoInternet);
            }
            else
            {
                Application.OpenURL(RateUs.URL);
                RateUs.ApplyReward();
                Hide();
            }
        }

        private void CloseButton_OnClick() => Hide();

        #endregion
    }
}
