﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Fighter5011.Ui
{
    public class OkayScreen : AnimatorScreen
    {
        #region Fields

        [SerializeField] private TMP_Text textField = default;
        [SerializeField] private Button okayButton = default;

        #endregion



        #region Overrided properties

        public override ScreenType ScreenType => ScreenType.OkayScreen;

        #endregion



        #region Overrided methods

        public override void Show()
        {
            base.Show();

            okayButton.onClick.AddListener(OkayButton_OnClick);
        }

        public override void Hide()
        {
            okayButton.onClick.RemoveListener(OkayButton_OnClick);

            base.Hide();
        }

        #endregion



        #region Methods

        public void ChangeText(string text) => textField.text = text;

        #endregion



        #region Events handlers

        private void OkayButton_OnClick() => Hide();

        #endregion
    }
}

