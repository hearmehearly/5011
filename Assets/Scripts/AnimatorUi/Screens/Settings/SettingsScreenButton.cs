﻿using UnityEngine;
using UnityEngine.UI;
using System;


namespace Fighter5011
{
    [Serializable]
    public class SettingsScreenButton
    {
        public enum Type
        {
            None            = 0,
            Sound           = 1,
            Music           = 2,
            Vibration       = 3,
        }


        [Serializable]
        private class Data
        {
            [SerializeField] private Graphic graphic = default;

            [SerializeField] private Color enabledColor = default;
            [SerializeField] private Color disabledColor = default;

            public void SetVisual(bool enabled)
            {
                Color colorToSet = enabled ? enabledColor : disabledColor;
                graphic.color = colorToSet;
            }
        }
                
        [SerializeField] private Type type = default;
        [SerializeField] private Button button = default;
        
        [SerializeField] private Data[] visualData = default;

        private Action enableStateCallback;
        private Action disableStateCallback;
        
        public bool IsEnabledState { get; private set; }


        public Type CurrentType => type;
        

        public void Initialize()
        {
            RefreshVisual(true);
            button.onClick.AddListener(Switch);
        }


        public void Deinitialize()
        {
            button.onClick.RemoveListener(Switch);
        }


        public void SetEnabled(bool enabled)
        {
            bool isImmediately = IsEnabledState == enabled;
            IsEnabledState = enabled;
            RefreshVisual(isImmediately);
        }


        public void AddButtonOnClickCallback(Action callback, bool isEnabledButton)
        {
            if (isEnabledButton)
            {
                enableStateCallback += callback;
            }
            else
            {
                disableStateCallback += callback;
            }
        }


        public void RemoveButtonOnClickCallback(Action callback, bool isEnabledButton)
        {
            if (isEnabledButton)
            {
                enableStateCallback -= callback;
            }
            else
            {
                disableStateCallback -= callback;
            }
        }


        private void RefreshVisual(bool isImmediately)
        {
            foreach (var data in visualData)
            {
                data.SetVisual(IsEnabledState);
            }            
        }
        

        private void Switch()
        {
            Action callback = IsEnabledState ? enableStateCallback : disableStateCallback;
            callback?.Invoke();
        }
    }
}
