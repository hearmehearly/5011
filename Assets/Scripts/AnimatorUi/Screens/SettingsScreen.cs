﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Fighter5011.Ui
{
    public class SettingsScreen : AnimatorScreen
    {
        [SerializeField] private Button hideButton = default;

        [SerializeField] private SettingsScreenButton musicButton = default;
        [SerializeField] private SettingsScreenButton soundButton = default;
        [SerializeField] private SettingsScreenButton vibrationButton = default;
        [SerializeField] private Button rateUsButton = default;

        private List<SettingsScreenButton> buttons;


        
        public override ScreenType ScreenType => ScreenType.Settings;

        protected override string ShowKey => AnimationKeys.Screen.Show;

        protected override string HideKey => AnimationKeys.Screen.Hide;
        


        public override void Initialize(Action<AnimatorView> onShowEndCallback = null,
                                        Action<AnimatorView> onHideEndCallback = null,
                                        Action<AnimatorView> onShowBegin = null,
                                        Action<AnimatorView> onHideBegin = null)
        {
            base.Initialize(onShowEndCallback, onHideEndCallback, onShowBegin, onHideBegin);

            hideButton.onClick.AddListener(Hide);
            rateUsButton.onClick.AddListener(OpenRateUs);

            buttons = new List<SettingsScreenButton>
            {
                vibrationButton,
                musicButton,
                soundButton
            };

            foreach (var button in buttons)
            {
                button.Initialize();
            }

            vibrationButton.AddButtonOnClickCallback(EnableVibration, false);
            vibrationButton.AddButtonOnClickCallback(DisableVibration, true);

            musicButton.AddButtonOnClickCallback(EnableMusic, false);
            musicButton.AddButtonOnClickCallback(DisableMusic, true);

            soundButton.AddButtonOnClickCallback(EnableSound, false);
            soundButton.AddButtonOnClickCallback(DisablSound, true);
            
            RefreshButtonsState();
        }


        public override void Deinitialize()
        {
            base.Deinitialize();

            hideButton.onClick.RemoveListener(Hide);
            rateUsButton.onClick.RemoveListener(OpenRateUs);

            vibrationButton.RemoveButtonOnClickCallback(EnableVibration, false);
            vibrationButton.RemoveButtonOnClickCallback(DisableVibration, true);

            musicButton.RemoveButtonOnClickCallback(EnableMusic, false);
            musicButton.RemoveButtonOnClickCallback(DisableMusic, true);

            soundButton.RemoveButtonOnClickCallback(EnableSound, false);
            soundButton.RemoveButtonOnClickCallback(DisablSound, true);
            
            
            foreach (var button in buttons)
            {
                button.Deinitialize();
            }
        }


        private void RefreshButtonsState()
        {

            bool isMusicEnabled = AudioManager.Instance.IsMusicEnable;
            musicButton.SetEnabled(isMusicEnabled);

            bool isSoundsMuted = !AudioManager.Instance.IsSoundEnable;
            soundButton.SetEnabled(!isSoundsMuted);
        }
        
              
        
        private void EnableVibration()
        {
            RefreshButtonsState();
        }


        private void DisableVibration()
        {
            RefreshButtonsState();
        }


        private void EnableMusic()
        {
            AudioManager.Instance.IsMusicEnable = true;

            RefreshButtonsState();
        }


        private void DisableMusic()
        {
            AudioManager.Instance.IsMusicEnable = false;

            RefreshButtonsState();
        }


        private void EnableSound()
        {
            AudioManager.Instance.IsSoundEnable = true;

            RefreshButtonsState();
        }


        private void DisablSound()
        {
            AudioManager.Instance.IsSoundEnable = false;

            RefreshButtonsState();
        }


        private void OpenRateUs()
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                UiScreenManager.Instance.ShowPopup(OkPopupType.NoInternet);
                return;
            }

            Application.OpenURL(RateUs.URL);
        }


        private void OpenInstagram()
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                UiScreenManager.Instance.ShowPopup(OkPopupType.NoInternet);
                return;
            }

            Application.OpenURL(SocialController.URL);
        }
    }
}
