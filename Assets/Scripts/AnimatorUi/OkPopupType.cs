﻿namespace Fighter5011.Ui
{
    public enum OkPopupType
    {
        None                =   0,
        NotEnoughCurrency   =   1,
        NoInternet          =   2,
        NoVideo             =   3
    }
}
