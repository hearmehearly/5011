﻿using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;


namespace Fighter5011
{
    public static class EventSystemController
    {
        #region Fields

        private static EventSystem eventSystem;

        private static readonly List<object> pauseSenders = new List<object>();

        #endregion



        #region Methods

        public static void SetupEventSystem(EventSystem eventSystemValue)
        {
            eventSystem = eventSystemValue;

            if (eventSystem == null)
            {
                Debug.Log("No event system found");
            }
        }


        public static void EnableEventSystem()
        {
            if (eventSystem != null)
            {
                eventSystem.enabled = true;
            }
        }


        public static bool IsPointerOverGameObject(int pointerId)
        {
            bool result = default;

            if (eventSystem != null)
            {
                result = eventSystem.IsPointerOverGameObject(pointerId) ||
                         eventSystem.currentSelectedGameObject != null;
            }

            return result;
        }


        public static bool IsPointerOverGameObject() => IsPointerOverGameObject(-1);


        public static void SetSystemEnabled(bool enable, object target)
        {
            if (enable)
            {
                pauseSenders.Remove(target);
            }
            else
            {
                pauseSenders.AddExclusive(target);
            }

            if (eventSystem != null)
            {
                eventSystem.enabled = (pauseSenders.Count == 0);
            }
        }

        #endregion
    }
}
