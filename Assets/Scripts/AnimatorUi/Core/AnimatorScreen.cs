﻿namespace Fighter5011.Ui
{    
    public abstract class AnimatorScreen : AnimatorView
    {
        public abstract ScreenType ScreenType { get; }
        
        public override ViewType Type => ViewType.Screen;

        protected override string HideKey => AnimationKeys.Screen.Hide;

        protected override string ShowKey => AnimationKeys.Screen.Show;
    }
}
