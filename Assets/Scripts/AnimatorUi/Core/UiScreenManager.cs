﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace Fighter5011.Ui
{
    public class UiScreenManager : SingletonMonobehaviour<UiScreenManager>
    {
        private readonly List<AnimatorScreen> activeScreens = new List<AnimatorScreen>();

        private readonly Dictionary<OkPopupType, string> popupsText = new Dictionary<OkPopupType, string>
        {
            { OkPopupType.NotEnoughCurrency, "Not enough\nmoney" },
            { OkPopupType.NoInternet, "No internet" },
            { OkPopupType.NoVideo, "No video\navailable" }
        };
        
        
        public AnimatorScreen ShowPopup(OkPopupType popupType,
                                        Action<AnimatorView> onShowed = null,
                                        Action<AnimatorView> onHided = null,
                                        bool isForceHideIfExist = false,
                                        Action<AnimatorView> onShowBegin = null,
                                        Action<AnimatorView> onHideBegin = null)
        {
            onShowBegin += view =>
            {
                OkayScreen screen = view as OkayScreen;

                screen.ChangeText(popupsText[popupType]);
            };

            return ShowScreen(ScreenType.OkayScreen, onShowed, onHided, onShowBegin, onHideBegin, isForceHideIfExist);
        }


        public AnimatorScreen ShowScreen(ScreenType type,
                                         Action<AnimatorView> onShowed = null,
                                         Action<AnimatorView> onHided = null,
                                         Action<AnimatorView> onShowBegin = null,
                                         Action<AnimatorView> onHideBegin = null,
                                         bool isForceHideIfExist = true)
        {
            bool isScreenExist = IsScreenActive(type);

            if (isScreenExist)
            {
                if (isForceHideIfExist)
                {
                    AnimatorScreen existScreen = LoadedScreen<AnimatorScreen>(type);

                    if (existScreen != null)
                    {
                        existScreen.HideImmediately();
                    }
                }
                else
                {
                    Debug.Log($"Trying to show already active screen with type {type}");

                    return null;
                }
            }

            AnimatorScreen prefabToShow = ContentStorage.GetAnimatorScreen(type);

            if (prefabToShow == null)
            {
                Debug.Log($"Screen prefab missing for type {type}");

                return null;
            }

            AnimatorScreen screenToShow = Instantiate(prefabToShow);

            CommonUtility.SetObjectActive(screenToShow.gameObject, false);

            activeScreens.Add(screenToShow);

            onHided += (hidedView) =>
                {
                    ViewManager.RemoveLastViewInfo(hidedView);

                    RemoveScreenFromActiveList(screenToShow);
                };

            screenToShow.Initialize(onShowed, onHided, onShowBegin, onHideBegin);

            ViewManager.AddViewInfo(screenToShow);

            screenToShow.OnHideEnd += ScreenToShow_OnHideEnd;

            screenToShow.Show();

            return screenToShow;
        }


        private void ScreenToShow_OnHideEnd(AnimatorView view)
        {
            view.OnHideEnd -= ScreenToShow_OnHideEnd;

            if (view is AnimatorScreen screen)
            {
                RemoveScreenFromActiveList(screen);
            }
        }


        public void HideScreen(ScreenType type, Action<AnimatorView> callback = null)
        {
            var foundScreen = LoadedScreen<AnimatorScreen>(type);

            if (foundScreen != null)
            {
                foundScreen.Hide(callback, null);
            }
        }


        public void HideScreenImmediately(ScreenType type, Action<AnimatorView> callback = null)
        {
            var foundScreen = LoadedScreen<AnimatorScreen>(type);

            if (foundScreen != null)
            {
                foundScreen.HideImmediately(callback);
            }
        }


        public bool IsScreenActive(ScreenType type) => LoadedScreen<AnimatorScreen>(type) != null;


        public T LoadedScreen<T>(ScreenType type) where T : AnimatorScreen
        {
            AnimatorScreen result = activeScreens.Find(element => element.ScreenType == type);

            return (result as T);
        }

        
        private void RemoveScreenFromActiveList(AnimatorScreen screen)
        {
            if (activeScreens.Count > 0)
            {
                activeScreens.Remove(screen);
                screen.Deinitialize();
                Destroy(screen.gameObject);
            }
        }
       
    }
}