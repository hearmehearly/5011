﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Fighter5011.Ui
{
    [RequireComponent(typeof(Canvas))]
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(CanvasScaler))]
    public abstract class AnimatorView : MonoBehaviour, IDeinitializable, IView
    {
        #region Fields

        public event Action<AnimatorView> OnHideEnd;
        public event Action<AnimatorView> OnHideBegin;

        public event Action<AnimatorView> OnShowBegin;
        public event Action<AnimatorView> OnShowEnd;

        private Animator mainAnimator;
        protected Canvas mainCanvas;

        private List<ShowState> showStates;
        private List<HideState> hideStates;
        
        protected Action<AnimatorView> showEndCallback;
        protected Action<AnimatorView> hideEndCallback;

        protected Action<AnimatorView> showBeginCallback;
        protected Action<AnimatorView> hideBeginCallback;

        private RectTransform rect;

        #endregion


        #region Properties
        
        protected abstract string ShowKey { get; }

        protected abstract string HideKey { get; }

        protected virtual string IdleBeforeHideKey { get; }

        protected virtual string IdleAfterShowKey { get; }

        private RectTransform Rect
        {
            get
            {
                if (rect == null)
                {
                    rect = GetComponent<RectTransform>();
                }

                return rect;
            }
        }

        #endregion



        #region IView

        #region Properties

        public abstract ViewType Type { get; }

        public int SortingOrder { get; set; }

        public float ZPosition { get; set; }

        #endregion



        #region Methods

        public void SetVisualOrderSettings()
        {
            transform.position = transform.position.SetZ(ZPosition);
            mainCanvas.sortingOrder = SortingOrder;
        }


        public void ResetVisualOrderSettings() { }

        #endregion

        #endregion



        #region IInitializable

        public virtual void Initialize(Action<AnimatorView> onShowEndCallback = null,
                                       Action<AnimatorView> onHideEndCallback = null,
                                       Action<AnimatorView> onShowBeginCallback = null,
                                       Action<AnimatorView> onHideBeginCallback = null)
        {
            CommonUtility.SetObjectActive(gameObject, true);

            mainAnimator = GetComponent<Animator>();
            mainAnimator.enabled = false;

            mainCanvas = GetComponent<Canvas>();
            mainCanvas.renderMode = RenderMode.ScreenSpaceCamera;
            mainCanvas.worldCamera = Camera.main;


            showStates = new List<ShowState>(mainAnimator.GetBehaviours<ShowState>());
            showStates.ForEach(state =>
            {
                state.Initialize(mainAnimator);
                state.OnShowBegin += ShowState_OnShowBegin;
                state.OnShowEnd += ShowState_OnShowEnd;
            });


            hideStates = new List<HideState>(mainAnimator.GetBehaviours<HideState>());
            hideStates.ForEach(state =>
            {
                state.Initialize(mainAnimator);
                state.OnHideBegin += HideState_OnHideBegin;
                state.OnHideEnd += HideState_OnHideEnd;
            });
            
            showEndCallback = onShowEndCallback;
            hideEndCallback = onHideEndCallback;
            showBeginCallback = onShowBeginCallback;
            hideBeginCallback = onHideBeginCallback;

            InitPosition();
        }

        #endregion



        #region IDeinitializable

        public virtual void Deinitialize()
        {
            showStates.ForEach(state =>
            {
                state.OnShowBegin -= ShowState_OnShowBegin;
                state.OnShowEnd -= ShowState_OnShowEnd;
            });
            
            hideStates.ForEach(state =>
            {
                state.Initialize(mainAnimator);
                state.OnHideBegin -= HideState_OnHideBegin;
                state.OnHideEnd -= HideState_OnHideEnd;
            });
        }

        #endregion


        
        #region Methods

        private void InitPosition()
        {
            transform.localScale = Vector3.one;

            Rect.offsetMin = Vector2.zero;
            Rect.offsetMax = Vector2.zero;
        }

        
        public virtual void Show()
        {
            OnShowBegin?.Invoke(this);

            mainAnimator.enabled = true;
            SetInputStateEnabled(false);

            mainAnimator.SetTrigger(ShowKey);
        }


        public void Hide(Action<AnimatorView> onHideEnd, Action<AnimatorView> onHideBegin)
        {
            hideEndCallback += onHideEnd;
            hideBeginCallback += onHideBegin;

            Hide();
        }

        public virtual void Hide()
        {
            OnHideBegin?.Invoke(this);

            SetInputStateEnabled(false);

            mainAnimator.SetTrigger(HideKey);
                
            if (!string.IsNullOrEmpty(IdleBeforeHideKey))
            {
                mainAnimator.Play(IdleBeforeHideKey);
            }
        }

        public void HideImmediately(Action<AnimatorView> callback)
        {
            hideEndCallback += callback;

            HideImmediately();
        }


        public void HideImmediately()
        {
            OnHideBegin?.Invoke(this);

            FinishHide();
        }
        

        private void FinishHide()
        {
            SetInputStateEnabled(true);

            OnHideEnd?.Invoke(this);

            CommonUtility.SetObjectActive(gameObject, false);

            hideEndCallback?.Invoke(this);
        }


        private void FinishShow()
        {
            SetInputStateEnabled(true);

            OnShowEnd?.Invoke(this);

            showEndCallback?.Invoke(this);
        }


        protected void SetStrigger(string key) => mainAnimator.SetTrigger(key);


        protected void SetInputStateEnabled(bool isEnabled) => EventSystemController.SetSystemEnabled(isEnabled, this);

        #endregion



        #region Events handlers

        private void ShowState_OnShowBegin()
        { 
            showBeginCallback?.Invoke(this);
        }


        private void ShowState_OnShowEnd()
        {
            FinishShow();
        }


        private void HideState_OnHideBegin()
        {
            hideBeginCallback?.Invoke(this);
        }


        private void HideState_OnHideEnd()
        {
            FinishHide();
        }

        #endregion

           }
}
