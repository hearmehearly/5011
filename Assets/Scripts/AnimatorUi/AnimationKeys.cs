﻿namespace Fighter5011.Ui
{
    public static class AnimationKeys
    {
        public static class Screen
        {
            public const string Show = "Show";
            public const string Hide = "Hide";
        }
    }
}
