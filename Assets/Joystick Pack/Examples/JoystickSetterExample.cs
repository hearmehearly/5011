﻿using UnityEngine;
using UnityEngine.UI;


public class JoystickSetterExample : MonoBehaviour
{
    public VariableJoystick variableJoystick;
    public Text valueText;
    public Image background;
    public Sprite[] axisSprites;
    public Text angleText;

    public void ModeChanged(int index)
    {
        switch(index)
        {
            case 0:
                variableJoystick.SetMode(JoystickType.Fixed);
                break;
            case 1:
                variableJoystick.SetMode(JoystickType.Floating);
                break;
            default:
                break;
        }     
    }

    public void AxisChanged(int index)
    {
        switch (index)
        {
            case 0:
                variableJoystick.AxisOptions = AxisOptions.Both;
                background.sprite = axisSprites[index];
                break;
            default:
                break;
        }
    }
    

    private void Update()
    {
        valueText.text = "Current Value: " + variableJoystick.Direction;

        float angle = (Mathf.Atan2(variableJoystick.Direction.y, variableJoystick.Direction.x) * Mathf.Rad2Deg).Clamp0360();
        angleText.text = $"Angle: {angle}";
    }
}