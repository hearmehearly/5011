﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;


public enum JoystickType
{
    Fixed = 0,
    Floating = 1
}


public class VariableJoystick : Joystick
{
    public UnityEvent onPressed { get; set; } = new UnityEvent();
    public UnityEvent onPressedUp { get; set; } = new UnityEvent();

    [SerializeField] private float moveThreshold = 1;
    [SerializeField] private JoystickType joystickType = JoystickType.Fixed;

    private Vector2 fixedPosition = Vector2.zero;

    public float MoveThreshold
    {
        get => moveThreshold;
        set => moveThreshold = Mathf.Abs(value);
    }


    public bool IsFixed => joystickType == JoystickType.Fixed;


    public void SetMode(JoystickType _joystickType)
    {
        joystickType = _joystickType;

        if (IsFixed)
        {
            background.anchoredPosition = fixedPosition;
        }

        background.gameObject.SetActive(IsFixed);
    }


    protected override void Start()
    {
        base.Start();

        fixedPosition = background.anchoredPosition;
        SetMode(joystickType);
    }


    public override void OnPointerDown(PointerEventData eventData)
    {
        if (joystickType != JoystickType.Fixed)
        {
            background.anchoredPosition = ScreenPointToAnchoredPosition(eventData.position);
            background.gameObject.SetActive(true);
        }

        base.OnPointerDown(eventData);

        onPressed?.Invoke();
    }


    public override void OnPointerUp(PointerEventData eventData)
    {
        if (!IsFixed)
        {
            background.gameObject.SetActive(false);
        }

        base.OnPointerUp(eventData);

        onPressedUp?.Invoke();
    }
}
